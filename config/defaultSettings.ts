import { Settings as LayoutSettings } from '@ant-design/pro-components';

const Settings: LayoutSettings & {
  pwa?: boolean;
  logo?: string;
} = {
  navTheme: 'light',
  // 拂晓蓝
  colorPrimary: '#1890ff',
  layout: 'top',
  contentWidth: 'Fluid',
  fixedHeader: false,
  fixSiderbar: true,
  colorWeak: false,
  title: '环保设施向公众开放沉浸式展厅',
  pwa: false,
  iconfontUrl: '/iconfont/iconfont.js',
  logo: '/logo.png',
};

export default Settings;
