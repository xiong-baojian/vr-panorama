// import * as THREE from 'three';
let canvasTextures = [];
let canvasArr = [];
let loadedFaces = [];
// let raycaster = new THREE.Raycaster();

self.onmessage = async function ({data}) {
  // const { viewer, panoramaBox, tileUrls, canvasArr } = e.data;
  // await renderTiles(viewer, panoramaBox, tileUrls, canvasArr);
  // self.postMessage('renderTiles 完成');
};

 async function renderTiles(tileUrls, textures) {
  tileUrls?.forEach((item, index) => {
    const texture = textures[index];
    renderFaceTiles(texture, item);
  });
}

/**
 * 渲染每面
 * @param {*} panorama 全景载体
 * @param {*} index 每个面的index
 * @param {*} imageLeafParts 16张图片
 */
async function renderFaceTiles(texture, imageLeafParts) {
  const canvas = texture.image;
  const ctx = canvas.getContext('2d');

  async function loadImagePart(url) {
    return new Promise((resolve, reject) => {
      const image = new Image();
      image.src = url;
      image.onload = () => {
        resolve(image);
      };
      image.onerror = () => {
        reject(`图片加载失败: ${url}`);
      };
    });
  }
  async function loadImages() {
    for (let i = 0; i < 4; i++) {
      const x = (i % 2) * (width / 2);
      const y = Math.floor(i / 2) * (height / 2);
      await loadLeafImages(x, y, i);
    }
  }
  async function loadLeafImages(x, y, baseImageIndex) {
    const leafWidth = width / 4;
    const leafHeight = height / 4;
    const startLeafIndex = baseImageIndex * 4;

    for (let i = 0; i < 4; i++) {
      try {
        const leafImage = await loadImagePart(imageLeafParts[startLeafIndex + i]);
        const leafX = x + (i % 2) * leafWidth;
        const leafY = y + Math.floor(i / 2) * leafHeight;
        ctx.drawImage(leafImage, leafX, leafY, leafWidth, leafHeight);
      } catch (error) {
        console.error(error);
      }
    }
  }
  const width = 3600;
  const height = 3600;
  await loadImages();
}
