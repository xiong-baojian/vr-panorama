import { useState } from 'react';

export default () => {
  const [isMobile, setIsMobile] = useState(window.innerWidth <= 768);

  window.addEventListener('resize', () => {
    setIsMobile(window.innerWidth <= 768);
  });
  const [activeData, setActiveData] = useState<any>({}); // 交互数据
  const [dataModalType, setDataModalType] = useState<number | string>(0);
  const [isGuideShow, setIsGuideShow] = useState<any>(false);
  const [modalShow, setModalShow] = useState<any>({
    isThumbnailShow: false,
    isIntroShow: false,
    isListShow: false,
    isGuideShow: false,
  });
  const [instanceConf, setInstanceConf] = useState<any>({});
  const [panoramaData, setPanoramaData] = useState<any>({});
  const [toPanoramaId, setToPanoramaId] = useState<any>();
  const [allPanoramas, setAllPanoramas] = useState<API.PanoramaVo[]>([]);
  const [openViewId, setOpenViewId] = useState<any>(null);

  const [dialogInfo, setDialogInfo] = useState<any>({});
  const [sound, setSound] = useState<boolean>(false);
  const [videoSrc, setVideoSrc] = useState('');

  return {
    modalShow,
    setModalShow,
    isMobile,
    setIsMobile,
    isGuideShow,
    setIsGuideShow,
    instanceConf,
    setInstanceConf,
    panoramaData,
    setPanoramaData,
    toPanoramaId,
    setToPanoramaId,
    allPanoramas,
    setAllPanoramas,
    dataModalType,
    setDataModalType,
    dialogInfo,
    setDialogInfo,
    activeData,
    setActiveData,
    sound,
    setSound,
    videoSrc,
    setVideoSrc,
    openViewId,
    setOpenViewId,
  };
};
