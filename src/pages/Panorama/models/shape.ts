import { useState } from 'react';
import * as THREE from 'three';
import arrowRightImg from '@/assets/images/common/arrow-right.png';
import { Draw } from '@/utils/three/xThree';
import { isEmpty } from 'lodash';
import { findDrawByMap } from '@/services/swagger/DrawController';

let faceOp = 0.5;

let shape: any;
export default () => {
  const [shapeInfo, setShapeInfo] = useState<any>([]);
  const [selectedShape, setSelectedShape] = useState<any>({});

  const changeLineStyle = (changeValues, id = null) => {
    const {
      lineLink,
      lineCurve,
      lineToFace,
      lineWidth,
      lineColor,
      faceColor,
      faceBlink,
      lineType,
      faceOpacity,
    } = changeValues;
    if (faceOpacity) {
      faceOp = faceOpacity;
    }
    const group = window.viewer.scene.getObjectByName('shape');
    if (group) {
      let line = group.getObjectByName('activeLine');
      let face = group.getObjectByName('activeFace');
      if (selectedShape && !isEmpty(selectedShape)) {
        line = group.getObjectByName('meshLine' + selectedShape.id);
        face = group.getObjectByName('meshFace' + selectedShape.id);
      }
      if (id) {
        line = group.getObjectByName('meshLine' + id);
        face = group.getObjectByName('meshFace' + id);
      }
      if (line) {
        const originalPositions = line.geometry.originalPositions;
        const lineMaterial = line.material;

        lineMaterial.opacity = 1.0;

        if (lineColor) {
          const color = new THREE.Color(lineColor);
          lineMaterial.color = color;
        }
        if (lineWidth) {
          lineMaterial.lineWidth = lineWidth;
        }

        // 闭环 (有问题需要修改)
        if (lineLink === true) {
          const lPoints: any = [];
          originalPositions?.forEach((item) => {
            lPoints.push(item);
          });
          lPoints.push(originalPositions[0], originalPositions[1], originalPositions[2]);
          line.geometry.setPoints(lPoints);
        } else if (lineLink === false) {
          line.geometry.setPoints(originalPositions);
        }

        // 面

        const f = id || selectedShape.id || 'activeFunc';
        const faceFuncId = 'face' + f;

        window.viewer.animateEventList.forEach((val, i) => {
          if (val.name === faceFuncId) window.viewer.animateEventList.splice(i, 1);
        });

        if (lineToFace === true) {
          face.visible = true;
        } else if (lineToFace === false) {
          face.visible = false;
        }

        if (faceBlink === true) {
          const faceAnimation = {
            name: faceFuncId,
            fun: () => {
              if (face.material) {
                const time = Date.now() * 0.001;
                const blendFactor = Math.sin(time * 2) * 0.5 + 0.5;
                face.material.opacity = (faceOp / 1) * blendFactor;
              }
            },
            content: window.viewer,
          };
          window.viewer.addAnimate(faceAnimation);
        } else if (faceBlink === false) {
          face.material.opacity = faceOp; // 默认透明度
          // if (faceAnimation) {
          //   face.material.opacity = faceOp; // 默认透明度
          //   window.viewer.removeAnimate(faceAnimation);
          // }
        }
        if (faceOp) {
          face.material.opacity = faceOp;
        }

        if (faceColor) {
          const color = new THREE.Color(faceColor);
          face.material.color = color;
        }

        // 曲线 - 曲率

        if (lineCurve) {
          const points = [];
          for (let i = 0; i < originalPositions.length; i += 3) {
            const x = originalPositions[i];
            const y = originalPositions[i + 1];
            const z = originalPositions[i + 2];
            points.push(new THREE.Vector3(x, y, z));
          }
          const curve = new THREE.CatmullRomCurve3(points, false, 'catmullrom', lineCurve);
          const curvePoints = curve.getPoints(100);
          line.geometry.setPoints(curvePoints);
        }

        // 线样式

        const i = id || selectedShape.id || 'activeFunc';
        const lineFuncId = 'line' + i;

        if (lineType !== null && lineType !== undefined) {
          lineMaterial.useMap = 0;
          lineMaterial.map = null;
          window.viewer.animateEventList.forEach((val, i) => {
            if (val.name === lineFuncId) window.viewer.animateEventList.splice(i, 1);
          });
          if (lineType === 2) {
            // 箭头
            const points = [];
            for (let i = 0; i < originalPositions.length; i += 3) {
              const x = originalPositions[i];
              const y = originalPositions[i + 1];
              const z = originalPositions[i + 2];
              points.push(new THREE.Vector3(x, y, z));
            }
            let totalLength = 0;
            for (let i = 0; i < points.length - 1; i++) {
              totalLength += points[i].distanceTo(points[i + 1]);
            }
            const count = totalLength * 2;
            lineMaterial.useMap = 1;
            const texture = new THREE.TextureLoader().load(arrowRightImg, function (tex) {
              tex.needsUpdate = true;
              tex.wrapS = tex.wrapT = THREE.RepeatWrapping;
              tex.repeat.set(1, 1);
            });
            lineMaterial.repeat = new THREE.Vector2(count, 1);
            lineMaterial.map = texture;
            const animation = {
              name: lineFuncId,
              fun: () => {
                lineMaterial.uniforms.offset.value.x -= (100 / count) * 0.00005;
              },
              content: window.viewer,
            };
            window.viewer.addAnimate(animation);
          } else if (lineType === 3) {
            // 闪烁线
            const animation = {
              name: lineFuncId,
              fun: () => {
                const time = Date.now() * 0.002;
                const blendFactor = Math.sin(time * 2) * 0.5 + 0.5;
                lineMaterial.opacity = blendFactor;
              },
              content: window.viewer,
            };
            window.viewer.addAnimate(animation);
          } else {
          }
        }
      }
    }
  };

  const initShape = () => {
    shape = new Draw(window.viewer);
  };

  const refreshShapeList = async (panoramaId) => {
    shape.removeAll();
    const res = await findDrawByMap({
      ems_draw_panoramaid: panoramaId,
    });
    if (res?.code === 200) {
      const list = res?.result?.list;
      const shapeInfoArr = list?.map((item) => {
        const points = JSON.parse(item.ems_draw_data);
        const form = JSON.parse(item.ems_draw_jsondata);
        // 线回显
        shape.creatLine(points, item.ems_draw_id);
        changeLineStyle(form, item.ems_draw_id);
        const obj = {
          points: points,
          form: form,
          id: item.ems_draw_id,
        };
        return obj;
      });
      setShapeInfo([...shapeInfoArr]);
    }
  };

  const startDrawLine = () => {
    shape.start();
  };

  const deleteLine = (id = null) => {
    shape.deleteLine(id);
  };

  const getLinePoints = (id, type) => {
    const points = shape.getLinePoints(id, type);
    return points;
  };
  return {
    changeLineStyle,
    shapeInfo,
    setShapeInfo,
    selectedShape,
    setSelectedShape,
    initShape,
    refreshShapeList,
    startDrawLine,
    deleteLine,
    getLinePoints,
  };
};
