import { useEffect, useState, useRef } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import styles from './index.less';
import { UnorderedListOutlined } from '@ant-design/icons';
import 'swiper/swiper-bundle.css';
import SwiperCore, { Scrollbar, Mousewheel } from 'swiper/core';
import { useParams, useModel } from 'umi';
import { Tabs, Space, Segmented, Spin } from 'antd';
import { createFromIconfontCN, CloseOutlined } from '@ant-design/icons';
import { findGroupByInstance } from '@/services/swagger/InstancePanoramaGroupController';
import { findPanoramaByMapV1 } from '@/services/swagger/panoramaController';

const IconFont = createFromIconfontCN({
  scriptUrl: '/iconfont/iconfont.js',
});
SwiperCore.use([Scrollbar, Mousewheel]);

const LayerManagement = () => {
  const swiperRef = useRef(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [items, setItems] = useState<any>([]);
  const [panoramaList, setPanoramaList] = useState<any>([]);
  const params: any = useParams();
  const { instanceId } = params;
  const [groupId, setGroupId] = useState<string | undefined>(null);

  const { isMobile } = useModel('mobile', (ret) => ({
    isMobile: ret.isMobile,
  }));

  const { panoramaData, setToPanoramaId, allPanoramas, isListShow, modalShow, setModalShow } =
    useModel('Panorama.data', (ret) => ({
      isListShow: ret.modalShow.isListShow,
      modalShow: ret.modalShow,
      setModalShow: ret.setModalShow,
      panoramaData: ret.panoramaData,
      setToPanoramaId: ret.setToPanoramaId,
      allPanoramas: ret.allPanoramas,
    }));

  const handleChange = (value: string[]) => {
    console.log(value);
  };

  const onChange = (key: string) => {
    const childrens = items?.find((item) => item.ems_instance_panorama_group_id == key).childrens;
    if (childrens.length > 0) {
      refreshList(childrens[0].ems_instance_panorama_group_id);
    }
  };

  const onChangeChildren = (key: string) => {
    refreshList(key);
  };

  const handleToPanorama = (toPanoramaId: any) => {
    setToPanoramaId(toPanoramaId);
  };

  /**
   * 分组
   */
  const getGroupList = () => {
    findGroupByInstance({
      ems_instance_panorama_group_instanceid: instanceId,
    }).then((res: any) => {
      setItems(res.result || []);
      if (res.result && res.result?.length > 0) {
        if (res.result[0].childrens?.length > 0) {
          const groupId = res.result[0].childrens[0].ems_instance_panorama_group_id;
          refreshList(groupId);
        }
      }
    });
  };

  /**
   * 过滤列表，防止初始groupId没查询出来前加载全部
   * @param groupId
   */
  const refreshList = async (groupId) => {
    setGroupId(groupId);
    setLoading(true);
    setPanoramaList([]);
    const { result } = await findPanoramaByMapV1({
      ems_panorama_instanceid: instanceId,
      ems_panorama_groupid: groupId,
    });

    const list = result?.list || [];
    setLoading(false);
    setPanoramaList(list);
  };

  useEffect(() => {
    if (isListShow) {
      getGroupList();
    }
  }, [isListShow]);

  return (
    <>
      <div
        className={
          isListShow ? `${styles.layerBoxList} ${styles.layerBoxListSelected}` : styles.layerBoxList
        }
        onClick={() => {
          modalShow.isListShow = !modalShow.isListShow;
          setModalShow({
            isThumbnailShow: modalShow.isThumbnailShow,
            isIntroShow: false,
            isListShow: modalShow.isListShow,
            isGuideShow: false,
          });
        }}
      >
        <UnorderedListOutlined className={styles.layerBoxListIcon} />
        <div>场景</div>
      </div>
      {isListShow && !isMobile && (
        <div className={styles.layerBox}>
          <div className={styles.layerTitle}>
            <Space>
              <IconFont type="icon-shijian" style={{ color: '#fff', fontSize: 18 }} />
              <span style={{ color: '#fff', fontSize: 14 }}>2025</span>
              <Segmented
                className="ant-segmented-gray"
                onChange={() => {}}
                defaultValue={3}
                options={[
                  { label: '春', value: 0 },
                  { label: '夏', value: 1 },
                  { label: '秋', value: 2 },
                  { label: '冬', value: 3 },
                ]}
              />
            </Space>
          </div>
          <CloseOutlined
            onClick={() => {
              modalShow.isListShow = !modalShow.isListShow;
              setModalShow({
                isThumbnailShow: modalShow.isThumbnailShow,
                isIntroShow: false,
                isListShow: modalShow.isListShow,
                isGuideShow: false,
              });
            }}
            className={styles.btnClose}
          />
          <Tabs
            onChange={onChange}
            centered
            className={styles.layerBoxTabs}
            items={items.map((item) => {
              return {
                label: item.ems_instance_panorama_group_name,
                key: item.ems_instance_panorama_group_id,
                children: (
                  <>
                    <Tabs
                      activeKey={groupId}
                      onChange={onChangeChildren}
                      centered
                      className={styles.layerBoxTabs2}
                      items={item.childrens.map((item) => {
                        return {
                          label: item.ems_instance_panorama_group_name,
                          key: item.ems_instance_panorama_group_id,
                        };
                      })}
                    />
                  </>
                ),
              };
            })}
          />
          {panoramaList?.length > 0 ? (
            <Swiper
              // centeredSlides={panoramaList?.length < 4 ? true : false}
              className={styles.layerSwiperBox}
              direction="horizontal"
              slidesPerView={4}
              freeMode={true}
              scrollbar={{}}
              autoplay={false}
              spaceBetween={10}
              lazy={true}
              mousewheel={true}
            >
              {panoramaList.map((item) => {
                return (
                  <SwiperSlide
                    key={item.ems_panorama_id}
                    className={`${
                      item.ems_panorama_id == panoramaData?.ems_panorama_id
                        ? styles.selectedImgBox
                        : ''
                    } ${styles.layerSwiperBoxItem}`}
                    onClick={(e) => {
                      handleToPanorama(item.ems_panorama_id);
                    }}
                  >
                    <img
                      src={`/systemfile${item?.ems_panorama_cover?.ems_sysfile_path}`}
                      alt=""
                      loading="lazy"
                    />
                    <span>{item.ems_panorama_name}</span>
                  </SwiperSlide>
                );
              })}
            </Swiper>
          ) : (
            <div
              style={{
                width: 650,
                height: 108,
                color: '#fff',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Spin spinning={loading} />
            </div>
          )}
        </div>
      )}
    </>
  );
};
export default LayerManagement;
