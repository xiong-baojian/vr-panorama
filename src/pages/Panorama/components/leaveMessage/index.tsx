import React, { useState, useRef, useEffect } from 'react';
import { useModel, useParams } from 'umi';
import { Avatar, List, Typography, Space, Input, Button, message } from 'antd';
import { Css2dObject } from '@/utils/three/xThree';
import {
  CloseOutlined,
  MessageOutlined,
  LikeOutlined,
  DislikeOutlined,
  ShareAltOutlined,
  SendOutlined,
  CloseCircleOutlined,
  CaretDownOutlined,
  CaretUpOutlined,
} from '@ant-design/icons';
import { findMessageByMap, insertMessage } from '@/services/swagger/messageController';
import { insertReplies, findRepliesByMap } from '@/services/swagger/repliesController';
import { insertOrDeleteRating } from '@/services/swagger/ratingController';
import { Camera } from '@/utils/three/xThree';
import Ws from '@/utils/ws';
const { Text } = Typography;
const { TextArea } = Input;
import styles from './index.less';
import { isEmpty } from 'lodash';
import e from 'express';
let cmessage: API.Message | null = {}; // 动态改变
let myWebSocket;
const LeaveMessage: React.FC = () => {
  const params: any = useParams();
  const { instanceId } = params;
  const listItemRefs = useRef<any>({}); // 用来存储每个列表项的引用
  const textRef = useRef<any>(null);
  const divObject = useRef<any>(null);
  const [isExpandId, setIsExpandId] = useState<any>(null);
  const [currentMessage, setCurrentMessage] = useState<API.MessageVo>({});
  const [leaveMessageInfo, setLeaveMessageInfo] = useState({
    text: '',
    defalutText: '请输入留言内容!',
  });
  const [messageList, setMessageList] = useState<any>([]);
  const [allMessageList, setAllMessageList] = useState<any>([]);

  const [replyList, setReplyList] = useState<any>([]);
  const [type, setType] = useState<number>(0);
  const leaveMessageRef = useRef(null);
  const currentObject = useRef<any>(null);

  const { isLeaveMessageShow, modalShow, setModalShow, panoramaData, setToPanoramaId } = useModel(
    'Panorama.data',
    (ret) => ({
      isLeaveMessageShow: ret.modalShow.isLeaveMessageShow,
      modalShow: ret.modalShow,
      setModalShow: ret.setModalShow,
      panoramaData: ret.panoramaData,
      setToPanoramaId: ret.setToPanoramaId,
    }),
  );

  /**
   * 图标 + 文字
   * @param param0
   * @returns
   */
  const IconText = ({ icon, text }: { icon: React.FC; text: string }) => (
    <Space className={styles.iconText}>
      {React.createElement(icon)}
      {text}
    </Space>
  );

  /**
   * 初始化添加正在留言
   */
  const addLeaveMessageObject = () => {
    if (divObject.current) {
      const leaveMessage: any = leaveMessageRef.current;
      const element: any = leaveMessage?.cloneNode(true);
      element.classList.add('currentMessage');
      currentObject.current = divObject.current.add(element, {
        name: 'selectedMessage',
        draggable: true,
        show: false,
      });
    }
  };

  /**
   * 移除 正在留言
   */
  const removeLeaveMessage = () => {
    if (divObject.current) {
      divObject.current.remove('name', 'selectedMessage');
    }
  };

  /**
   * 聚焦事件
   */
  const onFocus = () => {
    // 判断是否是 回复留言
    if (cmessage && !isEmpty(cmessage)) {
      divObject.current.show('name', 'selectedMessage', false);
    } else {
      divObject.current.show('name', 'selectedMessage', true);
    }
  };

  /**
   * 清除样式
   */
  const clearStyle = () => {
    const elements = document.querySelectorAll('.lm');
    elements.forEach((element) => {
      element.classList.remove('selectedMessageStyle');
    });
  };

  /**
   * 展开留言
   */
  const showDrawer = (flag) => {
    if (flag) {
      modalShow.isLeaveMessageShow = true;
      removeLeaveMessage();
      addLeaveMessageObject();
    } else {
      modalShow.isLeaveMessageShow = false;
      // 关闭
      cmessage = {};
      setCurrentMessage({});
      removeLeaveMessage();
      clearStyle();
      setLeaveMessageInfo({
        text: '',
        defalutText: '请输入留言内容',
      });
    }
    setModalShow({
      isIntroShow: false,
      isListShow: false,
      isGuideShow: false,
      isThumbnailShow: modalShow.isThumbnailShow,
      isLeaveMessageShow: modalShow.isLeaveMessageShow,
    });
  };

  /**
   * 查询回复
   */
  const queryReplys = async (message: API.MessageVo) => {
    const { result } = await findRepliesByMap({
      ems_replies_auditstatus: 2,
      ems_replies_messageid: message.ems_message_id,
    });
    const list = result?.list || [];
    setReplyList(list);
    return list;
  };

  /**
   * 展开回复
   * @param message
   */
  const handleExpand = (message: API.MessageVo) => {
    if (isExpandId && isExpandId === message.ems_message_id) {
      setIsExpandId(null);
    } else {
      setIsExpandId(message.ems_message_id);
    }
    queryReplys(message);
  };

  /**
   * 发布留言保存
   */
  const saveMessage = async () => {
    if (!leaveMessageInfo.text) {
      message.info('请输入！');
      return;
    }
    // 回复
    if (currentMessage && !isEmpty(currentMessage)) {
      // 回复的恢复
      if (currentMessage.type === 'reply') {
        const { code, message: m } = await insertReplies({
          ems_replies_messageid: currentMessage.ems_message_id,
          ems_replies_info: leaveMessageInfo.text,
          ems_replies_instanceid: currentMessage.ems_message_instanceid,
          ems_replies_panoramaid: currentMessage.ems_message_panoramaid,
          ems_replies_pid: currentMessage.ems_message_replies_id,
          ems_replies_repliesuserid: currentMessage.ems_message_replies_userid,
        });
        if (code === 200) {
          message.success(m);
        } else {
          message.error(m);
        }
      } else {
        const { code, message: m } = await insertReplies({
          ems_replies_messageid: currentMessage.ems_message_id,
          ems_replies_info: leaveMessageInfo.text,
          ems_replies_instanceid: currentMessage.ems_message_instanceid,
          ems_replies_panoramaid: currentMessage.ems_message_panoramaid,
          ems_replies_pid: 0,
          ems_replies_repliesuserid: currentMessage.ems_message_userid,
        });
        if (code === 200) {
          message.success(m);
        } else {
          message.error(m);
        }
      }

      await queryReplys(currentMessage);
      queryMessage();
      selectMessage(null);
      setLeaveMessageInfo({
        text: '',
        defalutText: '请输入留言内容',
      });
    } else {
      // 留言
      const position = JSON.stringify(currentObject.current?.position) || '';
      const { code, message: m } = await insertMessage({
        ems_message_info: leaveMessageInfo.text,
        ems_message_panoramaid: panoramaData?.ems_panorama_id,
        ems_message_instanceid: panoramaData?.ems_panorama_instanceid,
        ems_message_location: position,
      });
      if (code === 200) {
        message.success('发布成功！');
      } else {
        message.error(m);
      }
      // initAllMessageList();
      // selectMessage(null);
      showDrawer(false);
    }
  };

  const addMessageObject = ({
    imgUrl = '',
    info = '',
    position = null,
    draggable = false,
    name = 'leaveMessage',
  }) => {
    const leaveMessageInfo = info?.length > 12 ? info?.substring(0, 13) + '...' : info;
    const leaveMessage: any = leaveMessageRef.current?.cloneNode(true);

    if (!leaveMessage) {
      return;
    }

    const dataBox = leaveMessage.querySelector('.leaveMessageInfo');
    dataBox.textContent = leaveMessageInfo;

    const imgBox = leaveMessage.querySelector('.leaveMessageImg');
    imgBox.src = imgUrl;

    leaveMessage.style.display = 'flex !important';
    divObject.current.add(leaveMessage, {
      position,
      name,
      draggable: draggable,
    });
  };

  /**
   * 回复消息
   */
  const replyMessage = async (reply: API.RepliesVo) => {
    const message = allMessageList?.find(
      (item) => item.ems_message_id === reply.ems_replies_messageid,
    );
    message.ems_message_replies_id = reply.ems_replies_id;
    message.ems_message_replies_userid = reply.ems_replies_repliesuserid;
    message.ems_message_createby = reply.ems_replies_createby;
    message.type = 'reply';
    selectMessage(message);
  };

  /**
   * 移动到该条留言视角
   * @param position
   */
  const moveTo = (message) => {
    setTimeout(() => {
      if (listItemRefs.current[message.ems_message_id] && message.type !== 'reply') {
        listItemRefs.current[message.ems_message_id].scrollIntoView({
          behavior: 'smooth',
          block: 'start',
        });
      }
    }, 50);

    if (message && message.ems_message_location) {
      // const panoramaId = message.ems_message_panoramaid;
      const point = JSON.parse(message.ems_message_location);
      Camera.flyTo({
        position: point,
      });
    }
  };

  /**
   * 查询留言
   */
  const queryMessage = async (panoramaId = panoramaData?.ems_panorama_id) => {
    const { result } = await findMessageByMap({
      ems_message_auditstatus: 2, // 已审核
      ems_message_instanceid: panoramaData?.ems_panorama_instanceid,
      ems_message_panoramaid: panoramaId,
    });
    setMessageList(result?.list || []);
    if (!panoramaId) {
      return;
    }

    if (divObject.current) {
      divObject.current.removeAll();
    }
    result?.list?.forEach((item) => {
      const url = item.ems_message_useravatarfile?.ems_sysfile_path
        ? '/systemfile' + item.ems_message_useravatarfile?.ems_sysfile_path
        : require('@/assets/images/panorama/icon/user.jpg');
      addMessageObject({
        imgUrl: url,
        info: item.ems_message_info,
        position: JSON.parse(item.ems_message_location || '{x:25,y:0.35,z:-12.82}'),
        draggable: false,
        name: 'lmessage' + item.ems_message_id,
      });
    });
  };

  /**
   *  初始化ws
   */
  const initWs = () => {
    const wsParams = {
      url: `ws://${location.host}/wsUrl/message/realtimeDisplay?panoramaid=${panoramaData.ems_panorama_id}`,
      callback: (data: string) => {
        if (data) {
          initAllMessageList();
          selectMessage(null);
        }
      },
    };
    myWebSocket = new Ws(wsParams);
    myWebSocket.connect();
  };

  /**
   * 选中留言
   */
  const selectMessage = (message) => {
    if (
      message &&
      !isEmpty(message) &&
      message?.ems_message_panoramaid !== panoramaData?.ems_panorama_id
    ) {
      selectMessage(null);
      setToPanoramaId(message?.ems_message_panoramaid);
      return;
    }

    cmessage = message;
    clearStyle();

    if (
      (message && !isEmpty(message) && currentMessage.ems_message_id !== message.ems_message_id) ||
      (message && !isEmpty(message) && message.type === 'reply')
    ) {
      if (textRef.current) {
        textRef.current.focus();
      }
      moveTo(message);

      setCurrentMessage({ ...message });
      // 标签样式
      const el = divObject.current.findMeshElementById(message.ems_message_id);
      el.classList.add('selectedMessageStyle');
    } else if (message && !isEmpty(message) && currentMessage.type === 'reply') {
      if (textRef.current) {
        textRef.current.focus();
      }
      moveTo(message);
      message.ems_message_replies_id = null;
      setCurrentMessage({ ...message });
      // 标签样式
      const el = divObject.current.findMeshElementById(message.ems_message_id);
      el.classList.add('selectedMessageStyle');
    } else {
      setCurrentMessage({});
      cmessage = {};
    }
  };
  /**
   * 初始化选中事件
   */
  const initSelect = (list) => {
    divObject.current.select((el) => {
      if (el) {
        if (el.classList.contains('currentMessage')) {
          return;
        }
        showDrawer(true);
        // 选中
        const classList = el.classList?.value?.split(' ');
        const messageId = classList
          ?.find((item) => item.includes('lmessage'))
          ?.replace('lmessage', '');
        const message = list?.find((item) => item.ems_message_id == messageId);
        if (message && !isEmpty(message) && cmessage?.ems_message_id !== message.ems_message_id) {
          cmessage = message;
        } else {
          cmessage = null;
        }
        selectMessage(cmessage);
      } else {
        // 未选中
        cmessage = null;
        showDrawer(false);
      }
    });
  };

  /**
   * 初始化所有message 和 选中事件
   */
  const initAllMessageList = () => {
    findMessageByMap({
      ems_message_auditstatus: 2, // 已审核
      ems_message_instanceid: instanceId,
    }).then((res) => {
      const list = res.result?.list;
      setAllMessageList(list);
    });
  };

  /**
   * 监听 切换全景
   */
  useEffect(() => {
    if (window.panorama && panoramaData?.ems_panorama_id && allMessageList.length > 0) {
      queryMessage();
      initWs();
      // 选中事件
      initSelect(allMessageList);
    }
  }, [window.panorama, panoramaData, allMessageList]);

  /**
   * 初始化
   */
  useEffect(() => {
    if (window.viewer) {
      divObject.current = new Css2dObject(window.viewer);
      initAllMessageList();
    }
  }, [window.viewer]);

  //----
  const handleMessageLike = async (message: API.MessageVo, flag: boolean) => {
    await insertOrDeleteRating({
      ems_rating_messageid: message.ems_message_id,
      ems_rating_userid: message.ems_message_userid,
      ems_rating_id: flag ? 1 : 0,
    });
    await queryReplys(currentMessage);
    queryMessage();
  };

  return (
    <>
      <div
        ref={leaveMessageRef}
        className={`${styles.leaveMessageBox} lm`}
        style={{ display: 'none' }}
      >
        <img
          src={require('@/assets/images/panorama/icon/user.jpg')}
          alt=""
          style={{ width: 26, height: 26, marginRight: 6, borderRadius: '50%' }}
          className="leaveMessageImg"
        />
        <span className="leaveMessageInfo">
          {leaveMessageInfo?.defalutText?.length > 12
            ? leaveMessageInfo?.defalutText?.substring(0, 13) + '...'
            : leaveMessageInfo?.defalutText}
        </span>
      </div>
      <div
        className={styles.leaveMessage}
        onClick={() => {
          modalShow.isLeaveMessageShow = !modalShow.isLeaveMessageShow;
          showDrawer(modalShow.isLeaveMessageShow);
        }}
        style={{
          cursor: 'pointer',
          userSelect: 'none',
          color: `${modalShow.isLeaveMessageShow ? '#1890ff' : '#fff'}`,
        }}
      >
        <MessageOutlined className={styles.leaveMessageIcon} />
        <div>留言</div>
      </div>
      {modalShow.isLeaveMessageShow && (
        <div className={styles.leaveMessageOption}>
          <div className={styles.leaveMessageOptionHeader}>
            <Space>
              <div
                style={{ color: type === 0 ? '#1890ff' : 'rgba(255, 255, 255, 0.8)' }}
                className={styles.leaveMessageOptionTitle}
                onClick={() => {
                  setType(0);
                  selectMessage(null);
                }}
              >
                当前场景({messageList.length}条)
              </div>
              <div
                style={{ color: type === 1 ? '#1890ff' : 'rgba(255, 255, 255, 0.8)' }}
                className={styles.leaveMessageOptionTitle}
                onClick={() => {
                  setType(1);
                  selectMessage(null);
                }}
              >
                全部场景({allMessageList.length}条)
              </div>
            </Space>
            <CloseOutlined
              onClick={() => {
                showDrawer(false);
              }}
              className={styles.leaveMessageOptionClose}
            />
          </div>
          <Space direction="vertical" style={{ width: '100%' }}>
            <div className={styles.messageList}>
              <List
                className={styles.messageListContent}
                itemLayout="vertical"
                size="small"
                id="ems_message_id"
                pagination={false}
                dataSource={type === 0 ? messageList : allMessageList}
                renderItem={(item: API.MessageVo) => (
                  <div
                    key={item.ems_message_id}
                    style={{ marginBottom: 8 }}
                    ref={(el) => (listItemRefs.current[item.ems_message_id] = el)}
                  >
                    <List.Item
                      key={item.ems_message_id}
                      actions={[
                        <IconText
                          icon={LikeOutlined}
                          text={item.ems_message_likescount}
                          key="list-vertical-like-o"
                        />,
                        <IconText
                          icon={DislikeOutlined}
                          text={item.ems_message_treadcount}
                          key="list-vertical-like-o"
                        />,
                        <Text
                          className={
                            currentMessage &&
                            currentMessage.ems_message_id === item.ems_message_id &&
                            currentMessage.type !== 'reply'
                              ? styles.currentIconText
                              : styles.iconText
                          }
                          onClick={() => {
                            item.type = 'null';
                            selectMessage(item);
                          }}
                        >
                          <MessageOutlined />
                          &nbsp;&nbsp;
                          {currentMessage &&
                          currentMessage.ems_message_id === item.ems_message_id &&
                          currentMessage.type !== 'reply'
                            ? '回复中'
                            : '回复'}
                        </Text>,
                      ]}
                    >
                      <List.Item.Meta
                        avatar={
                          <Avatar
                            src={`/systemfile${item.ems_message_useravatarfile?.ems_sysfile_path}`}
                          />
                        }
                        title={
                          <Text ellipsis style={{ color: 'rgba(255, 255, 255, 0.6)' }}>
                            {item.ems_message_createby}
                          </Text>
                        }
                        description={
                          <span
                            className={
                              currentMessage &&
                              currentMessage.ems_message_id === item.ems_message_id &&
                              currentMessage.type !== 'reply'
                                ? styles.currentMessageStyle
                                : styles.messageStyle
                            }
                            onClick={() => {
                              item.type = 'null';
                              selectMessage(item);
                            }}
                          >
                            {item.ems_message_info}
                          </span>
                        }
                      />
                    </List.Item>
                    {/* 回复------------------------------------------- */}
                    {item.ems_message_repliescount !== 0 && (
                      <div>
                        {isExpandId && isExpandId === item.ems_message_id && (
                          <List
                            style={{ marginLeft: 50 }}
                            itemLayout="vertical"
                            size="small"
                            id="ems_replies_id"
                            pagination={false}
                            dataSource={replyList}
                            renderItem={(jtem: API.RepliesVo) => (
                              <div key={jtem.ems_replies_id} style={{ marginBottom: 8 }}>
                                <List.Item
                                  key={jtem.ems_replies_id}
                                  actions={[
                                    <IconText
                                      icon={LikeOutlined}
                                      text={jtem.ems_replies_likescount}
                                      key="list-vertical-like-o"
                                    />,
                                    <IconText
                                      icon={DislikeOutlined}
                                      text={jtem.ems_replies_treadcount}
                                      key="list-vertical-like-o"
                                    />,
                                    <Text
                                      className={
                                        currentMessage &&
                                        currentMessage.ems_message_replies_id ===
                                          jtem.ems_replies_id &&
                                        currentMessage.type === 'reply'
                                          ? styles.currentIconText
                                          : styles.iconText
                                      }
                                      onClick={() => {
                                        replyMessage(jtem);
                                      }}
                                    >
                                      <MessageOutlined />
                                      &nbsp;&nbsp;
                                      {currentMessage &&
                                      currentMessage.ems_message_replies_id ===
                                        jtem.ems_replies_id &&
                                      currentMessage.type === 'reply'
                                        ? '回复中'
                                        : '回复'}
                                    </Text>,
                                  ]}
                                >
                                  <List.Item.Meta
                                    avatar={
                                      <Avatar
                                        src={`/systemfile${jtem.ems_replies_useravatarfile?.ems_sysfile_path}`}
                                      />
                                    }
                                    title={
                                      <Text ellipsis style={{ color: 'rgba(255, 255, 255, 0.6)' }}>
                                        {jtem.ems_replies_createby}
                                      </Text>
                                    }
                                    description={
                                      <span
                                        className={
                                          currentMessage &&
                                          currentMessage.ems_message_replies_id ===
                                            jtem.ems_replies_id &&
                                          currentMessage.type === 'reply'
                                            ? styles.currentMessageStyle
                                            : styles.messageStyle
                                        }
                                        onClick={() => {
                                          replyMessage(jtem);
                                        }}
                                      >
                                        {jtem.ems_replies_info}
                                      </span>
                                    }
                                  />
                                </List.Item>
                              </div>
                            )}
                          />
                        )}
                        <Text
                          className={styles.expandBtn}
                          onClick={() => {
                            handleExpand(item);
                          }}
                        >
                          {isExpandId && isExpandId === item.ems_message_id ? (
                            <>
                              ---- 收起&nbsp;
                              <CaretUpOutlined />
                            </>
                          ) : (
                            <>
                              ---- 展开{item.ems_message_repliescount}条回复&nbsp;
                              <CaretDownOutlined />
                            </>
                          )}
                        </Text>
                      </div>
                    )}
                  </div>
                )}
              />
              <div className={styles.messageSend}>
                {currentMessage && !isEmpty(currentMessage) && (
                  <div className={styles.replyState}>
                    <span>回复 @{currentMessage.ems_message_createby}</span>
                    <CloseCircleOutlined
                      className={styles.replyStateIcon}
                      onClick={(e) => {
                        e.stopPropagation();
                        setLeaveMessageInfo({
                          text: '',
                          defalutText: '请输入留言内容',
                        });
                        if (currentObject.current && currentObject.current.element) {
                          const leaveMessage = currentObject.current.element;
                          const dataBox = leaveMessage.querySelector('.leaveMessageInfo');
                          dataBox.textContent = '请输入留言内容';
                        }
                        selectMessage(null);
                      }}
                    />
                  </div>
                )}
                <Input
                  onFocus={onFocus}
                  ref={textRef}
                  placeholder="请输入留言！"
                  className={styles.messageSendInput}
                  value={leaveMessageInfo.text}
                  onChange={(e) => {
                    setLeaveMessageInfo({
                      text: e.target.value,
                      defalutText: e.target.value,
                    });
                    if (currentObject.current && currentObject.current.element) {
                      const leaveMessage = currentObject.current.element;
                      const dataBox = leaveMessage.querySelector('.leaveMessageInfo');
                      dataBox.textContent = e.target.value;
                    }
                  }}
                  suffix={<SendOutlined onClick={saveMessage} style={{ color: '#fff' }} />}
                />
              </div>
            </div>
          </Space>
        </div>
      )}
    </>
  );
};

export default LeaveMessage;
