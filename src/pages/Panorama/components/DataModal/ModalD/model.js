import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';

import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer.js';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js';
import { OutlinePass } from 'three/examples/jsm/postprocessing/OutlinePass.js';
class Model {
  constructor(viewer) {
    this.viewer = viewer;
    this.meshs = new THREE.Group();
    this.init();
  }

  init() {
    this.meshs.name = 'modelGroup';
    this.viewer.scene.add(this.meshs);

    this.viewer.camera.position.set(-1.8, 0.6, 2.7);
    this.viewer.orbitControls.minDistance = 2;
    this.viewer.orbitControls.maxDistance = 10;
    this.viewer.orbitControls.target.set(0, 0, -0.2);
    this.viewer.orbitControls.autoRotateSpeed = 0.5;
    this.viewer.orbitControls.autoRotate = true;

    // this.addOutlinePass();
    // this.getMeshByClick((e) => {
    //   if (e) {
    //     this.outlinePass.selectedObjects = [e];
    //   }
    // });
  }

  addOutlinePass() {
    const scene = this.viewer.scene;
    const camera = this.viewer.camera;
    const renderer = this.viewer.renderer;
    const composer = new EffectComposer(renderer);
    const renderPass = new RenderPass(scene, camera);
    composer.addPass(renderPass);
    const outlinePass = new OutlinePass(
      new THREE.Vector2(window.innerWidth, window.innerHeight),
      scene,
      camera,
    );
    outlinePass.edgeStrength = 2.5; // 轮廓的强度
    outlinePass.edgeGlow = 0.5; // 轮廓的光晕
    outlinePass.edgeThickness = 2.0; // 轮廓的厚度
    outlinePass.visibleEdgeColor.set('#ff0000'); // 轮廓的颜色
    outlinePass.hiddenEdgeColor.set('#190a05'); // 隐藏边缘的颜色
    composer.addPass(outlinePass);
    this.outlinePass = outlinePass;
  }

  remove() {
    this.viewer.scene.remove(this.meshs);
  }

  add() {
    const gltfLoader = new GLTFLoader();
    let url = require('@/assets/model/glb/factory_low.glb');
    gltfLoader.load(url, (gltf) => {
      // gltf.scene.rotation.y = Math.PI;
      let model = gltf.scene;
      model.traverse(function (object) {
        if (object.isMesh) {
          object.castShadow = true;
          object.receiveShadow = true;
          object.material.depthTest = true;
        }
      });
      model.scale.set(0.0004, 0.0004, 0.0004);
      model.rotation.set(0, (-30 / 180) * Math.PI, 0);
      model.position.set(-1, -0.2, 0);
      this.meshs.add(model);
    });
  }

  getMeshByClick(callback) {
    this.viewer.container.addEventListener('click', (event) => {
      const Sx = event.offsetX;
      const Sy = event.offsetY;
      const x = (Sx / this.viewer.container?.offsetWidth) * 2 - 1;
      const y = (-Sy / this.viewer.container?.offsetHeight) * 2 + 1;
      var raycaster = new THREE.Raycaster();
      raycaster.setFromCamera(new THREE.Vector2(x, y), this.viewer.camera);
      var intersects = raycaster.intersectObjects(this.meshs.children, true);
      if (intersects.length > 0) {
        const res = intersects.filter(function (res) {
          return res && res.object;
        })[0];
        if (res && res.object) {
          if (res.object.selectAble == true) {
            const selectedObject = res.object;
          }
          callback(res.object);
        }
      } else {
      }
    });
  }

  animateObj(animationObj) {
    this.animaObject = {
      fun: animationObj,
      content: this,
    };
    this.viewer.addAnimate(this.animaObject);
  }
}
export { Model };
