import { CloseOutlined } from '@ant-design/icons';
import { Viewer } from '@/utils/three/xThree';
import { useEffect, useState } from 'react';
import { useModel } from 'umi';
import './index.less';
import './mobile.less';
import { Model } from './model.js';
let viewer: any;

const ModalD = () => {
  const { isMobile } = useModel('mobile', (ret) => ({
    isMobile: ret.isMobile,
  }));

  const { setDataModalType } = useModel('Panorama.data', (ret) => ({
    setDataModalType: ret.setDataModalType,
  }));

  const close = () => {
    setDataModalType(0);
  };

  useEffect(() => {
    viewer = new Viewer({
      domId: 'modelCanvas',
    });
    viewer.viewHelper.visible = true;
    const model = new Model(viewer);
    model.add();
    return () => {};
  }, []);

  return (
    <div className={`${isMobile ? 'ModalD' : 'ModalD'}`}>
      <div className="header">
        <CloseOutlined onClick={close} style={{ fontSize: 20 }} />
      </div>
      <div className="modal" id="modelCanvas"></div>
    </div>
  );
};

export default ModalD;
