import { useModel } from 'umi';
import ModalA from './ModalA';
import ModalB from './ModalB';
import ModalC from './ModalC';
import ModalD from './ModalD';
import ModalE from './ModalE';
import ModalVideo from './ModalVideo';
import './index.less';
import { useEffect } from 'react';
const DataModal = () => {
  const { dataModalType } = useModel('Panorama.data', (ret) => ({
    dataModalType: ret.dataModalType,
  }));

  useEffect(() => {}, [dataModalType]);

  return (
    <div
      className={`modalBox ${dataModalType !== 0 ? 'modalshowAnimation' : 'modalhideAnimation'}`}
    >
      {dataModalType === 1 && <ModalA />}
      {dataModalType === 2 && <ModalB />}
      {dataModalType === 3 && <ModalC />}
      {dataModalType === 4 && <ModalD />}
      {dataModalType === 5 && <ModalE />}
      {dataModalType === 'video' && <ModalVideo />}
    </div>
  );
};

export default DataModal;
