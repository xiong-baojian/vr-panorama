import { CloseOutlined } from '@ant-design/icons';
import { useEffect, useState } from 'react';
import { useModel } from 'umi';
import './index.less';
import './mobile.less';
import { Image, Space, Typography } from 'antd';
import { ProCard } from '@ant-design/pro-components';
const { Paragraph, Title } = Typography;
let viewer: any;

const ModalE = () => {
  const { isMobile } = useModel('mobile', (ret) => ({
    isMobile: ret.isMobile,
  }));

  const { instanceConf } = useModel('Panorama.data', (ret) => ({
    instanceConf: ret.instanceConf,
  }));

  const { setDataModalType } = useModel('Panorama.data', (ret) => ({
    setDataModalType: ret.setDataModalType,
  }));

  const close = () => {
    setDataModalType(0);
  };

  useEffect(() => {
    return () => {};
  }, []);

  return (
    <div className={`${isMobile ? 'ModalE' : 'ModalE'}`}>
      <div className="header">
        <CloseOutlined onClick={close} style={{ fontSize: 20 }} />
      </div>
      <ProCard
        split="vertical"
        style={{ maxWidth: 1200, margin: 'auto' }}
        className="ModalE-bg"
        boxShadow
      >
        <ProCard title="" colSpan="50%">
          <Space>
            <img src="/logo.png" alt="" style={{ width: 100 }} />
            <img
              className="img2"
              style={{ width: 200 }}
              src={
                instanceConf?.logoImgFile?.ems_sysfile_path
                  ? `/systemfile${instanceConf?.logoImgFile?.ems_sysfile_path}`
                  : '暂无'
              }
              alt=""
            />
          </Space>
          <div style={{ marginTop: 20 }}>
            <Title>垃圾焚烧工艺流程</Title>
            <Image
              width="100%"
              height={300}
              src={require('@/assets/images/panorama/dataModal/焚烧bg.jpg')}
            />
            <Paragraph ellipsis={{ rows: 10, expandable: true, symbol: '更多' }}>
              垃圾由垃圾车定期运入电厂，经设在厂区内的地磅进行计量后，自
              动卸入垃圾堆库。生产运行时，垃圾经桥式抓斗起重机转卸到炉前垃圾
              料斗，经垃圾给料机连续均匀送入焚烧炉内。同时粒度合格的辅助燃料
              煤，经计量后由输煤皮带送入炉前钢制大煤斗，通过炉前皮带称重式给
              煤机计量后送入炉膛内燃烧。炉膛整体由膜式水冷壁组成，在下部布置
              有水冷布风板及风帽。燃烧空气分为一、二次风，预热后的一次风经风
              帽小孔进入密相区使燃料开始燃烧，并将物料吹离布风板。二次风由床
              层上方的二次风口分层送入炉膛。烟气经悬浮段碰撞炉顶防磨层，部分
              粗物料返回密相区，烟气只携带细物料离开炉膛进入高温旋风筒分离器。
              进入高温旋风筒分离器的烟气经旋风筒分离后，细物料通过返料器返回
              炉膛后循环燃烧。分离后含少量飞灰的干净烟气通过上排气口流经过热
              器及尾部受热面后排出锅炉本体。锅炉混烧的垃圾和煤渣经燃烧后产生
              的炉渣，从布置的排渣口放出，直接落至冷渣器，经冷却后运至渣库，
              外运用于制砖。锅炉产生的高温烟气经受热面热交换产生过热蒸汽，最后
              垃圾焚烧发电设施试行版
              接入蒸汽母管用于发电。烟气经余热锅炉进入烟气净化主系统，烟气净化
              主系统由反应塔、袋式除尘器、引风机和烟道管组成。净化后的烟气中烟
              尘和有害成分降低到符合环境允许的排放浓度后，通过烟囱排入大气。
            </Paragraph>
          </div>
        </ProCard>
        <ProCard title="焚烧发电工艺流程图" headerBordered>
          <Image
            width="100%"
            height={300}
            src={require('@/assets/images/panorama/dataModal/焚烧.png')}
          />
          <div style={{ marginTop: 10 }}>
            <p>
              第一步：垃圾通过垃圾车投入到垃圾仓(装垃圾的仓库),垃圾仓内的大爪子(垃圾吊抓斗)进行搅拌，使垃圾发酵；
            </p>
            <p>第二步：发酵后的垃圾被投入到焚烧炉内，垃圾被点燃；</p>
            <p>
              第三步：垃圾点燃后产生大量的热、炉渣和烟气。锅炉里面的水烧热后，产生大量的水蒸汽，水蒸气推动汽轮机，汽轮器转动，进行发电，产生的电能就被利用了。
            </p>
            <p>第四步：炉渣经过搅拌机，被运送到炉渣仓，经过处理后，运送到填埋场。</p>
            <p>第五步：烟气经过飞灰处置装置处理后，被运送到填埋场。</p>
            <p>第六步：垃圾渗滤液经过五道工序的处理，脏水变成了清水，达到工业用水排放标准。</p>
          </div>
        </ProCard>
      </ProCard>
    </div>
  );
};

export default ModalE;
