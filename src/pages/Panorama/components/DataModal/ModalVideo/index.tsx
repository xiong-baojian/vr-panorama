import { CloseOutlined } from '@ant-design/icons';
import { useEffect, useState } from 'react';
import { useModel } from 'umi';
import './index.less';
import './mobile.less';

const ModalVideo = () => {
  const { isMobile } = useModel('mobile', (ret) => ({
    isMobile: ret.isMobile,
  }));

  const { setDataModalType, activeData } = useModel('Panorama.data', (ret) => ({
    setDataModalType: ret.setDataModalType,
    activeData: ret.activeData,
  }));

  const close = () => {
    setDataModalType(0);
  };

  useEffect(() => {}, []);

  const url = activeData?.form?.videoFile ? activeData?.form?.videoFile[0]?.url : '';

  return (
    <div className={`${isMobile ? 'ModalD' : 'ModalD'}`}>
      <div className="header">
        <CloseOutlined onClick={close} style={{ fontSize: 20 }} />
      </div>

      <video src={`${url}`} autoPlay controls></video>
    </div>
  );
};

export default ModalVideo;
