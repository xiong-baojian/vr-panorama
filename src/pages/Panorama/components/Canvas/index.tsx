import { useEffect, useState, useRef } from 'react';
import { useParams, useModel } from 'umi';
import { Spin, Progress } from 'antd';
import { Viewer, Panorama } from '@/utils/three/xThree';
import { findPanoramaByMapV1 } from '@/services/swagger/panoramaController';
import './index.less';
let panorama: any;
const PanoramaCanvas = () => {
  const [loading, setLoading] = useState(true);

  const params: any = useParams();
  const { instanceId } = params;

  const {
    setToPanoramaId,
    toPanoramaId,
    setPanoramaData,
    setAllPanoramas,
    allPanoramas,
    setDataModalType,
    setActiveData,
    isMobile,
  } = useModel('Panorama.data', (ret) => ({
    toPanoramaId: ret.toPanoramaId,
    setToPanoramaId: ret.setToPanoramaId,
    setPanoramaData: ret.setPanoramaData,
    setAllPanoramas: ret.setAllPanoramas,
    allPanoramas: ret.allPanoramas,
    setDataModalType: ret.setDataModalType,
    setActiveData: ret.setActiveData,
    isMobile: ret.isMobile,
  }));

  const { refreshObjects } = useModel('Panorama.object');

  const { initShape, refreshShapeList } = useModel('PanoramicTool.shape', (ret) => ({
    initShape: ret.initShape,
    refreshShapeList: ret.refreshShapeList,
  }));
  /**
   * 全景点击实体触发事件
   * @param list
   * @param panoramaData
   */
  const handleClickMesh = (list) => {
    panorama.cancelClickSelected();
    panorama.handleClickSelected((object: any) => {
      if (object && object.data) {
        const { type, form } = object.data;
        setActiveData(object.data);
        // 判断点击导航
        if (type === 'navigation') {
          const toPanoramaId = form.toPanoramaId;
          setToPanoramaId(toPanoramaId);
        } else if (type === 'commonLabel') {
          // 展示模版
          if (form.bindJsonData) {
            const json = JSON.parse(form.bindJsonData);
            const { template } = json;
            if (template) {
              setDataModalType(template);
            }
            return;
          }
        } else if (type === 'video') {
          setDataModalType('video');
        }
      } else {
        setActiveData({});
      }
    });
  };

  /**
   * 初始化全景
   */
  const initPanorama = async () => {
    await findPanoramaByMapV1({
      ems_panorama_default: 1,
      ems_panorama_instanceid: instanceId,
    }).then(async (res) => {
      const list = res?.result?.list || [];
      const panoramaData = list[0];
      if (panoramaData) renderPanoramaData(panoramaData);
    });
    // 查询所有全景
    findPanoramaByMapV1({
      ems_panorama_instanceid: instanceId,
    }).then(async (res) => {
      const list = res?.result?.list || [];
      setAllPanoramas(list);
    });
  };

  const getImgUrl = (files) => {
    return files.map((item) => {
      return '/systemfile' + item.ems_sysfile_path;
    });
  };

  /**
   * 渲染全景图片 、 热点数据
   * @param panoramaData
   */
  const renderPanoramaData = async (panoramaData: API.PanoramaViewVo) => {
    setPanoramaData(panoramaData);
    const panoramaImgs = {
      preview: '/systemfile' + panoramaData.ems_panorama_previewfile?.ems_sysfile_path,
      tileUrls: [
        getImgUrl(panoramaData.firstLevel.px),
        getImgUrl(panoramaData.firstLevel.nx),
        getImgUrl(panoramaData.firstLevel.py),
        getImgUrl(panoramaData.firstLevel.ny),
        getImgUrl(panoramaData.firstLevel.pz),
        getImgUrl(panoramaData.firstLevel.nz),
      ],
    };
    panorama.removeAll();

    let cameraPosition: any = JSON.parse(panoramaData?.ems_panorama_initview || '{}');
    await panorama.setPanoramaImg(panoramaImgs, cameraPosition);

    if (panoramaData.ems_panorama_fov) {
      let params = JSON.parse(panoramaData.ems_panorama_fov);
      if (isMobile) {
      } else {
        panorama.changeCameraParams(params);
      }
    }

    refreshObjects(panorama, panoramaData);

    // 添加图形object
    refreshShapeList(panoramaData.ems_panorama_id);

    // 添加 效果
    if (panoramaData.ems_panorama_id === 203) {
      panorama.addEffects();
    }
    if (panoramaData.ems_panorama_id === 252) {
      panorama.addEffects_252();
    }
    setLoading(false);
  };

  /**
   * 监听全景图详情数据切换，刷新热点相关数据提供给点击事件。
   */
  useEffect(() => {
    if (allPanoramas.length > 0 && panorama) {
      handleClickMesh(allPanoramas);
    }
  }, [allPanoramas]);

  /**
   * 监听切换全景 延迟1~2毫秒
   */
  useEffect(() => {
    if (toPanoramaId) {
      const panoramaData = allPanoramas?.find((item) => item.ems_panorama_id === toPanoramaId);

      if (panoramaData) {
        renderPanoramaData(panoramaData);
      }
    }
  }, [toPanoramaId]);

  // 初始化
  useEffect(() => {
    let viewer: any;
    viewer = new Viewer({
      domId: 'panoramaCanvas',
    });
    viewer.viewHelper.visible = false;
    panorama = new Panorama(viewer);
    console.log('123456789');

    window.viewer = viewer;
    window.panorama = panorama;
    initPanorama();
    // 图形绘制初始化
    initShape();
    return () => {
      panorama?.dispose();
    };
  }, []);

  return (
    <>
      {loading && (
        <div className="panorama-mask">
          <div className="panorama-mask-loading">
            <Spin size="large" />
          </div>
        </div>
      )}
      <div id="panoramaCanvas"></div>
    </>
  );
};
export default PanoramaCanvas;
