import { Button, Result } from 'antd';
import React from 'react';

const ExpirePage: React.FC = () => (
  <div
    style={{
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    }}
  >
    <Result
      title="哦豁，你来晚了，链接分享已过期，下次要早点哦！"
      extra={<Button type="primary" key="console"></Button>}
    ></Result>
  </div>
);
export default ExpirePage;
