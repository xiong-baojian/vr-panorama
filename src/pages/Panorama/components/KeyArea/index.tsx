import { useEffect, useState, useRef } from 'react';
import styles from './index.less';
import 'swiper/swiper-bundle.css';
import { useModel } from 'umi';
import { Space } from 'antd';
import { Camera } from '@/utils/three/xThree';

const KeyArea = () => {
  const { isMobile } = useModel('mobile', (ret) => ({
    isMobile: ret.isMobile,
  }));

  const { panoramaData, setToPanoramaId, allPanoramas, isListShow, modalShow, setModalShow } =
    useModel('Panorama.data', (ret) => ({
      isListShow: ret.modalShow.isListShow,
      modalShow: ret.modalShow,
      setModalShow: ret.setModalShow,
      panoramaData: ret.panoramaData,
      setToPanoramaId: ret.setToPanoramaId,
      allPanoramas: ret.allPanoramas,
    }));
  const [selectedItem, setSelectedItem] = useState(null);

  const areaList = [
    {
      id: 1,
      name: '集中焚烧处置车间',
    },
    {
      id: 2,
      name: '干化学消毒车间',
    },
  ];

  const handleClick = (item) => {
    if (selectedItem && item.id === selectedItem?.id) {
      return;
    }
    setSelectedItem(item);

    const cameraPosition = panoramaData.ems_panorama_initview
      ? JSON.parse(panoramaData.ems_panorama_initview)
      : null;
    if (cameraPosition) {
      //   Camera.flyTo({
      //     position: {
      //       x: -0.81,
      //       y: -17.31,
      //       z: 25.0,
      //     },
      //     fov: 50,
      //     duration: 0.3,
      //   });
    }
  };

  //   useEffect(() => {
  //     if (panoramaData) {
  //     }
  //   }, [panoramaData]);

  return (
    <>
      <div className={styles.keyAreas}>
        <Space direction="vertical">
          {areaList?.map((item) => (
            <div
              key={item.id}
              className={`${styles.keyArea} ${selectedItem?.id === item.id ? styles.selected : ''}`}
              onClick={() => handleClick(item)} // 只传递 id，避免重复的匿名函数
            >
              {item.name}
            </div>
          ))}
        </Space>
      </div>
    </>
  );
};
export default KeyArea;
