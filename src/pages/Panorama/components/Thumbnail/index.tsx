import { useModel, useParams } from 'umi';
import { CloseOutlined, createFromIconfontCN, ZoomInOutlined } from '@ant-design/icons';
import { Tabs } from 'antd';
import './index.less';
import { useEffect, useRef, useState } from 'react';
import SmallMap from './SmallMap';
import BigMap from './BigMap';

const IconFont = createFromIconfontCN({
  scriptUrl: '/iconfont/iconfont.js',
});

const items = [
  {
    key: '0',
    label: (
      <span>
        <IconFont type="icon-dituleiditu" />
        地图
      </span>
    ),
  },
  {
    key: '1',
    label: (
      <span>
        <IconFont type="icon-pingmiantu" />
        平面图
      </span>
    ),
  },
  {
    key: '2',
    label: (
      <span>
        <IconFont type="icon-a-3Dmoxing2" />
        3D模型
      </span>
    ),
  },
];

function Thumbnail(props) {
  const params: any = useParams();
  const bigMapRef: any = useRef(null);
  const { instanceId } = params;
  const { isThumbnailShow, setToPanoramaId } = useModel('Panorama.data', (ret) => ({
    isThumbnailShow: ret.modalShow.isThumbnailShow,
    setToPanoramaId: ret.setToPanoramaId,
  }));

  const [show, setShow] = useState(false);
  const [activeKey, setActiveKey] = useState<string>('1');

  const onChange = (activeKey: string) => {
    setActiveKey(activeKey);
    if (bigMapRef && bigMapRef.current) {
      bigMapRef.current.setMapType(activeKey);
    }
  };

  const changePanoramaId = (id) => {
    if (!id) {
      setShow(true);
    }
  };

  useEffect(() => {}, []);

  return (
    <>
      {isThumbnailShow && (
        <>
          {show && (
            <div className="thumbnailBox">
              <div className="header">
                <CloseOutlined
                  onClick={() => {
                    setShow(false);
                  }}
                  style={{ fontSize: 20 }}
                />
              </div>
              <div className="content">
                <div className="content-top">
                  <Tabs
                    size="large"
                    onChange={onChange}
                    items={items}
                    centered
                    activeKey={activeKey}
                    style={{ width: '100%' }}
                  />
                </div>
                <div className="content-bottom">
                  {(activeKey == '0' || activeKey == '1') && <BigMap ref={bigMapRef} />}
                </div>
              </div>
            </div>
          )}
          <div className={'thumbnail-fadeIn'}>
            <SmallMap changePanoramaId={changePanoramaId} />
            <ZoomInOutlined
              className="zoomBtn"
              onClick={() => {
                setShow(true);
              }}
            />
          </div>
        </>
      )}
    </>
  );
}

export default Thumbnail;
