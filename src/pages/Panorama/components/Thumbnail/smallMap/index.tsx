import { useModel, useParams } from 'umi';
import { useEffect, useRef, useState } from 'react';
import {
  initMap,
  unloadMap,
  setMapViewByType,
  setMapLayerByType,
  findMapLayersByType,
  clearLayer,
  addMarker,
  addInteractions,
  selectEventListener,
  setMarkerSelectedById,
  addOverlay,
} from '@/utils/openlayers/smallMap';
import { findAerialviewByMap } from '@/services/swagger/aerialviewController';
import { findLayoutByMap } from '@/services/swagger/layoutController';
import { isEmpty } from 'lodash';
import Magnifier from './Magnifier';

function SmallMap(props) {
  const params: any = useParams();
  const { instanceId } = params;
  const [markList, setMarkList] = useState<any>([]);
  const { setToPanoramaId, panoramaData } = useModel('Panorama.data', (ret) => ({
    setToPanoramaId: ret.setToPanoramaId,
    panoramaData: ret.panoramaData,
  }));

  const initMapMarks = async () => {
    const res = await findAerialviewByMap({
      ems_aerialview_type: 1, // 平面图
      ems_aerialview_instanceid: instanceId,
      ems_aerialview_isdefault: 1,
    });
    const list = res.result?.list || [];
    const viewData = list[0] || {};
    if (viewData && !isEmpty(viewData)) {
      setMapViewByType(1);
      const url = viewData.ems_aerialview_file?.ems_sysfile_path
        ? `/systemfile${viewData.ems_aerialview_file?.ems_sysfile_path}`
        : null;
      await setMapLayerByType(1, url);
      // 清除图层标记
      const layer = findMapLayersByType('markerLayer')[0];
      clearLayer(layer);
      const res = await findLayoutByMap({
        ems_layout_aerialviewid: viewData.ems_aerialview_id,
      });
      const list = res.result?.list || [];
      setMarkList(list);
      list?.forEach((item) => {
        const data = JSON.parse(item.ems_layout_data || '{}') || {};
        addMarker({
          id: item.ems_layout_id,
          name: data.title,
          titleShow: data.titleShow,
          coordinate: data.coordinate,
        });
      });
      selectEventListener((id) => {
        const mark = list?.find((item) => item.ems_layout_id === id);
        if (mark) {
          setToPanoramaId(mark.ems_layout_panoramaid);
        }
      });
    }
  };

  useEffect(() => {
    if (panoramaData && !isEmpty(panoramaData) && markList.length > 0) {
      const element: any = document.getElementById('magnifierBox');
      const panoramaId = panoramaData.ems_panorama_id;
      const mark = markList?.find((item) => item.ems_layout_panoramaid === panoramaId);
      if (mark && mark.ems_layout_id) {
        setMarkerSelectedById(mark.ems_layout_id);
        const data = JSON.parse(mark.ems_layout_data || '{}') || {};
        if (data && data.coordinate) {
          element.style.display = 'block';
          addOverlay(element, data.coordinate);
        }
      } else {
        element.style.display = 'none';
        setMarkerSelectedById(null);
      }
    }
  }, [panoramaData, markList]);

  useEffect(() => {
    initMap('thumbnailMap');
    addInteractions(['select']);
    initMapMarks();
    return () => {
      unloadMap();
    };
  }, []);

  return (
    <div id="thumbnailMap" style={{ width: '100%', height: '100%' }}>
      <Magnifier />
    </div>
  );
}

export default SmallMap;
