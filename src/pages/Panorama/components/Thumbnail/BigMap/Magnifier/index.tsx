import React, { useEffect, useState, useImperativeHandle } from 'react';
import { isEmpty } from 'lodash';
import { Camera } from '@/utils/three/xThree';
import { useModel } from 'umi';
import './index.less';

var len = 70;
var t = 0;
const Magnifier = React.forwardRef((props: any, ref: any) => {
  const [offset, setOffset] = useState<any>([0, 0]);
  const { panoramaData } = useModel('Panorama.data', (ret) => ({
    panoramaData: ret.panoramaData,
  }));

  const setPath = (angle) => {
    const fanPath: any = document.getElementById('bigFan');
    if (fanPath) {
      const angleInRadians = -(angle * Math.PI) / 180 / 2;
      const endX1 = len + len * Math.sin(angleInRadians);
      const endY1 = len - len * Math.cos(angleInRadians);
      const endX2 = len - len * Math.sin(angleInRadians);
      const endY2 = len - len * Math.cos(angleInRadians);

      // 更新路径的 d 属性，重新计算路径
      fanPath.setAttribute(
        'd',
        `M ${len},${len} L ${endX1},${endY1} A ${len},${len} 0 0,1 ${endX2},${endY2} Z`,
      );
    }
  };

  useImperativeHandle(ref, () => ({
    async handleSetOffset(offset) {
      setOffset(offset);
    },
  }));

  useEffect(() => {
    if (panoramaData && !isEmpty(panoramaData)) {
      const compassCoord = panoramaData.ems_panorama_compass;
      if (compassCoord) {
        const coord = JSON.parse(compassCoord);
        const theta = Camera.getPositionTheta(window.viewer, coord);
        t = theta;
        const compass: any = document.getElementById('bigSvg');
        if (compass) {
          compass.style.transform = 'rotateZ(' + -theta + 'deg)';
        }
      }
    }
  }, [panoramaData]);

  useEffect(() => {
    if (panoramaData && !isEmpty(panoramaData)) {
      const compass: any = document.getElementById('bigSvg');
      const compassCoord = panoramaData.ems_panorama_compass;
      if (compassCoord) {
        const coord = JSON.parse(compassCoord);
        if (coord) {
          const theta = Camera.getPositionTheta(window.viewer, coord);
          t = -theta;
        }
      } else {
        t = 0;
      }
      if (compass) {
        console.log(-t);
        compass.style.transform = 'rotateZ(' + -t + 'deg)';
      }
    }
  }, [panoramaData]);

  useEffect(() => {
    if (window.viewer) {
      const camera = window.viewer.camera;
      const fov = camera.fov;
      setPath(fov);
      window.viewer.container.addEventListener('wheel', (event) => {
        event.preventDefault();
        const camera = window.viewer.camera;
        const fov = camera.fov;
        setPath(fov);
      });

      Camera.getCameraTheta(window.viewer, (theta) => {
        if (theta) {
          const deg = theta + t;
          const compass: any = document.getElementById('bigSvg');
          if (compass) {
            compass.style.transform = 'rotateZ(' + deg + 'deg)';
          }
        }
      });
    }
    return () => {};
  }, [window.viewer]);

  return (
    <div
      className="magnifierBox"
      id="magnifierBigBox"
      style={{ width: `${len * 2}px`, height: `${len * 2}px` }}
    >
      <svg
        id="bigSvg"
        width={len * 2}
        height={len * 2}
        viewBox={`0 0 ${len * 2} ${len * 2}`}
        style={{ top: offset[1], left: offset[0] }}
      >
        <path
          id="bigFan"
          d={`M ${len},${len} L ${len},0 A ${len},${len} 0 0,1 ${len},0 Z`}
          stroke="rgb(0,255,0)"
          strokeWidth="0.5"
          strokeOpacity="0.5"
          fill="rgb(255,255,255)"
          fillOpacity="0.5"
        />
      </svg>
    </div>
  );
});

export default Magnifier;
