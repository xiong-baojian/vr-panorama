import { useModel, useParams } from 'umi';
import React, { useEffect, useImperativeHandle, useState } from 'react';
import {
  initMap,
  unloadMap,
  setMapViewByType,
  setMapLayerByType,
  findMapLayersByType,
  clearLayer,
  addMarker,
  addInteractions,
  selectEventListener,
  setMarkerSelectedById,
  addOverlay,
} from '@/utils/openlayers/bigMap';
import { findAerialviewByMap } from '@/services/swagger/aerialviewController';
import { findLayoutByMap } from '@/services/swagger/layoutController';
import { isEmpty, size } from 'lodash';
import Magnifier from './Magnifier';

const BigMap = React.forwardRef((props: any, ref: any) => {
  const params: any = useParams();
  const { instanceId } = params;
  const [markList, setMarkList] = useState<any>([]);
  const { isThumbnailShow, setToPanoramaId, panoramaData } = useModel('Panorama.data', (ret) => ({
    isThumbnailShow: ret.modalShow.isThumbnailShow,
    setToPanoramaId: ret.setToPanoramaId,
    panoramaData: ret.panoramaData,
  }));

  const initMapMarks = async (type = 1) => {
    const mapType = Number(type);
    const res = await findAerialviewByMap({
      ems_aerialview_type: mapType, // 平面图
      ems_aerialview_instanceid: instanceId,
      ems_aerialview_isdefault: 1,
    });
    const list = res.result?.list || [];
    const viewData = list[0] || {};
    if (viewData && !isEmpty(viewData)) {
      setMapViewByType(mapType);
      const url = viewData.ems_aerialview_file?.ems_sysfile_path
        ? `/systemfile${viewData.ems_aerialview_file?.ems_sysfile_path}`
        : null;
      await setMapLayerByType(mapType, url);
      // 清除图层标记
      const layer = findMapLayersByType('markerLayer')[0];
      clearLayer(layer);
      const res = await findLayoutByMap({
        ems_layout_aerialviewid: viewData.ems_aerialview_id,
      });
      const list = res.result?.list || [];
      setMarkList(list);
      list?.forEach((item) => {
        const data = JSON.parse(item.ems_layout_data || '{}') || {};
        addMarker({
          id: item.ems_layout_id,
          name: data.title,
          titleShow: data.titleShow,
          coordinate: data.coordinate,
        });
      });
      selectEventListener((id) => {
        const mark = list?.find((item) => item.ems_layout_id === id);
        if (mark) {
          setToPanoramaId(mark.ems_layout_panoramaid);
        }
      });
    }
  };

  useEffect(() => {
    if (panoramaData && !isEmpty(panoramaData) && markList.length > 0) {
      const panoramaId = panoramaData.ems_panorama_id;
      const mark = markList?.find((item) => item.ems_layout_panoramaid === panoramaId);
      if (mark) {
        setMarkerSelectedById(mark?.ems_layout_id);
        const data = JSON.parse(mark?.ems_layout_data || '{}') || {};
        const element = document.getElementById('magnifierBigBox');
        addOverlay(element, data.coordinate);
      }
    }
  }, [panoramaData, markList]);

  useEffect(() => {
    initMap('bigMap');
    addInteractions(['select']);
    initMapMarks(1);
    return () => {
      unloadMap();
    };
  }, []);

  useImperativeHandle(ref, () => ({
    async setMapType(type) {
      initMapMarks(type);
    },
  }));

  return (
    <div id="bigMap" style={{ width: '100%', height: '100%' }}>
      <Magnifier />
    </div>
  );
});

export default BigMap;
