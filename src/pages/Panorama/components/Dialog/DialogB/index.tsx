import './index.less';

function DialogB() {
  return (
    <>
      <div id="dialogB" className="dialogB" style={{ display: 'none' }}>
        <div className="dialog">
          <div className="dialog-top">
            <div className="dialogImg">
              <img src={require('@/assets/images/dialog/ylfw.png')} alt="" />
            </div>
          </div>
          <div className="dialog-bottom">
            <div className="qrcode">
              <img src={require('@/assets/images/dialog/qrcode.png')} alt="" />
            </div>
            <div className="d-b-title">医疗废物贮存设施</div>
            {/* {[
              { name: '单位名称' },
              { name: '设施编号' },
              { name: '废物类别' },
              { name: '负责人及联系方式' },
            ]?.map((item, index) => {
              return (
                <div className="d-b-content" key={index}>
                  <p>{item.name}：</p>
                  <div>{item.value}</div>
                </div>
              );
            })} */}
          </div>
        </div>
      </div>
    </>
  );
}

export default DialogB;
