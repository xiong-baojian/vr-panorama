import { useEffect, useMemo, useState } from 'react';
import { useModel } from 'umi';
import './index.less';

function DialogA(props) {
  const [style, setStyle] = useState('popup-container-style1');

  const { dialogInfo } = useModel('Panorama.data', (ret) => ({
    dialogInfo: ret.dialogInfo,
  }));

  useMemo(() => {
    if (dialogInfo && dialogInfo.id) {
      if (Number(dialogInfo.id) % 2 == 0) {
        setStyle('popup-container-style1');
      } else if (Number(dialogInfo.id) % 3 == 0) {
        setStyle('popup-container-style2');
      } else {
        setStyle('popup-container-style3');
      }
    }
  }, [dialogInfo]);

  return (
    <>
      {dialogInfo?.id ? (
        <div className={style} id="popup-container">
          <div className="dialog">
            <div className="popup-title">{dialogInfo?.name}</div>
            <div className="popup-content">
              <p>{dialogInfo?.content}</p>
            </div>
          </div>
        </div>
      ) : null}
    </>
  );
}

export default DialogA;
