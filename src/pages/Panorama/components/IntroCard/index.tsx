import { useModel } from 'umi';
import './index.less';
import {
  FundOutlined,
  InfoCircleOutlined,
  DashboardOutlined,
  SendOutlined,
  InsuranceOutlined,
  HddOutlined,
  CloseOutlined,
} from '@ant-design/icons';
import { Space, Typography } from 'antd';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.css';
import SwiperCore, { Scrollbar, Autoplay } from 'swiper/core';
import { useEffect, useMemo } from 'react';
SwiperCore.use([Scrollbar, Autoplay]);
const { Paragraph, Text } = Typography;

function IntroCard(props) {
  const { isIntroShow, modalShow, setModalShow, panoramaData } = useModel(
    'Panorama.data',
    (ret) => ({
      isIntroShow: ret.modalShow.isIntroShow,
      modalShow: ret.modalShow,
      setModalShow: ret.setModalShow,
      panoramaData: ret.panoramaData,
    }),
  );

  const handleSetInfoShow = () => {
    modalShow.isIntroShow = !modalShow.isIntroShow;
    setModalShow({
      isThumbnailShow: modalShow.isThumbnailShow,
      isIntroShow: modalShow.isIntroShow,
      isListShow: false,
      isGuideShow: false,
    });
  };

  const list = [
    {
      imgUrl: require('@/assets/images/panorama/park/door.jpg'),
    },
    {
      imgUrl: require('@/assets/images/panorama/park/intro1.png'),
    },
    {
      imgUrl: require('@/assets/images/panorama/park/intro2.jpg'),
    },
    {
      imgUrl: require('@/assets/images/panorama/park/intro3.png'),
    },
  ];

  const introData = {
    projectTitle: '德州市医疗废物处置中心危废综合处置项目(一期)项目',
  };

  return (
    <>
      <div
        className="info-icon-text"
        style={{
          cursor: 'pointer',
          userSelect: 'none',
          color: `${isIntroShow ? '#1890ff' : '#fff'}`,
        }}
      >
        <InfoCircleOutlined
          className="icon-info"
          style={{ fontSize: 22 }}
          onClick={handleSetInfoShow}
        />
        <div>概况</div>
      </div>
      {panoramaData?.ems_panorama_instanceid === 439 && isIntroShow && (
        <div className={'introCard-container-fadeIn'}>
          <div className="popup">
            <div className="popup-title">
              <div className="popup-title-scanning">
                <div>
                  {introData.projectTitle.length > 18
                    ? introData.projectTitle.substring(0, 19) + '...'
                    : introData.projectTitle}
                </div>
              </div>
              <CloseOutlined
                onClick={() => {
                  handleSetInfoShow();
                }}
                className="popup-title-close"
              />
            </div>
            <div className="popup-title-company">项目单位：德州龙瑞环保工程有限公司</div>
            <div className="popup-content">
              <div className="popup-content-swiperImgs">
                <Swiper
                  direction="horizontal"
                  slidesPerView={1}
                  freeMode={true}
                  scrollbar={{
                    hide: true,
                  }}
                  autoplay={{
                    delay: 2000,
                    disableOnInteraction: false,
                  }}
                >
                  {list?.map((value, valueIndex) => (
                    <SwiperSlide key={valueIndex}>
                      <div className="popup-content-swiperImg">
                        <img src={value.imgUrl} alt="" />
                      </div>
                    </SwiperSlide>
                  ))}
                </Swiper>
              </div>
              <div className="popup-content-top">
                <div className="ctrItem">
                  <div className="title">
                    <Space>
                      <FundOutlined /> 经济指标
                    </Space>
                  </div>
                  <div className="content">
                    <Space>
                      <div>
                        总投资：<span className="importantText">2.02亿</span>
                      </div>
                      <div>
                        年新增营业： <span className="importantText">1.488亿</span>
                      </div>
                      <div>
                        新增利税：<span className="importantText">0.23亿</span>
                      </div>
                    </Space>
                  </div>
                </div>
                <div className="ctrItem" style={{ marginTop: '10px' }}>
                  <div className="title">
                    <Space>
                      <HddOutlined />
                      处理处置设施
                    </Space>
                  </div>
                  <div className="content ">
                    <div>
                      <Space>
                        {/* <SendOutlined style={{ color: '#fff' }} /> */}
                        <span className="importantText">2条</span>危险废物(含医疗废物)焚烧处置线
                      </Space>
                    </div>
                    <div style={{ fontSize: '12px', marginLeft: '20px' }}>
                      <Space>
                        <DashboardOutlined style={{ color: '#fff' }} />
                        处理能力：
                        <span className="importantText">3万吨/年（50吨/日）</span>
                      </Space>
                    </div>
                    <div style={{ marginTop: 2 }}>
                      <span className="importantText">2条</span> 医疗废物消毒处理线
                    </div>
                    <div style={{ fontSize: '12px', marginLeft: '20px' }}>
                      <Space>
                        <DashboardOutlined style={{ color: '#fff' }} />
                        处理能力：
                        <span className="importantText">0.72万吨/年 （10吨/日）</span>
                      </Space>
                    </div>
                  </div>
                </div>
                <div className="ctrItem" style={{ marginTop: '10px' }}>
                  <div className="title">
                    <Space>
                      <InfoCircleOutlined />
                      设施开放单位简介
                    </Space>
                  </div>
                  <div className="contentA">
                    <Paragraph ellipsis={{ rows: 3, expandable: true, symbol: '更多' }}>
                      德州龙瑞环保工程有限公司位于山东省德州市德城区天衢工业园前小屯村东，占地11.9亩。是德州市一家处置医疗废物的企业，负责全市300余家各类医疗、保健、计生、防疫机构及社区门诊（卫生所）的医疗废物集中处置工作。
                    </Paragraph>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default IntroCard;
