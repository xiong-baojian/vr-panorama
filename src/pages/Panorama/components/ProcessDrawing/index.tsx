import { useModel } from 'umi';
import './index.less';
import {
  ClusterOutlined,
  InfoCircleOutlined,
  DashboardOutlined,
  SendOutlined,
  InsuranceOutlined,
  HddOutlined,
} from '@ant-design/icons';
import { Space, Typography } from 'antd';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.css';
import SwiperCore, { Scrollbar, Autoplay } from 'swiper/core';
import { useEffect, useMemo } from 'react';
SwiperCore.use([Scrollbar, Autoplay]);
const { Paragraph, Text } = Typography;

function ProcessDrawing(props) {
  const { isIntroShow, modalShow, setModalShow } = useModel('Panorama.data', (ret) => ({
    isIntroShow: ret.modalShow.isIntroShow,
    modalShow: ret.modalShow,
    setModalShow: ret.setModalShow,
  }));

  const handleSetInfoShow = () => {
    modalShow.isIntroShow = !modalShow.isIntroShow;
    setModalShow({
      isThumbnailShow: modalShow.isThumbnailShow,
      isIntroShow: modalShow.isIntroShow,
      isListShow: false,
      isGuideShow: false,
    });
  };

  const list = [
    {
      imgUrl: require('@/assets/images/panorama/process/processDrawing.png'),
    },
  ];

  const introData = {
    projectTitle: '德州市医疗废物处置中心危废综合处置项目(一期)项目',
  };

  return (
    <>
      <div
        className="info"
        style={{
          cursor: 'pointer',
          userSelect: 'none',
          marginBottom: 5,
          color: `${isIntroShow ? '#1890ff' : '#fff'}`,
        }}
      >
        <ClusterOutlined
          className="icon-info"
          style={{ fontSize: 22 }}
          onClick={handleSetInfoShow}
        />
        <div>工艺</div>
        {isIntroShow && (
          <div className={'processDrawing-container-fadeIn'}>
            <div className="processDrawing-container-title">
              <Space>
                <ClusterOutlined style={{ fontSize: 20 }} /> 危险废物焚烧设施-工艺图
              </Space>
            </div>
            <div className="processDrawing-container-content">
              <img src={require('@/assets/images/panorama/process/processDrawing.png')} alt="" />
            </div>
          </div>
        )}
      </div>
    </>
  );
}

export default ProcessDrawing;
