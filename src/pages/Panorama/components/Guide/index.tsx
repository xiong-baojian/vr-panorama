import './index.less';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Camera } from '@/utils/three/xThree';
import { Button, Typography, Space, Select } from 'antd';
import {
  PlayCircleOutlined,
  PauseCircleOutlined,
  CloseOutlined,
  createFromIconfontCN,
} from '@ant-design/icons';
import { findByViewangleMap } from '@/services/swagger/ViewangleController';
import { useParams, useModel } from 'umi';
import 'swiper/swiper-bundle.css';
import SwiperCore, { Scrollbar, Mousewheel, Pagination } from 'swiper/core';
import { findGuideByMap } from '@/services/swagger/guideController';
import { useEffect, useState, useRef } from 'react';
SwiperCore.use([Scrollbar, Mousewheel, Pagination]);
const { Paragraph, Text } = Typography;
const IconFont = createFromIconfontCN({
  scriptUrl: ['/iconfont/iconfont.js'],
});
let flag = true;

const Guide = () => {
  const swiperRef = useRef(null);
  const params: any = useParams();
  const { instanceId } = params;
  const [activeKey, setActiveKey] = useState<any>(null);
  const [guidePointlist, setGuidePointList] = useState<any>([]);
  const [currentGuideId, setCurrentGuideId] = useState<any>(null);
  const [lineList, setLineList] = useState<any>([]);
  const [isPlay, setIsPlay] = useState(false);
  const { isMobile } = useModel('mobile', (ret) => ({
    isMobile: ret.isMobile,
  }));
  const {
    isGuideShow,
    setToPanoramaId,
    setDialogInfo,
    modalShow,
    setModalShow,
    setVideoSrc,
    instanceConf,
    openViewId,
  } = useModel('Panorama.data', (ret) => ({
    setToPanoramaId: ret.setToPanoramaId,
    isGuideShow: ret.modalShow.isGuideShow,
    setIsGuideShow: ret.setIsGuideShow,
    setDialogInfo: ret.setDialogInfo,
    modalShow: ret.modalShow,
    setModalShow: ret.setModalShow,
    setVideoSrc: ret.setVideoSrc,
    instanceConf: ret.instanceConf,
    openViewId: ret.openViewId,
  }));

  /**
   * 滑到index
   * @param index
   */
  const slideTo = (index) => {
    if (swiperRef.current && swiperRef.current.swiper) swiperRef.current.swiper.slideTo(index);
  };

  /**
   * 切换路线
   * @param activeKey
   */
  const onChange = (activeKey: string) => {
    setActiveKey(activeKey);
    getGuidePointlist(activeKey);
    stop();
  };

  /**
   * 导览路线
   */
  const renderLineList = async (openViewId) => {
    await findByViewangleMap({
      ems_viewangle_instanceid: instanceId,
      ems_viewangle_id: openViewId,
    }).then((res) => {
      const list = res.result?.list || [];
      const lineList: any = [];
      list?.forEach((item) => {
        item.ems_viewangle_draws?.forEach((line) => {
          const obj = {
            label: line.ems_draw_name,
            value: line.ems_draw_id,
          };
          lineList.push(obj);
        });
      });
      setLineList(lineList);
      if (lineList.length > 0) {
        onChange(lineList[0].value);
      }
    });
  };

  /**
   * 导览点列表
   * @param id
   */
  const getGuidePointlist = (id) => {
    findGuideByMap({ ems_guide_instanceid: instanceId, ems_guide_drawid: id }).then((res) => {
      const list = res.result?.list;
      const arr = list?.map((item) => {
        let time = 0;
        if (item.ems_guide_jsondata) {
          const json = JSON.parse(item.ems_guide_jsondata);
          if (json.points && json.points.length > 0) {
            const len = json.points[json.points.length - 1];
            time = parseFloat(((len.width + len.x) / 60).toFixed(2));
          }
        }
        return {
          id: item.ems_guide_id,
          name: item.ems_guide_name,
          des: item.ems_guide_intr,
          cover: item.ems_guide_coverfile?.ems_sysfile_path,
          jsonData: item.ems_guide_jsondata,
          panoramaId: item.ems_guide_panoramaid,
          panoramaName: item.ems_guide_panoramaname,
          drVideoFile: item.ems_guide_drvideofile,
          time: time,
        };
      });
      setGuidePointList(arr);
    });
  };

  /**
   * 导览线总时间
   */
  const totalTime = guidePointlist?.reduce((total, item) => {
    if (item.time && !isNaN(item.time)) {
      return total + item.time;
    }
    return Math.floor(total);
  }, 0);

  const guide = (points: any, index = 0, onComplete: () => void) => {
    if (index >= points.length) {
      // 所有点都已经飞完，结束引导
      onComplete();
      return;
    }
    const currentPosition = points[index].data.form.position;
    const fov = points[index].fov;
    const duration = points[index].width / 60;
    let time = points[0].x;
    if (index > 0) {
      time = points[index].x - points[index - 1].x - points[index - 1].width;
    }

    time = time / 60;

    setTimeout(() => {
      setDialogInfo({});
    }, time * 1000);
    Camera.flyTo({
      position: currentPosition,
      fov: fov,
      duration: duration,
      delay: time,
      onComplete: () => {
        setDialogInfo(points[index - 1]);
        guide(points, index, onComplete);
      },
    });
    index += 1;
  };

  const startAnimation = () => {
    if (isPlay) {
      Camera.pauseFlyTo();
    } else {
      Camera.resumeFlyTo();
    }
    setIsPlay(!isPlay);

    let arr = guidePointlist?.map((item) => {
      const { points } = JSON.parse(item.jsonData);
      return {
        id: item.id,
        panoramaId: item.panoramaId,
        drVideoFile: item.drVideoFile,
        points,
      };
    });

    let index = 0;

    const startGuide = () => {
      if (index >= arr.length) {
        // 所有点都已经引导完成
        setIsPlay(false);
        flag = true;
        return;
      }

      if (arr[index].drVideoFile) {
        const filePath = '/systemfile' + arr[index].drVideoFile.ems_sysfile_path;
        setVideoSrc(filePath);
      } else {
        setVideoSrc('');
      }

      setCurrentGuideId(arr[index].id);

      setToPanoramaId(arr[index].panoramaId);

      setTimeout(() => {
        guide(arr[index].points, 0, () => {
          index += 1; // 当前点引导完成后，递增索引
          startGuide(); // 继续引导下一个点
          slideTo(index);
        });
      }, 1000);
    };

    if (flag) {
      startGuide();
      flag = false;
    }
  };

  /**
   * 关闭时触发
   */
  const stop = () => {
    flag = true;
    Camera.stopFlyTo();
    setIsPlay(false);
    setCurrentGuideId(null);
  };

  useEffect(() => {
    if (isGuideShow) {
      // 打开导览
      renderLineList(openViewId);
    } else {
      // 关闭导览
      stop();
    }
  }, [isGuideShow, openViewId]);

  return (
    <>
      <div
        className="guide"
        onClick={() => {
          modalShow.isGuideShow = !modalShow.isGuideShow;
          setModalShow({
            isThumbnailShow: modalShow.isThumbnailShow,
            isIntroShow: false,
            isListShow: false,
            isGuideShow: modalShow.isGuideShow,
          });
        }}
      >
        <IconFont
          type="icon-daolan"
          className={isGuideShow ? 'icon-guide selected' : 'icon-guide'}
        />
        <div className={isGuideShow ? 'guide-title selected' : 'guide-title '}>导览</div>
      </div>
      {isGuideShow && (
        <div className={`swiperBox ${isGuideShow ? 'showAnimation' : 'hideAnimation'}`}>
          <div className="btn">
            <Space>
              <Button
                size="small"
                style={{ fontSize: '0.875rem' }}
                type="ghost"
                icon={isPlay ? <PauseCircleOutlined /> : <PlayCircleOutlined />}
                onClick={startAnimation}
              >
                {isPlay ? '暂停' : '开始'}
              </Button>
              <div>
                <span
                  style={{
                    color: '#fff',
                    fontSize: '0.875rem',
                  }}
                >
                  导览线路：
                </span>
                <Select
                  size="small"
                  value={activeKey}
                  style={{ width: '9.375rem', fontSize: '0.875rem' }}
                  onChange={onChange}
                  options={lineList}
                />
              </div>
              <Text
                style={{
                  color: '#fff',
                  fontSize: '0.875rem',
                }}
              >
                总时长 ： 约{totalTime}s
              </Text>
            </Space>

            <CloseOutlined
              onClick={() => {
                setDialogInfo({});
                modalShow.isGuideShow = !isGuideShow;
                setModalShow({
                  isThumbnailShow: modalShow.isThumbnailShow,
                  isIntroShow: false,
                  isListShow: false,
                  isGuideShow: modalShow.isGuideShow,
                });

                if (!modalShow.isGuideShow && instanceConf && instanceConf.ems_instance_id) {
                  if (instanceConf.ems_instance_drvideofile) {
                    const filePath =
                      '/systemfile' + instanceConf.ems_instance_drvideofile.ems_sysfile_path;
                    setVideoSrc(filePath);
                  } else {
                    setVideoSrc('');
                  }
                }
              }}
              className="btn-close"
            />
          </div>
          {guidePointlist?.length > 0 ? (
            <Swiper
              ref={swiperRef}
              style={{ width: isMobile ? '100vw' : 650 }}
              slidesPerView={isMobile ? 3 : 4}
              freeMode={true}
              scrollbar={false}
              spaceBetween={10}
              mousewheel={true}
              lazy={true}
              pagination={{
                type: 'progressbar',
              }}
            >
              {guidePointlist?.map((item: any) => {
                return (
                  <SwiperSlide key={item.id}>
                    <div className={`item ${currentGuideId === item.id ? 'selected' : ''}`}>
                      <img
                        width="100%"
                        height="100%"
                        src={`/systemfile${item.cover}`}
                        loading="lazy"
                      ></img>
                      <div className="card-name">{item.panoramaName}</div>
                      <Text
                        style={{
                          width: '100%',
                          textAlign: 'right',
                          position: 'absolute',
                          right: 0,
                          top: 0,
                          color: '#fff',
                          fontSize: '14px',
                          padding: '0 2px',
                        }}
                      >
                        {item.time}s
                      </Text>
                    </div>
                  </SwiperSlide>
                );
              })}
            </Swiper>
          ) : (
            <div style={{ width: isMobile ? '100vw' : 650, height: '6.6rem', color: '#fff' }}></div>
          )}
        </div>
      )}
    </>
  );
};
export default Guide;
