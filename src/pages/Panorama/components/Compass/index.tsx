import { useEffect, useState } from 'react';
import { useModel, useParams } from 'umi';
import { Camera } from '@/utils/three/xThree';
import { Tabs, Space, Segmented, Tag } from 'antd';
import { createFromIconfontCN } from '@ant-design/icons';
import { switchPerspective } from '@/services/swagger/ViewangleController';
import { findByViewangleMap } from '@/services/swagger/ViewangleController';
import './index.less';
import { isEmpty } from 'lodash';
const IconFont = createFromIconfontCN({
  scriptUrl: '/iconfont/iconfont.js',
});

let t = 0;

const Compass = () => {
  const params: any = useParams();
  const { instanceId } = params;

  const [viewList, setViewList] = useState<any>([]);

  const { panoramaData, setToPanoramaId, setOpenViewId } = useModel('Panorama.data', (ret) => ({
    panoramaData: ret.panoramaData,
    setToPanoramaId: ret.setToPanoramaId,
    setOpenViewId: ret.setOpenViewId,
  }));

  const resetView = () => {
    let cameraPosition = JSON.parse(panoramaData?.ems_panorama_initview || '0'); // 相机初始坐标
    Camera.resetCamera(cameraPosition, 50);
  };

  const switchView = (id) => {
    switchPerspective({
      ems_viewangle_id: id,
      ems_instance_panorama_group_id: panoramaData.ems_panorama_groupid,
    }).then((res) => {
      if (res.result && !isEmpty(res.result)) {
        setToPanoramaId(res.result.ems_panorama_id);
        setOpenViewId(id);
        const list = viewList?.map((item) => {
          if (item.ems_viewangle_id == id) {
            item.panoramaId = res.result?.ems_panorama_id;
          }
          return item;
        });
        setViewList([...list]);
      }
    });
  };

  /**
   * 视角列表查询
   */
  const renderViewList = async () => {
    await findByViewangleMap({ ems_viewangle_instanceid: instanceId }).then((res) => {
      const list = res.result?.list || [];
      setViewList(list);
    });
  };

  useEffect(() => {
    if (window.viewer) {
      Camera.getCameraTheta(window.viewer, (theta) => {
        if (theta) {
          const deg = theta - t;
          const compass: any = document.getElementById('compass');
          if (compass) {
            compass.style.transform = 'rotateZ(' + -deg + 'deg)';
          }
        }
      });
    }
  }, [window.viewer]);

  useEffect(() => {
    if (window.viewer && panoramaData && !isEmpty(panoramaData)) {
      const compassCoord = panoramaData.ems_panorama_compass;
      if (compassCoord) {
        const coord = JSON.parse(compassCoord);
        if (coord) {
          const theta = Camera.getPositionTheta(window.viewer, coord);
          t = theta;
        }
      } else {
        t = 0;
      }
    }
  }, [panoramaData, window.viewer]);

  useEffect(() => {
    renderViewList();
  }, []);

  return (
    <>
      <div className="compassBox" onClick={resetView}>
        <div className="compass" id="compass"></div>
      </div>
      <Space className="view">
        {viewList?.map((view) => {
          return (
            <Tag
              key={view.ems_viewangle_id}
              color={
                panoramaData.ems_panorama_id === view.panoramaId ? '#f50' : 'rgba(74, 76, 234,0.3)'
              }
              style={{ cursor: 'pointer', fontSize: '10px', color: 'rgba(255,255,255,0.8)' }}
              onClick={() => {
                switchView(view.ems_viewangle_id);
              }}
            >
              {view.ems_viewangle_name}
            </Tag>
          );
        })}
      </Space>

      <div className="compassBoxOptions">
        <Space style={{ color: 'rgba(255,255,255,0.6)', fontSize: '12px' }}>
          <Space>{panoramaData?.ems_panorama_createtime}</Space>|
          <Space>
            <span>A（角度）：{panoramaData?.ems_panorama_camera_angle}°</span>
            <span>H（高度）：{panoramaData?.ems_panorama_camera_height}m</span>
          </Space>
        </Space>
      </div>
    </>
  );
};

export default Compass;
