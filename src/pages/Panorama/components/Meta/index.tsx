import { useEffect, useState, useRef } from 'react';
import { Button } from 'antd';
import styles from './index.less';
import { useModel } from 'umi';
import VideoJS from './VideoJS';

const Meta = () => {
  const playerRef = useRef(null);

  const [muted, setMuted] = useState(true);
  const [isMaskShow, setIsMaskShow] = useState(true);

  const { isMobile } = useModel('mobile', (ret) => ({
    isMobile: ret.isMobile,
  }));

  const { videoSrc, setVideoSrc, instanceConf, sound, setSound } = useModel(
    'Panorama.data',
    (ret) => ({
      panoramaData: ret.panoramaData,
      instanceConf: ret.instanceConf,
      sound: ret.sound,
      setSound: ret.setSound,
      videoSrc: ret.videoSrc,
      setVideoSrc: ret.setVideoSrc,
    }),
  );

  const videoRef = useRef<any>(null);

  /**
   * 播放
   */
  const playVideo = () => {
    if (videoRef.current) {
      videoRef.current.play(); // 播放视频
    }
  };

  /**
   * 暂停
   */
  const pauseVideo = () => {
    if (videoRef.current) {
      videoRef.current.pause(); // 暂停视频
    }
  };

  /**
   *  切换播放与暂停
   */
  const togglePlayPause = () => {
    if (videoRef.current) {
      if (videoRef.current.paused) {
        videoRef.current.play(); // 如果视频是暂停状态，播放视频
      } else {
        videoRef.current.pause(); // 否则暂停视频
      }
    }
  };

  const computeFrame = (video, ctx) => {
    if (video) {
      if (video.paused || video.ended) return;
    }

    const targetColor = { rTarget: 85, gTarget: 170, bTarget: 128 };
    const { rTarget, gTarget, bTarget } = targetColor;

    const a = 50;

    const rMin = rTarget - a,
      rMax = rTarget + a;
    const gMin = gTarget - a,
      gMax = gTarget + a;
    const bMin = bTarget - a,
      bMax = bTarget + a;

    ctx.clearRect(0, 0, video.clientWidth / 1, video.clientHeight / 1);
    ctx.drawImage(video, 0, 0, video.clientWidth / 1, video.clientHeight / 1);
    let frame = ctx.getImageData(0, 0, video.clientWidth, video.clientHeight);
    const pointLens = frame.data.length / 4;
    for (let i = 0; i < pointLens; i++) {
      let r = frame.data[i * 4];
      let g = frame.data[i * 4 + 1];
      let b = frame.data[i * 4 + 2];

      if (r >= rMin && r <= rMax && g >= gMin && g <= gMax && b >= bMin && b <= bMax) {
        // 设置该像素为透明
        frame.data[i * 4 + 3] = 0;
      }
    }

    ctx.putImageData(frame, 0, 0);
    setTimeout(() => {
      computeFrame(video, ctx);
    }, 0);
  };

  const video2Canvas = () => {
    const video: any = videoRef.current;
    const canvas: any = document.getElementById('outputCanvas');
    canvas.willReadFrequently = true;
    const ctx = canvas.getContext('2d');

    computeFrame(video, ctx);
  };

  const isWeChat = () => {
    const userAgent = navigator.userAgent.toLowerCase();
    return userAgent.includes('micromessenger');
  };

  useEffect(() => {
    setMuted(!sound);
  }, [sound]);

  useEffect(() => {
    if (instanceConf && instanceConf.ems_instance_id) {
      if (instanceConf.ems_instance_drvideofile) {
        const filePath = '/systemfile' + instanceConf.ems_instance_drvideofile.ems_sysfile_path;
        setVideoSrc(filePath);
      } else {
        setVideoSrc('');
      }
    }
  }, [instanceConf]);

  return (
    <>
      {isMobile && isMaskShow && isWeChat() && (
        <div className={styles.metaMask}>
          <Button
            className={styles.metaMaskButton}
            type="primary"
            size="large"
            onClick={() => {
              setSound(true);
              setMuted(false);
              setIsMaskShow(false);
              setTimeout(() => {
                playVideo();
              }, 100);
            }}
          >
            进入VR
          </Button>
        </div>
      )}

      {videoSrc && (
        <div className={styles.meta}>
          <div className={styles.mouse} onClick={togglePlayPause}></div>
          <video
            playsInline={true}
            ref={videoRef}
            width={300}
            height={533}
            src={videoSrc}
            muted={muted}
            autoPlay
            loop
            preload="none"
          ></video>
          {/* <canvas id="outputCanvas" width={300} height={533}></canvas> */}
        </div>
      )}
    </>
  );
};

export default Meta;
