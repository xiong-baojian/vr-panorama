import { useEffect, useRef, useState } from 'react';
import { useParams, useModel } from 'umi';
import { Space } from 'antd';
import { isExpired } from '@/services/swagger/vrPreviewController';
import ExpirePage from './components/ExpirePage';
import Compass from './components/Compass';
import Banner from './components/Banner';
import FuncBtns from './components/FuncBtns';
import { findInstanceByMap } from '@/services/swagger/instanceController';
import PanoramaCanvas from './components/Canvas';
import Layers from './components/Layers';
import Guide from './components/Guide';
import LeaveMessage from './components/LeaveMessage';
import PanoramaList from './components/PanoramaList';
import DataModal from './components/DataModal';
import DialogA from './components/Dialog/DialogA';
import DialogB from './components/Dialog/DialogB';
import IntroCard from './components/IntroCard';
import ProcessDrawing from './components/ProcessDrawing';
import Thumbnail from './components/Thumbnail';
import Meta from './components/Meta';

import './index.less';
import './moblie.less';

const PanoramaView = () => {
  const params: any = useParams();
  const { instanceId } = params;
  const { isMobile } = useModel('mobile', (ret) => ({
    isMobile: ret.isMobile,
  }));

  const [flag, setFlag] = useState<boolean>(false);
  const [loading, setLoading] = useState(true);
  const [coverLoading, setCoverLoading] = useState(true);

  const { instanceConf, setInstanceConf } = useModel('Panorama.data', (ret) => ({
    setInstanceConf: ret.setInstanceConf,
    instanceConf: ret.instanceConf,
  }));

  /**
   * 基础全局配置
   */
  const setConfig = () => {
    findInstanceByMap({ ems_instance_id: instanceId }).then((res) => {
      const list = res.result?.list || [];
      const instanceData: any = list[0];
      if (instanceData) {
        let conf = { ...JSON.parse(instanceData.ems_instance_globeconf), ...instanceData };
        setInstanceConf(conf);
      }
    });
  };

  useEffect(() => {
    isExpired({ accessUrl: window.location.href }).then((res) => {
      setFlag(!res.result);
      if (res.result) {
        setLoading(false); // 结束 loading 状态
        return;
      }
      setConfig();
      setLoading(false); // 结束 loading 状态
    });
  }, []);

  if (loading) {
    // 如果处于加载状态，什么都不渲染
    return null;
  }

  const handleEnter = () => {
    setCoverLoading(false);
  };
  return (
    <>
      {instanceConf?.openCoverFile?.ems_sysfile_path && coverLoading && (
        <div className="panoramaViewBox-mask" onClick={handleEnter}>
          <img
            src={
              instanceConf?.openCoverFile?.ems_sysfile_path
                ? `/systemfile${instanceConf?.openCoverFile?.ems_sysfile_path}`
                : '暂无'
            }
            alt=""
            className="panoramaViewBox-mask-img"
          />
          <button className="panoramaViewBox-mask-btn">
            <span>点击进入</span>
          </button>
        </div>
      )}
      {flag ? (
        <div className="panoramaViewBox">
          <div className={`panoramaView-${isMobile ? 'mobile' : 'web'}`}>
            <div className="banner">
              <Banner />
            </div>
            <h1 className="banner-header">环保设施向公众开放沉浸式展厅</h1>
            {instanceConf?.logoImgFile?.ems_sysfile_path && (
              <Space className="logo" direction="vertical">
                <Space>
                  <img src="/logo.png" alt="" className="img1" />
                  <img
                    className="img2"
                    src={
                      instanceConf?.logoImgFile?.ems_sysfile_path
                        ? `/systemfile${instanceConf?.logoImgFile?.ems_sysfile_path}`
                        : '暂无'
                    }
                    alt=""
                  />
                </Space>
                <Space className="festival">
                  <img src={require('@/assets/images/festival/全国低碳日.png')} alt="" />
                  全国低碳日
                </Space>
              </Space>
            )}
            <div className="funcBtns">
              <FuncBtns />
            </div>
            <Compass />
            <div className="leftIcon">{/* <ProcessDrawing /> */}</div>

            <IntroCard />
            <Layers />
            {isMobile && <PanoramaList />}
            <Guide />
            <LeaveMessage />

            {/* 任务窗口 */}
            <DataModal />
            <PanoramaCanvas />
            <DialogA />
            <DialogB />

            {/* 缩略图 */}
            <Thumbnail />

            {/* 数字人 */}
            <Meta />
          </div>
        </div>
      ) : (
        <ExpirePage />
      )}
    </>
  );
};
export default PanoramaView;
