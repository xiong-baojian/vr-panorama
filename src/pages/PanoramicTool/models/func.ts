import { useState } from 'react';

export default () => {
  const [menuId, setMenuId] = useState(1);
  const menuList = [
    { id: 1, name: '全局', icon: 'icon-jiemianshezhi' },
    { id: 2, name: '视角', icon: 'icon-shijiaoshoucang' },
    { id: 3, name: '绘制', icon: 'icon-fuhao-huizhi' },
    { id: 4, name: '热点', icon: 'icon-B' },
    { id: 5, name: '导览', icon: 'icon-luxian' },
    { id: 6, name: '留言', icon: 'icon-liuyan' },
    { id: 7, name: '其他', icon: 'icon-qita' },
  ];

  const selectMenu = (id: number) => {
    setMenuId(id);
  };

  return { menuId, menuList, selectMenu };
};
