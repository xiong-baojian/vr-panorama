import { useState } from 'react';
import { uniqueId } from 'lodash';
import { findGuideByMap } from '@/services/swagger/guideController';

export default () => {
  const [audioFile, setAudioFile] = useState({ name: 1, file: null });
  const [timeLineShow, setTimeLineShow] = useState(false);
  const [guidePointlist, setGuidePointList] = useState<any>([]);

  const [currentLineId, setCurrentLineId] = useState<any>(null); // 导览路线
  const [points, setPoints] = useState<any>([]);
  const [currentPoint, setCurrentPoint] = useState<any>({});
  const [currentGuidePoint, setCurrentGuidePoint] = useState<any>({});
  const [videoState, setVideoState] = useState<any>({ state: false, time: 0 });

  const addPoint = (params: any) => {
    const { data, name, fov, position } = params;
    let x = 0;
    if (points.length > 0) {
      let width = parseInt(points[points.length - 1].width);
      x = points[points.length - 1].x + width;
    }
    let newId = uniqueId();
    while (points.some((point) => point.id === newId)) {
      newId = uniqueId();
    }
    const newPoint = {
      id: newId,
      data,
      fov,
      position,
      name,
      x,
      y: 0,
      width: 300,
      height: 50,
    };
    setPoints([...points, newPoint]);
  };

  const getGuidePointlist = (instanceId, drawId = null) => {
    findGuideByMap({ ems_guide_instanceid: instanceId, ems_guide_drawid: drawId }).then((res) => {
      const list = res.result?.list;
      const arr = list?.map((item) => {
        let time = 0;
        if (item.ems_guide_jsondata) {
          const json = JSON.parse(item.ems_guide_jsondata);
          if (json.points && json.points.length > 0) {
            const len = json.points[json.points.length - 1];
            time = parseFloat(((len.width + len.x) / 60).toFixed(2));
          }
        }

        return {
          id: item.ems_guide_id,
          name: item.ems_guide_name,
          des: item.ems_guide_intr,
          cover: item.ems_guide_coverfile?.ems_sysfile_path,
          jsonData: item.ems_guide_jsondata,
          panoramaId: item.ems_guide_panoramaid,
          panoramaName: item.ems_guide_panoramaname,
          drVideoFile: item.ems_guide_drvideofile,
          drawId: item.ems_guide_drawid,
          time: time,
        };
      });
      setGuidePointList(arr);
    });
  };

  return {
    audioFile,
    setAudioFile,
    timeLineShow,
    setTimeLineShow,
    points,
    addPoint,
    setPoints,
    setCurrentPoint,
    currentPoint,
    guidePointlist,
    setGuidePointList,
    getGuidePointlist,
    currentGuidePoint,
    setCurrentGuidePoint,
    videoState,
    setVideoState,
    currentLineId,
    setCurrentLineId,
  };
};
