import './index.less';
import { useModel } from 'umi';
import { Rnd } from 'react-rnd';
import Marquee from 'react-fast-marquee';
import { Alert } from 'antd';
import { ReadOutlined } from '@ant-design/icons';
import { useEffect, useState, useRef } from 'react';
import OperateDirection from '../OperateDirection';
import LayerManagement from '../LayerManagement';
import FuncBtns from '../FuncBtns';
const PageDesign = () => {
  const boxRef = useRef<any>(null);
  const { logoImg, stationDes, instanceConf } = useModel('PanoramicTool.layout', (ret) => ({
    logoImg: ret.logoImg,
    stationDes: ret.stationDes,
    instanceConf: ret.instanceConf,
  }));
  const videoRef = useRef<HTMLVideoElement | null>(null); // 引用 video 元素
  const [videoSrc, setVideoSrc] = useState('');

  const [boxSize, setBoxSize] = useState({ width: 0, height: 0 });

  const [styles, setStyles] = useState({
    logo: { id: 1, width: 180, height: 80, position: { x: 0, y: 0 } },
    opt: { id: 2, width: 100, height: 100, position: { x: 0, y: 0 } },
    scene: { id: 3, width: 100, height: 170, position: { x: 0, y: 0 } },
    funcBtn: { id: 4, width: 260, height: 40, position: { x: 0, y: 0 } },
    box: { id: 5, width: 180, height: 180, position: { x: 0, y: 0 }, ratioX: 0.8, ratioY: 1 },
  });

  useEffect(() => {
    if (instanceConf && instanceConf.ems_instance_drvideofile) {
      if (videoSrc) {
        URL.revokeObjectURL(videoSrc);
      }
      const videoURL = '/systemfile' + instanceConf.ems_instance_drvideofile.ems_sysfile_path;
      setVideoSrc(videoURL);
    } else {
      setVideoSrc('');
    }
  }, [instanceConf]);

  useEffect(() => {
    const resizeObserver = new ResizeObserver((entries) => {
      for (let entry of entries) {
        if (entry.target === boxRef.current) {
          const width = entry.contentRect.width;
          const height = entry.contentRect.height;
          setBoxSize({
            width: width,
            height: height,
          });
          styles['logo'].position = {
            x: (width - styles['logo'].width) * 0,
            y: (height - styles['logo'].height) * 0.07,
          };
          styles['opt'].position = {
            x: (width - styles['opt'].width) * 0.99,
            y: (height - styles['opt'].height) * 0.99,
          };
          styles['scene'].position = {
            x: (width - styles['scene'].width) * 0,
            y: (height - styles['scene'].height) * 0.99,
          };
          styles['funcBtn'].position = {
            x: (width - styles['funcBtn'].width) * 0.99,
            y: (height - styles['funcBtn'].height) * 0.07,
          };
          styles['box'].position = {
            x: (width - styles['box'].width) * styles['box'].ratioX,
            y: (height - styles['box'].height) * styles['box'].ratioY,
          };
          setStyles({ ...styles });
        }
      }
    });
    if (boxRef.current) {
      resizeObserver.observe(boxRef.current);
    }
    return () => {
      resizeObserver.disconnect();
    };
  }, [boxRef]);
  return (
    <div className="pageDesignBox" ref={boxRef}>
      <div className="banner">
        <Alert
          type="info"
          closable
          icon={<ReadOutlined />}
          banner
          message={
            <Marquee pauseOnHover gradient={false}>
              {stationDes.des}
            </Marquee>
          }
        />
      </div>

      {/* logo */}
      <Rnd
        className="rndBox"
        bounds="parent"
        size={{ width: styles['logo'].width, height: styles['logo'].height }}
        position={styles['logo'].position}
        onResize={(e, direction, ref, delta, position) => {
          styles['logo'].width = ref.offsetWidth;
          styles['logo'].height = ref.offsetHeight;
          styles['logo'].position = position;
          setStyles({ ...styles });
        }}
        onDragStop={(e: any, d: any) => {
          styles['logo'].position = { x: d.x, y: d.y };
          setStyles({ ...styles });
        }}
      >
        <div className="dragBox" draggable={false}>
          {logoImg && (
            <img
              draggable={false}
              src={
                logoImg
                  ? `/systemfile${logoImg.ems_sysfile_path}`
                  : require('@/assets/images/panoramicTool/logo.png')
              }
              alt=""
            />
          )}
        </div>
      </Rnd>
      {/* 操作盘 */}
      <Rnd
        className="rndBox"
        bounds="parent"
        size={{ width: styles['opt'].width, height: styles['opt'].height }}
        position={styles['opt'].position}
        onResize={(e, direction, ref, delta, position) => {
          styles['opt'].width = ref.offsetWidth;
          styles['opt'].height = ref.offsetHeight;
          styles['opt'].position = position;
          setStyles({ ...styles });
        }}
        onDragStop={(e: any, d: any) => {
          styles['opt'].position = { x: d.x, y: d.y };
          setStyles({ ...styles });
        }}
      >
        <div className="dragBox" draggable={false}>
          <OperateDirection />
        </div>
      </Rnd>
      {/* 场景选择 */}
      <Rnd
        enableResizing={false}
        className="rndBox"
        bounds="parent"
        size={{ width: styles['scene'].width, height: styles['scene'].height }}
        position={styles['scene'].position}
        onDragStop={(e: any, d: any) => {
          styles['scene'].position = { x: d.x, y: d.y };
          setStyles({ ...styles });
        }}
      >
        <div className="dragBox" draggable={false}>
          <LayerManagement />
        </div>
      </Rnd>
      {/* 功能键 */}
      <Rnd
        enableResizing={false}
        className="rndBox"
        bounds="parent"
        size={{ width: styles['funcBtn'].width, height: styles['funcBtn'].height }}
        position={styles['funcBtn'].position}
        onDragStop={(e: any, d: any) => {
          styles['funcBtn'].position = { x: d.x, y: d.y };
          setStyles({ ...styles });
        }}
      >
        <div className="dragBox" draggable={false}>
          <FuncBtns />
        </div>
      </Rnd>
      {/* 数字人 */}
      <Rnd
        className="rndBox"
        bounds="parent"
        size={{ width: styles['box'].width, height: styles['box'].height }}
        position={styles['box'].position}
        onResize={(e, direction, ref, delta, position) => {
          styles['box'].width = ref.offsetWidth;
          styles['box'].height = ref.offsetHeight;
          styles['box'].position = position;
          setStyles({ ...styles });
        }}
        onDragStop={(e: any, d: any) => {
          const x = d.x / (boxSize.width - styles['box'].width);
          const y = d.y / (boxSize.height - styles['box'].height);
          styles['box'].ratioX = x;
          styles['box'].ratioY = y;
          styles['box'].position = { x: d.x, y: d.y };
          setStyles({ ...styles });
        }}
      >
        <div className="dragBox" draggable={false}>
          <video ref={videoRef} src={videoSrc}></video>
        </div>
      </Rnd>
    </div>
  );
};
export default PageDesign;
