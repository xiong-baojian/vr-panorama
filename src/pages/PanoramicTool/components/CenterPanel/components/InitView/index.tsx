import { Button, message } from 'antd';
import styles from './index.less';
import { useModel } from 'umi';
import { Common } from '@/utils/three/xThree';
import { updatePanorama } from '@/services/swagger/panoramaController';

const InitView = () => {
  const { panoramaData, setThreeImgData, setNorthViewImgData, queryPanoramaView } = useModel(
    'PanoramicTool.panoramaData',
    (ret) => ({
      panoramaData: ret.panoramaData,
      setThreeImgData: ret.setThreeImgData,
      setNorthViewImgData: ret.setNorthViewImgData,
      queryPanoramaView: ret.queryPanoramaView,
    }),
  );

  const saveNorthView = () => {
    const northView = Common.getCameraPosition(window.viewer);
    updatePanorama({
      ems_panorama_id: panoramaData?.ems_panorama_id,
      ems_panorama_compass: JSON.stringify(northView),
    }).then((res) => {
      if (res?.code == 200) {
        queryPanoramaView(panoramaData?.ems_panorama_id);
        const imgData = Common.getThreeCanvasImgData(window.viewer);
        setNorthViewImgData(imgData);
        message.success('保存成功！');
      }
    });
  };

  const saveInitView = () => {
    let initView = Common.getCameraPosition(window.viewer);
    updatePanorama({
      ems_panorama_id: panoramaData?.ems_panorama_id,
      ems_panorama_initview: JSON.stringify(initView),
    }).then((res) => {
      if (res?.code == 200) {
        queryPanoramaView(panoramaData?.ems_panorama_id);
        let imgData = Common.getThreeCanvasImgData(window.viewer);
        setThreeImgData(imgData);
        message.success('保存成功！');
      }
    });
  };

  return (
    <div className={styles.viewBox}>
      <div className={styles.viewT}></div>
      <div className={styles.viewL}></div>
      <div className={styles.viewR}></div>
      <div className={styles.viewB}></div>
      <div className={styles.view}>
        <div className={styles.btn}>
          {/* <Button onClick={closeInitViewModalShow}>退出</Button> */}
          <Button type="primary" onClick={saveInitView}>
            保存当前初始视角
          </Button>
        </div>
        <div className={styles.directionBtn}>
          <Button type="primary" onClick={saveNorthView}>
            设置当前视角为正北方向
          </Button>
        </div>
      </div>
    </div>
  );
};
export default InitView;
