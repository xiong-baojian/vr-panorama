import './index.less';
import { useModel } from 'umi';
import { Rnd } from 'react-rnd';
import { useEffect, useState, useRef } from 'react';
const PageDesign = () => {
  const { timeLineShow, audioFile, videoState } = useModel('PanoramicTool.timeLine', (ret) => ({
    audioFile: ret.audioFile,
    timeLineShow: ret.timeLineShow,
    videoState: ret.videoState,
  }));
  const boxRef = useRef<any>(null);
  const videoRef = useRef<HTMLVideoElement | null>(null); // 引用 video 元素
  const [videoSrc, setVideoSrc] = useState('');
  const [boxSize, setBoxSize] = useState({ width: 0, height: 0 });

  const [styles, setStyles] = useState({
    box: { id: 1, width: 180, height: 180, position: { x: 0, y: 0 }, ratioX: 0.8, ratioY: 1 },
  });

  useEffect(() => {
    if (audioFile && audioFile.file && videoState) {
      if (videoState.state) {
        if (videoState.time >= videoRef.current.duration) {
          return;
        }
        videoRef.current.currentTime = videoState.time;
        videoRef.current?.play();
      } else {
        videoRef.current?.pause();
      }
    }
  }, [videoState]);

  useEffect(() => {
    if (audioFile && audioFile.file) {
      if (videoSrc) {
        URL.revokeObjectURL(videoSrc);
      }
      const videoURL = URL.createObjectURL(audioFile.file);
      setVideoSrc(videoURL);
    } else {
      setVideoSrc('');
    }
  }, [audioFile]);

  useEffect(() => {
    const resizeObserver = new ResizeObserver((entries) => {
      for (let entry of entries) {
        if (entry.target === boxRef.current) {
          const width = entry.contentRect.width;
          const height = entry.contentRect.height;
          setBoxSize({
            width: width,
            height: height,
          });
          styles['box'].position = {
            x: (width - styles['box'].width) * styles['box'].ratioX,
            y: (height - styles['box'].height) * styles['box'].ratioY,
          };
          setStyles({ ...styles });
        }
      }
    });
    if (boxRef.current) {
      resizeObserver.observe(boxRef.current);
    }
    return () => {
      resizeObserver.disconnect();
    };
  }, [boxRef]);

  return (
    <div className="pageDesignBox" ref={boxRef}>
      {timeLineShow && videoSrc && (
        <Rnd
          className="rndBox"
          bounds="parent"
          size={{ width: styles['box'].width, height: styles['box'].height }}
          position={styles['box'].position}
          onResize={(e, direction, ref, delta, position) => {
            styles['box'].width = ref.offsetWidth;
            styles['box'].height = ref.offsetHeight;
            styles['box'].position = position;
            setStyles({ ...styles });
          }}
          onDragStop={(e: any, d: any) => {
            const x = d.x / (boxSize.width - styles['box'].width);
            const y = d.y / (boxSize.height - styles['box'].height);
            styles['box'].ratioX = x;
            styles['box'].ratioY = y;
            styles['box'].position = { x: d.x, y: d.y };
            setStyles({ ...styles });
          }}
        >
          <div className="dragBox" draggable={false}>
            <video ref={videoRef} src={videoSrc}></video>
          </div>
        </Rnd>
      )}
    </div>
  );
};
export default PageDesign;
