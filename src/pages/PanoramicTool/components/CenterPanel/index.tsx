import { useEffect, useState, useRef } from 'react';
import { Viewer, PanoramaTool as Panorama, Common } from '@/utils/three/xThree';
import { useModel, useParams } from 'umi';
import { uniqueId } from 'lodash';
import isEmpty from 'lodash/isEmpty';
import styles from './index.less';
import Drag from '@/utils/drag';
let panorama: any;
const CenterPanel = () => {
  const [panoramaId, setPanoramaId] = useState<number | undefined>();
  const { panoramaData, setThreeImgData } = useModel('PanoramicTool.panoramaData', (ret) => ({
    panoramaData: ret.panoramaData,
    setThreeImgData: ret.setThreeImgData,
  }));
  const { menuId } = useModel('PanoramicTool.func', (ret) => ({
    menuId: ret.menuId,
  }));
  const {
    setPanorama,
    currentLabel,
    setCurrentLabel,
    addObject,
    addLabel,
    setLabels,
    currentLabelPos,
    setCurrentLabelPos,
    selectObject,
    refreshObjects,
  } = useModel('PanoramicTool.label');

  const { initShape, refreshShapeList } = useModel('PanoramicTool.shape', (ret) => ({
    initShape: ret.initShape,
    refreshShapeList: ret.refreshShapeList,
  }));

  // 标签监听
  useEffect(() => {
    if (!isEmpty(currentLabel) && !isEmpty(currentLabelPos)) {
      const id = uniqueId();
      let data = {
        id: id,
        pid: currentLabel?.ems_tagtype_pid,
        typeId: currentLabel.ems_tagtype_id,
        panoramaId: panoramaData?.ems_panorama_id,
        name: currentLabel.ems_tagtype_name,
        type: currentLabel.ems_tagtype_type,
        url: `/systemfile${currentLabel.ems_tagtype_iconfile?.ems_sysfile_path}`,
        imgType: currentLabel.ems_tagtype_iconfile?.ems_sysfile_type,
      };
      addLabel(data);
      let object = addObject(data, currentLabelPos);
      selectObject(object);
      setCurrentLabelPos({});
      setCurrentLabel({});
    }
  }, [currentLabel, currentLabelPos]);

  const getImgUrl = (files) => {
    return files.map((item) => {
      return '/systemfile' + item.ems_sysfile_path;
    });
  };

  // 全景监听切换
  useEffect(() => {
    if (panoramaData) {
      setPanoramaId(panoramaData.ems_panorama_id);
      if (panoramaData.ems_panorama_id !== panoramaId) {
        let cameraPosition = JSON.parse(panoramaData?.ems_panorama_initview || '0'); // 相机初始坐标
        const panoramaImgs = {
          preview: '/systemfile' + panoramaData.ems_panorama_previewfile?.ems_sysfile_path,
          tileUrls: [
            getImgUrl(panoramaData.firstLevel.px),
            getImgUrl(panoramaData.firstLevel.nx),
            getImgUrl(panoramaData.firstLevel.py),
            getImgUrl(panoramaData.firstLevel.ny),
            getImgUrl(panoramaData.firstLevel.pz),
            getImgUrl(panoramaData.firstLevel.nz),
          ],
        };
        panorama.setPanoramaImg(panoramaImgs, cameraPosition);
        let timer = setTimeout(() => {
          let imgData = Common.getThreeCanvasImgData(window.viewer);
          setThreeImgData(imgData);
          clearTimeout(timer);
        }, 200);
      }
      // 添加图形object
      refreshShapeList(panoramaData.ems_panorama_id);

      if (menuId == 3) {
        panorama.disposeDragControl();
        panorama.removeAll();
      } else if (menuId == 4) {
        refreshObjects(panoramaData);
        // 拖拽控制
        panorama.initDragControl((object: any) => {
          selectObject({ ...object });
        });
      } else if (menuId == 5) {
        panorama.disposeDragControl();
        refreshObjects(panoramaData);
      } else {
        panorama.disposeDragControl();
        panorama.removeAll();
      }
      setPanorama(panorama);
    }
  }, [panoramaData, menuId]);

  // 初始化
  useEffect(() => {
    let viewer: any;
    viewer = new Viewer({
      domId: 'threeCanvasBox',
    });
    window.viewer = viewer;

    // 全景初始化
    panorama = new Panorama(viewer);
    window.panorama = panorama;
    // 拖拽到画布内
    let drag = new Drag('threeCanvasBox');
    drag.drop((offsetX: number, offsetY: number) => {
      let position = Common.getPointByScreen(viewer, offsetX, offsetY);
      setCurrentLabelPos(position);
    });
    setPanorama(panorama);

    // 图形绘制初始化
    initShape();

    return () => {
      panorama?.dispose();
      drag.remove();
      setLabels([]);
    };
  }, []);
  return <div id="threeCanvasBox" className={styles.threeCanvasBox}></div>;
};
export default CenterPanel;
