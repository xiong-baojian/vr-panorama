import React, { useEffect, useState, useRef } from 'react';
import { useModel, useParams, history } from 'umi';
import { DoubleLeftOutlined, SettingOutlined, LinkOutlined } from '@ant-design/icons';
import { Button, Typography, Collapse, Select, Tooltip, message, Space, Divider, Tag } from 'antd';
import './index.less';

const { Panel } = Collapse;
const { Option } = Select;
const { Paragraph } = Typography;
const ShapeManagement: React.FC = () => {
  const { shapeInfo, selectedShape, setSelectedShape } = useModel('PanoramicTool.shape', (ret) => ({
    shapeInfo: ret.shapeInfo,
    selectedShape: ret.selectedShape,
    setSelectedShape: ret.setSelectedShape,
  }));

  const { selectMenu } = useModel('PanoramicTool.func', (ret) => ({
    selectMenu: ret.selectMenu,
  }));

  const onChange = (key: string | string[]) => {
    if (key) {
      const item = shapeInfo?.find((item) => item.id == key);
      setSelectedShape(item);
    } else {
      setSelectedShape({});
    }
  };

  return (
    <>
      <div className="ShapeManagement">
        <div className="title">
          图形绘制
          <DoubleLeftOutlined
            className="titleIcon"
            onClick={() => {
              selectMenu(0);
            }}
          />
        </div>
        <div className="content">
          <Collapse ghost onChange={onChange} accordion>
            {shapeInfo?.map((item, index) => {
              return (
                <Panel
                  className={selectedShape.id === item.id ? 'selectedPannel' : ''}
                  header={
                    <div style={{ width: '100%' }}>
                      {`${item.form.lineName}`}
                      <Tag style={{ float: 'right' }} color="#1677ff">
                        {item.form.isGuideLine ? '导览线' : '线'}
                      </Tag>
                    </div>
                  }
                  key={item.id}
                  // extra={
                  //   <SettingOutlined
                  //     style={{
                  //       color: `${selectedShape.id === item.id ? '#1890ff' : '#fff'}`,
                  //     }}
                  //     onClick={(event) => {
                  //       event.stopPropagation();
                  //     }}
                  //   />
                  // }
                >
                  {item.points?.map((point, i) => {
                    return (
                      <div key={i}>
                        <Space>
                          <span className="coord">{point.x}</span>
                          <span className="coord">{point.y}</span>
                          <span className="coord">{point.z}</span>
                          <span className="link">
                            <LinkOutlined />
                          </span>
                        </Space>
                      </div>
                    );
                  })}
                </Panel>
              );
            })}
          </Collapse>
        </div>
      </div>
    </>
  );
};
export default ShapeManagement;
