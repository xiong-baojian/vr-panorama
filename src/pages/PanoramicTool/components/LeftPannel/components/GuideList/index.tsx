import React, { useEffect, useState, useRef } from 'react';
import { useModel, useParams, history } from 'umi';
import { CheckCard } from '@ant-design/pro-components';
import { DoubleLeftOutlined, InfoCircleOutlined } from '@ant-design/icons';
import { Image, Tag, Typography, Tooltip, message, Collapse } from 'antd';
import { sortGuide } from '@/services/swagger/guideController';
import { findByViewangleMap } from '@/services/swagger/ViewangleController';
import './index.less';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
const { Panel } = Collapse;
const { Paragraph, Text } = Typography;

const GuideList: React.FC = () => {
  const params: any = useParams();
  const { instanceId } = params;

  const url = `/msManagement/panoramicConfig/${instanceId}`;

  const { selectMenu } = useModel('PanoramicTool.func', (ret) => ({
    selectMenu: ret.selectMenu,
  }));
  const {
    guidePointlist,
    getGuidePointlist,
    setGuidePointList,
    setCurrentGuidePoint,
    currentGuidePoint,
    setCurrentLineId,
    currentLineId,
  } = useModel('PanoramicTool.timeLine', (ret) => ({
    guidePointlist: ret.guidePointlist,
    setGuidePointList: ret.setGuidePointList,
    getGuidePointlist: ret.getGuidePointlist,
    setCurrentGuidePoint: ret.setCurrentGuidePoint,
    currentGuidePoint: ret.currentGuidePoint,
    setCurrentLineId: ret.setCurrentLineId,
    currentLineId: ret.currentLineId,
  }));

  const [lineList, setLineList] = useState<any>([]);

  const handleDragEnd = async (result: any) => {
    const { draggableId, destination, source } = result;
    // 从原位置移除元素
    const [removed] = guidePointlist.splice(source.index, 1);
    // 将元素插入到目标位置
    guidePointlist.splice(destination.index, 0, removed);
    setGuidePointList(guidePointlist);
    let next = guidePointlist[destination.index + 1] || {};
    let res = await sortGuide({
      sortId: draggableId,
      id: next.id || null,
    });
    if (res?.code == 200) {
      message.success(res?.message);
    }
  };

  const selectGuidePoint = (value) => {
    setCurrentGuidePoint(value);
    if (value) {
      history.push(`${url}/${value.panoramaId}`);
    }
  };

  useEffect(() => {
    renderViewList();

    return () => {
      setCurrentLineId(null);
    };
  }, []);

  /**
   * 视角列表查询
   */
  const renderViewList = async () => {
    await findByViewangleMap({ ems_viewangle_instanceid: instanceId }).then((res) => {
      const list = res.result?.list || [];
      const lineList: any = [];
      list?.forEach((item) => {
        item.ems_viewangle_draws?.forEach((line) => {
          line.viewName = item.ems_viewangle_name;
          line.viewId = item.ems_viewangle_id;
          lineList.push(line);
        });
      });
      setLineList(lineList);
      // setViewList(list);
      // formRef?.current?.setFieldValue('viewList', list);
    });
  };

  return (
    <>
      <div className="GuideList">
        <div className="title">
          导览线路图
          <Tooltip title="拖拽排序">
            <InfoCircleOutlined style={{ verticalAlign: 'middle', margin: '0 5px' }} />
          </Tooltip>
          <DoubleLeftOutlined
            className="titleIcon"
            onClick={() => {
              selectMenu(0);
            }}
          />
        </div>
        <div className="content">
          <Collapse
            ghost
            accordion
            onChange={(key) => {
              if (key) {
                setCurrentLineId(key);
                getGuidePointlist(instanceId, key);
              } else {
                setCurrentLineId(null);
                setGuidePointList([]);
              }
            }}
          >
            {lineList?.map((item, index) => {
              return (
                <Panel
                  className={item.ems_draw_id == currentLineId ? 'selectedPannel' : ''}
                  header={`路线 ${item.ems_draw_name ? '- ' + item.ems_draw_name : ''}`}
                  key={item.ems_draw_id}
                  extra={
                    <Tag
                      color="#1677ff"
                      style={{
                        float: 'right',
                        marginRight: 0,
                        color: '#fff',
                      }}
                    >
                      {item.viewName}
                    </Tag>
                  }
                >
                  <DragDropContext onDragEnd={handleDragEnd}>
                    <Droppable droppableId="droppable-guideList">
                      {(provided) => (
                        <div ref={provided.innerRef} {...provided.droppableProps}>
                          <CheckCard.Group
                            style={{ width: '100%' }}
                            onChange={selectGuidePoint}
                            value={currentGuidePoint}
                          >
                            {guidePointlist?.map((item, index) => {
                              return (
                                <Draggable
                                  draggableId={String(item.id)}
                                  key={String(item.id)}
                                  index={index}
                                >
                                  {(p) => (
                                    <div
                                      ref={p.innerRef}
                                      {...p.draggableProps}
                                      {...p.dragHandleProps}
                                      key={String(item.id)}
                                    >
                                      <CheckCard
                                        value={item}
                                        key={item.id}
                                        avatar={
                                          <div style={{ position: 'relative' }}>
                                            <Image
                                              width={150}
                                              height={100}
                                              src={`/systemfile${item.cover}`}
                                            />
                                            <Text
                                              className="card-name"
                                              style={{
                                                position: 'absolute',
                                                left: 0,
                                                bottom: 0,
                                                width: '100%',
                                                height: 25,
                                                color: '#fff',
                                                fontSize: '14px',
                                              }}
                                              strong
                                              ellipsis={{ tooltip: item.panoramaName }}
                                            >
                                              {item.panoramaName}
                                            </Text>
                                            <Text
                                              style={{
                                                width: '100%',
                                                textAlign: 'right',
                                                position: 'absolute',
                                                right: 0,
                                                top: 0,
                                                color: '#fff',
                                                fontSize: '14px',
                                                backgroundColor: 'rgba(0,0,0,0.4)',
                                                padding: '0 2px',
                                              }}
                                            >
                                              停留时长{item.time}s
                                            </Text>
                                          </div>
                                        }
                                        title={
                                          <div
                                            style={{
                                              display: 'flex',
                                              alignItems: 'center',
                                              width: '100%',
                                            }}
                                          >
                                            <strong>{item.name}</strong>
                                            <Tag
                                              color="#1677ff"
                                              style={{
                                                marginLeft: 'auto',
                                                color: '#fff',
                                              }}
                                            >
                                              {index + 1}/{guidePointlist.length}
                                            </Tag>
                                          </div>
                                        }
                                        description={
                                          <Paragraph ellipsis={{ rows: 3 }}>{item.des}</Paragraph>
                                        }
                                      />
                                    </div>
                                  )}
                                </Draggable>
                              );
                            })}
                          </CheckCard.Group>
                        </div>
                      )}
                    </Droppable>
                  </DragDropContext>
                </Panel>
              );
            })}
          </Collapse>
        </div>
      </div>
    </>
  );
};
export default GuideList;
