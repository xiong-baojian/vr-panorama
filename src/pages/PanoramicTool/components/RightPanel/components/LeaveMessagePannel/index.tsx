import React, { useState, useEffect, useRef } from 'react';
import {
  Tag,
  Segmented,
  Card,
  Button,
  List,
  Table,
  Popconfirm,
  DatePicker,
  Input,
  Typography,
  Tooltip,
  Avatar,
  Space,
  message,
  Switch,
} from 'antd';
import { CheckCard, ProCard } from '@ant-design/pro-components';
import {
  createFromIconfontCN,
  EditOutlined,
  DeleteOutlined,
  LikeOutlined,
  MessageOutlined,
  InfoCircleOutlined,
  DoubleLeftOutlined,
} from '@ant-design/icons';
import { leaveMessageModeOptions } from '../../const';
import { Css2dObject } from '@/utils/three/xThree';
import { useModel, useParams } from 'umi';
import {
  findMessageByMap,
  batchDeleteMessage,
  batchAuditMessage,
} from '@/services/swagger/messageController';
import { setAutoMessage, findInstanceByMap } from '@/services/swagger/instanceController';
import './index.less';

const { Text } = Typography;

const LeaveMessagePannel: React.FC = () => {
  const leaveMessageRef = useRef(null);
  const divObject = useRef<any>(null);
  const [isAutoAudit, setIsAutoAudit] = useState(false);
  const [mode, setMode] = useState<any>(1);
  const [selectedIds, setSelectedIds] = useState<any>([]);
  const [dataSource, setDataSource] = useState<any>([]);
  const [leaveMessageInfo, setLeaveMessageInfo] = useState({ text: '请输入留言内容' });
  const params: any = useParams();
  const { instanceId } = params;

  const { selectMenu } = useModel('PanoramicTool.func', (ret) => ({
    selectMenu: ret.selectMenu,
  }));

  const { panoramaData } = useModel('PanoramicTool.panoramaData', (ret) => ({
    panoramaData: ret.panoramaData,
  }));

  const IconText = ({ icon, text }: { icon: React.FC; text: string }) => (
    <Space>
      {React.createElement(icon)}
      {text}
    </Space>
  );

  const addMessageObject = ({
    info = '',
    position = null,
    draggable = false,
    name = 'leaveMessage',
  }) => {
    const leaveMessageInfo = info?.length > 12 ? info?.substring(0, 13) + '...' : info;
    const leaveMessage: any = leaveMessageRef.current?.cloneNode(true);
    const dataBox = leaveMessage.querySelector('.leaveMessageInfo');
    dataBox.textContent = leaveMessageInfo;
    leaveMessage.style.display = 'flex !important';
    divObject.current.add(leaveMessage, {
      position,
      name,
      draggable: draggable,
    });
  };

  /**
   *  设置是否自动留言
   * @param value
   */
  const handleSetAutoAudit = async (value) => {
    const { code, message: m } = await setAutoMessage({
      ems_instance_id: instanceId,
      ems_instance_isauditmessage: value ? 1 : 0,
    });
    if (code === 200) {
      message.success(m);
    }
    queryIsAutoAudit();
  };

  const queryIsAutoAudit = async () => {
    const { code, result } = await findInstanceByMap({
      ems_instance_id: instanceId,
    });
    if (code === 200) {
      const isauditmessage = result?.list[0]?.ems_instance_isauditmessage;
      const bool = isauditmessage == 0 ? false : true;
      setIsAutoAudit(bool);
    }
  };

  /**
   * 切换 未审核、已审核
   * @param mode
   */
  const changeMode = (mode: string | number) => {
    setSelectedIds([]);
    setMode(mode);
    queryMessage(mode);
  };

  /**
   * 批量删除留言
   * @param ids
   */
  const handleDelete = async (ids) => {
    const { code, message: m } = await batchDeleteMessage({ ems_message_ids: ids });
    if (code === 200) {
      message.success(m);
    } else {
      message.error(m);
    }
    queryMessage(mode);
  };

  const handleAuditMessage = async (ids, flag) => {
    const params = ids.map((item) => {
      return {
        ems_message_id: item,
        ems_message_auditconclusion: flag == 2 ? '通过！' : '未通过！',
        ems_message_auditstatus: flag,
      };
    });
    const { code, message: m } = await batchAuditMessage(params);
    if (code === 200) {
      message.success(m);
    } else {
      message.error(m);
    }
    queryMessage(mode);
  };

  /**
   * 渲染留言
   * @param isaudit
   */
  const queryMessage = async (isaudit) => {
    const { result } = await findMessageByMap({
      pageNum: 1,
      pageSize: 10,
      ems_message_auditstatus: isaudit,
      ems_message_panoramaid: panoramaData?.ems_panorama_id,
    });

    setDataSource(result?.list || []);

    if (divObject.current) {
      divObject.current.removeAll();
    }

    divObject.current = new Css2dObject(window.viewer);
    result?.list?.forEach((item) => {
      addMessageObject({
        info: item.ems_message_info,
        position: JSON.parse(item.ems_message_location || '{x:25,y:0.35,z:-12.82}'),
        draggable: false,
      });
    });
  };

  useEffect(() => {
    if (window.panorama && panoramaData?.ems_panorama_id) {
      queryMessage(mode);
    }
  }, [window.panorama, panoramaData]);

  useEffect(() => {
    queryIsAutoAudit();
    return () => {
      if (divObject.current) {
        divObject.current.removeAll();
      }
    };
  }, []);

  return (
    <>
      <div className="LeaveMessagePannel">
        <div ref={leaveMessageRef} className="leaveMessageBox" style={{ display: 'none' }}>
          <img
            src={require('@/assets/images/panorama/icon/user.jpg')}
            alt=""
            className="leaveMessageImg"
          />
          <span className="leaveMessageInfo">
            {leaveMessageInfo?.text?.length > 12
              ? leaveMessageInfo?.text?.substring(0, 13) + '...'
              : leaveMessageInfo?.text}
          </span>
        </div>
        <div className="title">
          留言管理
          <DoubleLeftOutlined
            className="titleIcon"
            onClick={() => {
              selectMenu(0);
            }}
          />
        </div>
        <div className="content">
          <ProCard
            title="展厅是否自动审核留言"
            extra={
              <Switch
                checked={isAutoAudit}
                checkedChildren="是"
                unCheckedChildren="否"
                onChange={(value) => {
                  handleSetAutoAudit(value);
                  setIsAutoAudit(value);
                }}
              />
            }
            ghost
          ></ProCard>
          <div className="mode">
            <Segmented
              block
              className="ant-segmented-gray"
              onChange={changeMode}
              options={leaveMessageModeOptions}
            />
          </div>
          <div className="leaveMessageList">
            <CheckCard.Group
              multiple
              className="messageList"
              value={selectedIds}
              onChange={(value) => {
                setSelectedIds(value);
              }}
            >
              {dataSource?.map((item: any) => {
                return (
                  <CheckCard
                    style={{ width: '100%' }}
                    avatar={
                      <Avatar
                        src={`/systemfile${item.ems_message_useravatarfile?.ems_sysfile_path}`}
                      />
                    }
                    extra={
                      <Tag
                        color="#1677ff"
                        style={{
                          float: 'right',
                          color: '#fff',
                        }}
                      >
                        {item.ems_message_auditstatus == '1'
                          ? '未审核'
                          : item.ems_message_auditstatus == '2'
                          ? '审核通过'
                          : '审核未通过'}
                      </Tag>
                    }
                    title={<Text>{item.ems_message_createby}</Text>}
                    description={
                      <Tooltip title={item.ems_message_auditconclusion}>
                        <Text>{item.ems_message_info}</Text>
                      </Tooltip>
                    }
                    value={item.ems_message_id}
                    key={item.ems_message_id}
                    actions={[
                      <IconText icon={LikeOutlined} text="156" key="list-vertical-like-o" />,
                      <IconText icon={MessageOutlined} text="2" key="list-vertical-message" />,
                    ]}
                  ></CheckCard>
                );
              })}
            </CheckCard.Group>
          </div>
        </div>
        <div className="bottom">
          <div className="bottom-top">
            <Popconfirm
              title="是否通过！"
              okText="是"
              cancelText="否"
              onCancel={() => {
                if (selectedIds && selectedIds.length > 0) {
                  handleAuditMessage(selectedIds, 3);
                } else {
                  message.info('请选择留言！');
                }
              }}
              onConfirm={() => {
                if (selectedIds && selectedIds.length > 0) {
                  handleAuditMessage(selectedIds, 2);
                } else {
                  message.info('请选择留言！');
                }
              }}
            >
              <Button type="primary" ghost onClick={() => {}} className="btn">
                审核
              </Button>
            </Popconfirm>

            <Popconfirm
              title="是否删除该项！"
              okText="是"
              cancelText="否"
              onConfirm={() => {
                if (selectedIds && selectedIds.length > 0) {
                  handleDelete(selectedIds);
                } else {
                  message.info('请选择留言！');
                }
              }}
            >
              <Button ghost danger className="btn" onClick={() => {}}>
                删除
              </Button>
            </Popconfirm>
          </div>
        </div>
      </div>
    </>
  );
};
export default LeaveMessagePannel;
