import {
  ProCard,
  ProFormSwitch,
  ProForm,
  ProFormTextArea,
  ProFormSelect,
  ProFormSlider,
  ProFormItem,
} from '@ant-design/pro-components';
import { viewOptions } from '../../const';
import { useModel } from 'umi';
import { Segmented, Button, message } from 'antd';
import type { ProFormInstance } from '@ant-design/pro-components';
import { useState, useRef, useEffect } from 'react';
import InputSlider from '@/components/InputSlider';
import { updatePanorama } from '@/services/swagger/panoramaController';
import { Camera, Common } from '@/utils/three/xThree';
import './index.less';
const ViewPanel = () => {
  const {
    panoramaData,
    threeImgData,
    queryPanoramaView,
    setThreeImgData,
    setNorthViewImgData,
    northViewImgData,
  } = useModel('PanoramicTool.panoramaData', (ret) => ({
    panoramaData: ret.panoramaData,
    threeImgData: ret.threeImgData,
    queryPanoramaView: ret.queryPanoramaView,
    setThreeImgData: ret.setThreeImgData,
    setNorthViewImgData: ret.setNorthViewImgData,
    northViewImgData: ret.northViewImgData,
  }));
  const [mode, setMode] = useState<number | string>('base');
  const { panorama } = useModel('PanoramicTool.label', (ret) => ({
    panorama: ret.panorama,
  }));
  const formRef = useRef<ProFormInstance>();

  /**
   * 基础 / 其他
   * @param mode
   */
  const changeMode = (mode: string | number) => {
    if (mode === 'base') {
      returnInitView();
      const imgData = Common.getThreeCanvasImgData(window.viewer);
      setThreeImgData(imgData);
    } else {
      returnNorthView();
      const imgData = Common.getThreeCanvasImgData(window.viewer);
      setNorthViewImgData(imgData);
    }
    setMode(mode);
  };

  /**
   * 表单改变标签实体样式
   * @param changeValues
   * @param values
   */
  const changeValues = (changeValues: any, values: any) => {
    panorama.changeCameraParams(values);
  };

  /**
   * 保存
   */
  const handleSave = () => {
    let values = formRef.current?.getFieldsValue();
    updatePanorama({
      ems_panorama_id: panoramaData?.ems_panorama_id,
      ems_panorama_fov: JSON.stringify(values),
    }).then((res) => {
      if (res?.code == 200) {
        message.success('保存成功！');
      } else {
        message.error(res.code);
      }
      queryPanoramaView(panoramaData?.ems_panorama_id);
    });
  };

  const returnInitView = () => {
    if (panoramaData?.ems_panorama_initview) {
      const cameraPosition = JSON.parse(panoramaData?.ems_panorama_initview);
      Camera.resetCamera(cameraPosition, 50);
    }
  };

  const returnNorthView = () => {
    if (panoramaData?.ems_panorama_compass) {
      const cameraPosition = JSON.parse(panoramaData?.ems_panorama_compass);
      Camera.resetCamera(cameraPosition, 50);
    }
  };

  useEffect(() => {
    if (panoramaData) {
      if (panoramaData.ems_panorama_fov) {
        let values = JSON.parse(panoramaData.ems_panorama_fov);
        formRef?.current?.setFieldsValue(values);
        panorama.changeCameraParams(values);
      } else {
        formRef?.current?.resetFields();
      }
    } else {
      formRef?.current?.resetFields();
    }
  }, [panoramaData]);

  return (
    <div className="ViewPanel">
      <div className="title">视角</div>
      <div className="content">
        <div className="mode">
          <Segmented
            block
            className="ant-segmented-gray"
            onChange={changeMode}
            options={viewOptions}
          />
        </div>
        <ProForm
          formRef={formRef}
          onValuesChange={changeValues}
          colon={false}
          labelAlign="left"
          labelCol={{ span: 6 }}
          layout="horizontal"
          submitter={false}
        >
          <div className={`form ${mode == 'base' ? 'show' : ''}`}>
            <ProCard
              title={<div className="card-title">初始视角</div>}
              extra={
                <Button
                  type="primary"
                  size="small"
                  onClick={() => {
                    returnInitView();
                  }}
                >
                  回到初始视角
                </Button>
              }
              ghost
            >
              <div className="card-form">
                <div className="viewCanvas">
                  <img src={threeImgData} alt="" />
                </div>
              </div>
            </ProCard>
            <ProCard title={<div className="card-title">视场角（FOV）范围</div>} ghost>
              <div className="card-form">
                <ProFormItem name="fovs" initialValue={[10, 50]}>
                  <InputSlider
                    leftLabel="最近"
                    rightLabel="最远"
                    min={0.1}
                    max={100}
                    step={0.1}
                    marks={{
                      10: '10',
                      50: '50',
                    }}
                    defaultValue={[10, 50]}
                  />
                </ProFormItem>
              </div>
            </ProCard>
            <ProCard title={<div className="card-title">视角限制</div>} ghost>
              <div className="card-form">
                <div className="formTitle">水平视角限制</div>
                <ProFormItem name="azimuthAngle" initialValue={[-180, 180]}>
                  <InputSlider
                    leftLabel="最左"
                    rightLabel="最右"
                    min={-180}
                    max={180}
                    defaultValue={[-180, 180]}
                  />
                </ProFormItem>
                <div className="formTitle">垂直视角限制</div>
                <ProFormItem name="polarAngle" initialValue={[-90, 90]}>
                  <InputSlider
                    leftLabel="最下"
                    rightLabel="最上"
                    min={-90}
                    max={90}
                    defaultValue={[-90, 90]}
                  />
                </ProFormItem>
              </div>
            </ProCard>
          </div>
          <div className={`form ${mode == 'other' ? 'show' : ''}`}>
            <ProCard
              title={<div className="card-title">正北视角</div>}
              extra={
                <Button
                  type="primary"
                  size="small"
                  onClick={() => {
                    returnNorthView();
                  }}
                >
                  回到正北视角
                </Button>
              }
              ghost
            >
              <div className="card-form">
                <div className="viewCanvas">
                  <img src={northViewImgData} alt="" />
                </div>
              </div>
            </ProCard>
          </div>
        </ProForm>
      </div>
      <div className="bottom">
        <div className="bottom-top">
          <Button type="primary" ghost onClick={handleSave} className="btn">
            保存
          </Button>
        </div>
        <Button ghost block onClick={() => {}} className="btn btn-cancel">
          取消
        </Button>
      </div>
    </div>
  );
};
export default ViewPanel;
