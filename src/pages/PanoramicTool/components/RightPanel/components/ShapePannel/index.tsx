import { useModel, useParams } from 'umi';
import { Segmented, Space, Radio, Typography, Button, Popconfirm, message } from 'antd';
import type { ProFormInstance } from '@ant-design/pro-components';
import {
  ProCard,
  ProFormSwitch,
  ProForm,
  ProFormDigit,
  ProFormDependency,
  ProFormSelect,
  ProFormSlider,
  ProFormItem,
  ProFormColorPicker,
  ProFormText,
  ProFormList,
} from '@ant-design/pro-components';
import {
  findByViewangleMap,
  insertViewangle,
  updateViewangle,
  deleteViewangle,
} from '@/services/swagger/ViewangleController';
import {
  insertDraw,
  updateDraw,
  findDrawByMap,
  deleteDraw,
} from '@/services/swagger/DrawController';
import { createFromIconfontCN, InfoCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import { Draw } from '@/utils/three/xThree';
import { lineTypeOptions, shapeOptions } from '../../const';
import './index.less';
import { useState, useRef, useEffect } from 'react';
import { uniqueId, isEmpty } from 'lodash';
const IconFont = createFromIconfontCN({
  scriptUrl: ['/iconfont/iconfont.js'],
});
const { Title } = Typography;

const ShapePannel = () => {
  const params: any = useParams();
  const { instanceId, id } = params;
  const panoramaId = id;
  // react
  const actionRef = useRef<any>();
  const formRef = useRef<ProFormInstance>();
  const [mode, setMode] = useState<number | string>('base');
  const [viewId, setViewId] = useState(null);
  const [viewList, setViewList] = useState<any>([]);

  const {
    changeLineStyle,
    selectedShape,
    setSelectedShape,
    refreshShapeList,
    startDrawLine,
    deleteLine,
    getLinePoints,
  } = useModel('PanoramicTool.shape', (ret) => ({
    changeLineStyle: ret.changeLineStyle,
    selectedShape: ret.selectedShape,
    setSelectedShape: ret.setSelectedShape,
    refreshShapeList: ret.refreshShapeList,
    startDrawLine: ret.startDrawLine,
    deleteLine: ret.deleteLine,
    getLinePoints: ret.getLinePoints,
  }));

  /**
   * 表单改变标签实体样式
   * @param changeValues
   * @param values
   */
  const changeValues = async (changeValues: any, values: any) => {
    changeLineStyle(changeValues);
  };

  /**
   * 基础 / 其他
   * @param mode
   */
  const changeMode = (mode: string | number) => {
    setMode(mode);
  };

  /**
   * 保存对象（添加 or 编辑）
   */
  const handleSave = async () => {
    const values = formRef?.current?.getFieldsValue();
    const { isGuideLine, lineName, lineView } = values;
    values.viewList = null;
    // 判断是否选中实体 （编辑）
    let res: any;
    if (selectedShape && !isEmpty(selectedShape)) {
      const points = getLinePoints(selectedShape.id, 'edit');
      res = await updateDraw({
        ems_draw_data: JSON.stringify(points),
        ems_draw_jsondata: JSON.stringify(values),
        ems_draw_isguide: isGuideLine ? 1 : 0,
        ems_draw_name: lineName || '',
        ems_draw_type: 0, // 线
        ems_draw_viewangleid: lineView || null,
        ems_draw_panoramaid: panoramaId,
        ems_draw_id: selectedShape.id,
      });
    } else {
      const id = uniqueId();
      const points = getLinePoints(id, 'add');
      // 判断是否有实体
      if (points && points.length > 0) {
        res = await insertDraw({
          ems_draw_data: JSON.stringify(points),
          ems_draw_jsondata: JSON.stringify(values),
          ems_draw_isguide: isGuideLine ? 1 : 0,
          ems_draw_name: lineName || '',
          ems_draw_type: 0, // 线
          ems_draw_viewangleid: lineView || null,
          ems_draw_panoramaid: panoramaId,
        });
      } else {
        message.info('请先绘制图形！');
        return;
      }
    }
    if (res?.code === 200) {
      message.success('保存成功！');
    } else {
      message.error('保存失败！');
    }
    refreshShapeList(panoramaId);
    handleReset();
  };

  /**
   * 删除图形对象
   */
  const handleDelete = async () => {
    if (selectedShape && !isEmpty(selectedShape)) {
      deleteLine(selectedShape.id);
      const res = await deleteDraw({
        ems_draw_id: selectedShape.id,
      });
      if (res?.code === 200) {
        message.success('删除成功！');
      } else {
        message.error('删除失败！');
      }
      // 刷新
      refreshShapeList(panoramaId);
    }
    handleReset();
  };

  /**
   * 给导览线设置视角
   * @param e
   */
  const handleChangeView = (e) => {
    setViewId(e.target.value);
  };

  useEffect(() => {
    if (selectedShape && !isEmpty(selectedShape)) {
      const values = selectedShape.form;
      formRef?.current?.setFieldsValue(values);
      const viewId = values.lineView;
      setViewId(viewId);
    } else {
      formRef?.current?.resetFields();
      setViewId(null);
    }
  }, [selectedShape]);

  /**
   * 重置
   */
  const handleReset = () => {
    setSelectedShape({});
    formRef?.current?.resetFields();
    deleteLine();
    setViewId(null);
  };

  /**
   * 绘制线
   */
  const drawLine = () => {
    handleReset();
    startDrawLine();
  };

  /**
   * 视角列表查询
   */
  const renderViewList = async () => {
    await findByViewangleMap({ ems_viewangle_instanceid: instanceId }).then((res) => {
      const list = res.result?.list || [];
      setViewList(list);
      formRef?.current?.setFieldValue('viewList', list);
    });
  };

  useEffect(() => {
    renderViewList();
  }, []);

  return (
    <div className="ShapePannel">
      <div className="title">{isEmpty(selectedShape) ? '新增' : '编辑'}</div>
      <div className="content">
        <Button
          icon={<IconFont type="icon-xian" />}
          type="primary"
          onClick={() => {
            drawLine();
          }}
        >
          线绘制
        </Button>
        <div className="mode">
          <Segmented
            block
            className="ant-segmented-gray"
            onChange={changeMode}
            options={shapeOptions}
          />
        </div>
        <ProForm
          formRef={formRef}
          onValuesChange={changeValues}
          colon={false}
          labelAlign="left"
          labelCol={{ span: 5 }}
          layout="horizontal"
          submitter={false}
        >
          <div className={`form ${mode == 'base' ? 'show' : ''}`}>
            <ProCard
              title={
                <div className="card-title">
                  <InfoCircleOutlined />
                  基本信息
                </div>
              }
              ghost
            >
              <div className="card-form">
                <ProFormText name="lineName" label="名称" placeholder="请输入名称" />
                <ProFormDigit label="线宽" name="lineWidth" initialValue={5} min={1} max={100} />
                <ProFormSelect
                  name="lineType"
                  label="线型"
                  initialValue={0}
                  options={lineTypeOptions}
                />
                <ProFormColorPicker label="颜色" name="lineColor" initialValue="#fff" />
                <ProFormSlider
                  name="lineCurve"
                  label="曲率"
                  min={0}
                  max={1}
                  initialValue={0}
                  marks={{
                    0: '0',
                    1: '1',
                  }}
                  step={0.01}
                />
                <ProFormSwitch
                  label="闭环"
                  initialValue={false}
                  name="lineLink"
                  checkedChildren={'是'}
                  unCheckedChildren={'否'}
                />
                <ProFormSwitch
                  label="显示面"
                  initialValue={false}
                  name="lineToFace"
                  checkedChildren={'是'}
                  unCheckedChildren={'否'}
                />
                <ProFormColorPicker label="面颜色" name="faceColor" initialValue="#fff" />
                <ProFormSwitch
                  label="面闪烁"
                  initialValue={false}
                  name="faceBlink"
                  checkedChildren={'是'}
                  unCheckedChildren={'否'}
                />
                <ProFormSlider
                  name="faceOpacity"
                  label="透明度"
                  min={0}
                  max={1}
                  initialValue={0.5}
                  marks={{
                    0: '0',
                    1: '1',
                  }}
                  step={0.01}
                />
              </div>
            </ProCard>
          </div>
          <div className={`form ${mode == 'other' ? 'show' : ''}`}>
            <ProCard
              title={
                <div className="card-title">
                  <IconFont type="icon-luxian" /> 是否作为导览线
                </div>
              }
              extra={
                <ProFormSwitch
                  initialValue={false}
                  name="isGuideLine"
                  noStyle
                  checkedChildren={'是'}
                  unCheckedChildren={'否'}
                />
              }
              ghost
            >
              <ProFormDependency name={['isGuideLine']}>
                {({ isGuideLine }) => {
                  if (isGuideLine) {
                    formRef?.current?.setFieldValue('viewList', viewList);
                    return (
                      <div className="card-form">
                        <Space>
                          <IconFont
                            type="icon-shijiaoshoucang"
                            style={{ color: '#fff', fontSize: 20 }}
                          ></IconFont>
                          <Title level={5} style={{ color: '#fff', fontSize: 16 }}>
                            请选择视角
                          </Title>
                        </Space>
                        <ProFormItem name="lineView" label="">
                          <Radio.Group onChange={handleChangeView} value={viewId}>
                            <ProFormList
                              actionRef={actionRef}
                              name="viewList"
                              label=""
                              deleteIconProps={{
                                Icon: CloseCircleOutlined,
                                tooltipText: '删除',
                              }}
                              copyIconProps={false}
                              alwaysShowItemLabel
                              creatorButtonProps={{
                                creatorButtonText: '新增视角',
                              }}
                              actionGuard={{
                                beforeAddRow: async (defaultValue, insertIndex, count) => {
                                  const res = await insertViewangle({
                                    ems_viewangle_instanceid: instanceId,
                                    ems_viewangle_name: '',
                                  });
                                  if (res.code === 200) {
                                    message.success('新增成功！');
                                    return true;
                                  } else {
                                    message.error('新增失败！');
                                    return false;
                                  }
                                },
                                beforeRemoveRow: async (index, count) => {
                                  const row = actionRef.current?.get(index);
                                  if (viewId === row.ems_viewangle_id) {
                                    setViewId(null);
                                  }
                                  const res = await deleteViewangle({
                                    ems_view_id: row.ems_viewangle_id,
                                  });
                                  if (res.code === 200) {
                                    message.success('删除成功！');
                                    return true;
                                  } else {
                                    message.error('删除失败！');
                                    return false;
                                  }
                                },
                              }}
                              onAfterAdd={() => {
                                renderViewList();
                              }}
                              onAfterRemove={() => {
                                renderViewList();
                              }}
                            >
                              {(meta, index, action, count) => {
                                const row = action.getCurrentRowData();
                                return (
                                  <Space key="row" align="center">
                                    <Radio
                                      value={row.ems_viewangle_id}
                                      style={{ marginBottom: 15 }}
                                    ></Radio>
                                    <ProFormText
                                      name="ems_viewangle_name"
                                      fieldProps={{
                                        onBlur: async () => {
                                          const data = action.getCurrentRowData();
                                          const res = await updateViewangle(data);
                                          await renderViewList();
                                          if (res.code === 200) {
                                            message.success('编辑成功！');
                                          } else {
                                            message.error('编辑失败！');
                                          }
                                        },
                                      }}
                                    />
                                  </Space>
                                );
                              }}
                            </ProFormList>
                          </Radio.Group>
                        </ProFormItem>
                      </div>
                    );
                  } else {
                    return <></>;
                  }
                }}
              </ProFormDependency>
            </ProCard>
          </div>
        </ProForm>
      </div>
      <div className="bottom">
        <div className="bottom-top">
          <Button type="primary" ghost onClick={handleSave} className="btn">
            保存
          </Button>
          <Popconfirm title="是否删除该项！" okText="是" cancelText="否" onConfirm={handleDelete}>
            <Button ghost danger className="btn">
              删除
            </Button>
          </Popconfirm>
        </div>
        <Button ghost block className="btn cancel-btn" onClick={handleReset}>
          取消
        </Button>
      </div>
    </div>
  );
};
export default ShapePannel;
