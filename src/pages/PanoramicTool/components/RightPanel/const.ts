export const layoutOptions = [
  {
    label: '基础信息',
    value: 'base',
  },
  {
    label: '开场设置',
    value: 'begin',
  },
  {
    label: '界面功能',
    value: 'func',
  },
];

export const modeOptions = [
  {
    label: '基础',
    value: 'base',
  },
  {
    label: '高级',
    value: 'high',
  },
  {
    label: '监测数据',
    value: 'device',
  },
  // {
  //   label: '运维任务',
  //   value: 'task',
  // },
];

export const viewOptions = [
  {
    label: '初始视角',
    value: 'base',
  },
  {
    label: '正北视角',
    value: 'other',
  },
];

export const shapeOptions = [
  {
    label: '基础',
    value: 'base',
  },
  {
    label: '其他',
    value: 'other',
  },
];

export const guideModeOptions = [
  {
    label: '基础',
    value: 'base',
  },
  {
    label: '高级',
    value: 'high',
  },
];
export const styleOptions = ['B', 'U', 'I'];
export const lineTypeOptions = [
  { label: '实线', value: 0 },
  { label: '虚线', value: 1 },
  { label: '箭头', value: 2 },
  { label: '闪烁线', value: 3 },
];
export const fontSizeOptions = [
  { label: '4px', value: 4 },
  { label: '6px', value: 6 },
  { label: '8px', value: 8 },
  { label: '10px', value: 10 },
  { label: '12px', value: 12 },
  { label: '14px', value: 14 },
  { label: '16px', value: 16 },
  { label: '18px', value: 18 },
  { label: '20px', value: 20 },
];
export const logoOptions = [
  {
    label: '自定义',
    value: 1,
  },
  {
    label: '系统默认',
    value: 2,
  },
  {
    label: '无',
    value: 0,
  },
];

export const metaOptions = [
  {
    label: '清馨',
    imgUrl: require('@/assets/images/meta/meta1.png'),
    value: 1,
  },
  {
    label: '清缘',
    imgUrl: require('@/assets/images/meta/meta2.png'),
    value: 2,
  },
  {
    label: '源丞',
    imgUrl: require('@/assets/images/meta/meta3.png'),
    value: 3,
  },
];

export const startAnimationOptions = [
  {
    label: '全局进入',
    value: 1,
  },
  {
    label: '小行星动画',
    value: 2,
  },
];
export const templateOptions = [
  {
    label: '模板一',
    value: 1,
    type: 0,
  },
  {
    label: '模板二',
    value: 2,
    type: 0,
  },
  {
    label: '模板一',
    value: 3,
    type: 1,
  },
  {
    label: '模型模板',
    value: 4,
    type: 0,
  },
  {
    label: '界面模板一',
    value: 5,
    type: 0,
  },
];
export const sceneModeOptions = [
  {
    label: '缩略图',
    value: 0,
  },
  {
    label: '图标',
    value: 1,
  },
];
export const switchEffectOptions = [
  {
    label: '淡入淡出',
    value: 0,
  },
  {
    label: '缩放过渡',
    value: 1,
  },
];

export const leaveMessageModeOptions = [
  {
    label: '未审核',
    value: 1,
  },
  {
    label: '审核通过',
    value: 2,
  },
  {
    label: '审核未通过',
    value: 3,
  },
];
