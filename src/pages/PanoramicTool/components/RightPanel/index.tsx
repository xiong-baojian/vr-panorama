import { useModel } from 'umi';
import HostTagPanel from './components/HostTagPanel';
import ViewPanel from './components/ViewPanel';
import LayoutPanel from './components/LayoutPanel';
import GuidePanel from './components/GuidePanel';
import ShapePannel from './components/ShapePannel';
import LeaveMessagePannel from './components/LeaveMessagePannel';
import './index.less';

const RightPanel = () => {
  const { menuId } = useModel('PanoramicTool.func');
  return (
    <div className="rightPanel">
      {menuId == 1 && <LayoutPanel />}
      {menuId == 2 && <ViewPanel />}
      {menuId == 3 && <ShapePannel />}
      {menuId == 4 && <HostTagPanel />}
      {menuId == 5 && <GuidePanel />}
      {menuId == 6 && <LeaveMessagePannel />}
    </div>
  );
};
export default RightPanel;
