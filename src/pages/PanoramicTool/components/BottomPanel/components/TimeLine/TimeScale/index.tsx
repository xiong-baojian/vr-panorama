import { useEffect } from 'react';
import style from './index.less';
import * as echarts from 'echarts';

const TimeScale = () => {
  useEffect(() => {
    var chartDom = document.getElementById('timeEchart');
    var myChart = echarts.init(chartDom);
    var option;

    option = {
      grid: {
        left: '0%',
        right: '0%',
        top: '0%',
        bottom: '50%',
        containLabel: true,
      },
      xAxis: {
        name: '',
        min: 0,
        max: 120,
        splitNumber: 100,
        axisTick: {
          show: true,
          length: 10,
        },
        axisLabel: {
          formatter: '  {value}s',
          textStyle: {
            color: 'gray',
          },
          align: 'left',
        },
        minorTick: {
          show: true,
          splitNumber: 10,
        },
        splitLine: {
          show: false,
        },
      },
      yAxis: {
        show: false,
      },
      series: [],
    };
    option && myChart.setOption(option);
  }, []);
  return <div className={style.timeScale} id="timeEchart"></div>;
};
export default TimeScale;
