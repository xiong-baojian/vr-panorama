import { useEffect, useState, useRef } from 'react';
import {
  Tabs,
  Card,
  List,
  Typography,
  Avatar,
  Skeleton,
  Menu,
  Dropdown,
  message,
  Tooltip,
  Button,
} from 'antd';
import { createFromIconfontCN, PlusOutlined } from '@ant-design/icons';
import { findGroupByInstance } from '@/services/swagger/InstancePanoramaGroupController';
import { history, useModel, useParams } from 'umi';
import {
  findPanoramaByMapV1,
  setHomePage,
  deletePanorama,
  sort,
} from '@/services/swagger/panoramaController';
import styles from './index.less';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import UploadPanorama from './UploadPanorama';

const { Meta } = Card;
const { Text } = Typography;

const IconFont = createFromIconfontCN({
  scriptUrl: ['/iconfont/iconfont.js'],
});
const PanoramaList = () => {
  const uploadRef = useRef<any>();
  const params: any = useParams();
  const { instanceId } = params;
  const [items, setItems] = useState<any>([]);
  const [topItems, setTopItems] = useState<any>([]);
  const [group, setGroup] = useState<any>([]);

  const url = `/msManagement/panoramicConfig/${instanceId}`;

  const { panoramaData } = useModel('PanoramicTool.panoramaData', (ret) => ({
    panoramaData: ret.panoramaData,
  }));
  const [loading, setLoading] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const [activeKey, setActiveKey] = useState<string>(''); // 使用对象来存储每个 Card 的菜单可见状态
  const [menuVisible, setMenuVisible] = useState({}); // 使用对象来存储每个 Card 的菜单可见状态
  const scrollRef = useRef<HTMLDivElement>(null);

  const handleCardClick = (event, item: API.PanoramaVo) => {
    if (event.button === 2) {
      event.preventDefault(); // 阻止默认右键点击事件
      const newMenuVisible = { ...menuVisible, [String(item.ems_panorama_id)]: true };
      setMenuVisible(newMenuVisible);
    }
  };

  const handleMenuVisibleChange = (visible, item: API.PanoramaVo) => {
    const newMenuVisible = { ...menuVisible, [String(item.ems_panorama_id)]: visible };
    setMenuVisible(newMenuVisible);
  };

  //菜单项点击事件
  const handleMenuClick = async (event: any, item: API.PanoramaVo) => {
    const newMenuVisible = { ...menuVisible, [String(item.ems_panorama_id)]: false };
    setMenuVisible(newMenuVisible);
    let res: any;
    if (event.key == 1) {
      res = await setHomePage({
        ems_panorama_id: item.ems_panorama_id,
        ems_instance_id: instanceId,
      });
    } else if (event.key == 3) {
      res = await deletePanorama({
        ems_panorama_id: item.ems_panorama_id || 0,
      });
      // history.push(`${url}/${item.ems_panorama_id}`);
    }
    if (res?.code == 200) {
      message.success(res?.message);
      request(activeKey);
    }
  };

  const request = (activeKey = -1) => {
    let params = {
      ems_panorama_groupid: activeKey,
      ems_panorama_instanceid: instanceId,
    };
    if (activeKey == -1 || !activeKey) {
      delete params.ems_panorama_groupid;
    }
    setLoading(true);
    findPanoramaByMapV1(params).then((res) => {
      let list = res?.result?.list || [];
      setLoading(false);
      setDataSource(list);
    });
  };

  useEffect(() => {
    request(activeKey);
  }, [activeKey]);

  const handleDragEnd = async (result: any) => {
    const { draggableId, destination, source } = result;
    let arr = dataSource;
    // 从原位置移除元素
    const [removed] = arr.splice(source.index, 1);
    // 将元素插入到目标位置
    arr.splice(destination.index, 0, removed);
    setDataSource(arr);
    let next = arr[destination.index + 1] || {};
    let res = await sort({
      sortId: draggableId,
      id: next.ems_panorama_id || null,
    });
    if (res?.code == 200) {
      message.success(res?.message);
      request();
    }
  };

  /**
   * 滚轮事件
   * @param event
   */
  const handleWheel = (event: React.WheelEvent<HTMLDivElement>) => {
    // event.preventDefault();
    const scrollLeft = scrollRef.current?.scrollLeft ?? 0;
    const delta = Math.max(-1, Math.min(1, event.deltaY)) * 150;
    scrollRef.current!.scrollLeft = scrollLeft + delta;
  };

  const onChange = (activeKey: string) => {
    setActiveKey(activeKey);
    changeLeftItems(activeKey);
  };

  const onTopTabsChange = (activeKey) => {
    setActiveKey(activeKey);
  };

  const changeLeftItems = (activeKey: string, data = group) => {
    let arr: any = [];
    if (activeKey == '-1') {
      data?.forEach((item) => {
        arr.push(...item.childrens);
      });
    } else {
      data?.forEach((item) => {
        if (item.ems_instance_panorama_group_id === Number(activeKey)) {
          arr = item.childrens;
        }
      });
    }
    arr = arr?.map((item) => {
      return {
        label: item.ems_instance_panorama_group_name,
        children: '',
        key: item.ems_instance_panorama_group_id,
      };
    });
    setTopItems(arr);
  };

  const refresh = () => {
    findGroupByInstance({
      ems_instance_panorama_group_instanceid: instanceId,
    }).then((res) => {
      const tabItems: any = [
        {
          label: '全部',
          children: '',
          key: -1,
        },
      ];
      res.result?.forEach((item) => {
        tabItems.push({
          label: item.ems_instance_panorama_group_name,
          children: '',
          key: item.ems_instance_panorama_group_id,
        });
      });
      setItems(tabItems);
      setActiveKey(tabItems[0].key);
      setGroup(res.result || []);
      changeLeftItems('-1', res.result);
    });
  };
  useEffect(() => {
    refresh();
  }, []);

  return (
    <div className={styles.PanoramaList}>
      {/* <Button className={styles.btnAddGroup}>添加分组</Button> */}
      <Tabs tabBarGutter={0} items={topItems} onChange={onTopTabsChange} />
      <div style={{ display: 'flex' }}>
        <Tabs
          tabPosition="left"
          tabBarGutter={0}
          onChange={onChange}
          items={items}
          activeKey={activeKey}
        />
        <div className={styles.bottomPanelList} onWheel={handleWheel} ref={scrollRef}>
          <div className={styles.bottomPanelListIn}>
            <DragDropContext onDragEnd={handleDragEnd}>
              <Droppable droppableId="droppable-1" direction="horizontal">
                {(provided) => (
                  <div
                    ref={provided.innerRef}
                    {...provided.droppableProps}
                    style={{ width: '100%', height: '100%' }}
                  >
                    <Skeleton active loading={loading}>
                      <List<API.PanoramaVo>
                        style={{ width: '100%', height: '100%' }}
                        rowKey="ems_panorama_id"
                        grid={{
                          gutter: 0,
                        }}
                        dataSource={dataSource}
                        renderItem={(item: API.PanoramaVo, i) => {
                          if (item && item.ems_panorama_id) {
                            return (
                              <List.Item key={item.ems_panorama_id}>
                                <Draggable
                                  draggableId={String(item.ems_panorama_id)}
                                  key={i}
                                  index={i}
                                >
                                  {(p) => (
                                    <div
                                      ref={p.innerRef}
                                      {...p.draggableProps}
                                      {...p.dragHandleProps}
                                      key={String(item.ems_panorama_id)}
                                    >
                                      <Dropdown
                                        visible={menuVisible[item.ems_panorama_id]}
                                        onVisibleChange={(visible) =>
                                          handleMenuVisibleChange(visible, item)
                                        }
                                        overlay={
                                          <Menu
                                            onClick={(event) => {
                                              handleMenuClick(event, item);
                                            }}
                                          >
                                            <Menu.Item key="1">设置为首页</Menu.Item>
                                            <Menu.Item key="2">编辑</Menu.Item>
                                            <Menu.Item key="3">删除</Menu.Item>
                                          </Menu>
                                        }
                                        trigger={['contextMenu']}
                                      >
                                        <Card
                                          bodyStyle={{ padding: 0 }}
                                          onContextMenu={() => {
                                            history.push(`${url}/${item.ems_panorama_id}`);
                                            handleMenuVisibleChange(true, item);
                                          }}
                                          onClick={(event) => {
                                            handleCardClick(event, item);
                                            history.push(`${url}/${item.ems_panorama_id}`);
                                          }}
                                          className={
                                            item.ems_panorama_id == panoramaData?.ems_panorama_id
                                              ? styles.selectedCard
                                              : styles.card
                                          }
                                          size="small"
                                          hoverable
                                          cover={
                                            <img
                                              style={{ width: 160, height: 100 }}
                                              alt="暂无"
                                              src={`/systemfile${item?.ems_panorama_cover?.ems_sysfile_path}`}
                                            />
                                          }
                                        >
                                          {item.ems_panorama_default == 1 && (
                                            <Tooltip title="首页进入">
                                              <IconFont
                                                type="icon-enter"
                                                className={styles.enterIcon}
                                              />
                                            </Tooltip>
                                          )}

                                          <Text
                                            style={{
                                              position: 'absolute',
                                              textAlign: 'center',
                                              background: 'rgba(0, 0, 0, 0.3)',
                                              left: 0,
                                              bottom: 0,
                                              width: '100%',
                                              height: 25,
                                              color: '#fff',
                                              fontSize: '14px',
                                            }}
                                            color="#fff"
                                            strong
                                            ellipsis={{ tooltip: item.ems_panorama_name }}
                                          >
                                            {item.ems_panorama_name}
                                          </Text>
                                        </Card>
                                      </Dropdown>
                                    </div>
                                  )}
                                </Draggable>
                              </List.Item>
                            );
                          }
                          return <List.Item></List.Item>;
                        }}
                      />
                    </Skeleton>
                  </div>
                )}
              </Droppable>
            </DragDropContext>
          </div>
        </div>
        <div className={styles.btnAddBox}>
          <Button
            className={styles.btnAdd}
            onClick={() => {
              uploadRef?.current?.show();
            }}
            type="ghost"
          >
            <PlusOutlined />
            <br />
            添加全景
          </Button>
        </div>
      </div>
      <UploadPanorama
        ref={uploadRef}
        levelId={activeKey}
        instanceId={instanceId}
        refreshList={() => {
          request();
        }}
      />
    </div>
  );
};
export default PanoramaList;
