import React, { useEffect, useRef, useState } from 'react';
import { useModel, useRequest } from 'umi';
import moment from 'moment';
import 'moment/locale/zh-cn';
moment.locale('zh-cn');

import {
  Col,
  Form,
  DatePicker,
  Row,
  Select,
  Input,
  Alert,
  Segmented,
  Button,
  Tooltip,
  Card,
  Tag,
  Cascader,
  Popover,
  Space,
  Typography,
} from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { CheckCard, ProCard, ProFormDependency } from '@ant-design/pro-components';
import {
  createFromIconfontCN,
  AppstoreOutlined,
  BarsOutlined,
  InfoCircleOutlined,
  UpOutlined,
  DownOutlined,
} from '@ant-design/icons';
import styles from './style.less';
import StandardFormRow from '../../components/StandardFormRow';
import TagSelect from '../../components/TagSelect';
import StationCard from './components/StationCard';
import StationList from './components/StationList';
import { findOpenindustryByMap } from '@/services/swagger/openindustryController';
import { findOpensubjectByMap } from '@/services/swagger/opensubjectController';
import { findOpenfacilityByMap } from '@/services/swagger/openfacilityController';
import StationMap from './components/StationMap';
const { Text } = Typography;
const { Option } = Select;
const { RangePicker } = DatePicker;
const IconFont = createFromIconfontCN({
  scriptUrl: ['/iconfont/iconfont.js'],
});
const { Meta } = Card;
const { CheckableTag } = Tag;
const topColResponsiveProps = {
  xs: 24,
  sm: 12,
  md: 12,
  lg: 12,
  xl: 6,
  style: {
    marginTop: 10,
  },
};

const TableList: React.FC = (props) => {
  const { isMobile } = useModel('mobile', (ret) => ({
    isMobile: ret.isMobile,
  }));

  const formRef = useRef<any>();
  const stationCardRef = useRef<any>();
  const stationListRef = useRef<any>();
  const [selectedIndustryId, setSelectedIndustryId] = useState<string | number | null>(null); // 开放行业
  const [selectedTag, setSelectedTag] = useState<string | number | null>(null); // 开放设施
  const [selectedThemeId, setSelectedThemeId] = useState<string | number | null>(null); // 开放主题
  const [params, setParams] = useState<any>({}); // 查询参数
  const [viewType, setViewType] = useState<string | number>('card'); //视图类型切换 卡片/列表
  const [showSearchFilter, setShowSearchFiltere] = useState(false);
  const [more, setMore] = useState(false);

  const handleFormSubmit = (value: string) => {
    const p = {
      ...params,
      nameOrNo: value,
    };
    setParams(p);
    queryParams(p);
  };

  const onValuesChange = (_, values) => {
    const times =
      values.times?.map((item) => {
        return moment(item).format('YYYY/MM/DD');
      }) || [];
    const newParams = {
      ...params,
      ems_instance_openindustryid: values.ems_instance_openindustryid,
      ems_instance_opensubjectid: values.ems_instance_opensubjectid,
    };
    // 行政区划
    if (values.adCodes && values.adCodes.length > 0) {
      const adCode = values.adCodes[values.adCodes.length - 1];
      newParams.ems_instance_adcode = adCode;
    }
    setParams(newParams);
    queryParams(newParams);
  };

  const formItemLayout = {
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  };

  const queryParams = (params) => {
    if (viewType === 'card') {
      stationCardRef.current.refresh(params);
    } else {
      stationListRef.current.refresh(params);
    }
  };

  const changeViewType = (value: string | number) => {
    setViewType(value);
  };

  const handleSwitchType = (value) => {
    const newParams = {
      ...params,
      ems_instance_isdelete: 0,
    };
    if (value === 0 || value === 1) {
      newParams.ems_instance_isdelete = 0;
    } else {
      newParams.ems_instance_isdelete = 1;
    }
    setParams(newParams);
    queryParams(newParams);
  };

  const { data: options } = useRequest(
    async () => {
      return fetch('/json/pca-code.json')
        .then((response) => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        })
        .then((data) => {
          return data;
        });
    },
    {
      formatResult: (res: any) => {
        return res;
      },
    },
  );

  const { data: openIndustryData } = useRequest(
    () => {
      return findOpenindustryByMap({});
    },
    {
      formatResult: (res: any) => {
        return res?.result;
      },
    },
  );

  const { data: openThemeData } = useRequest(
    () => {
      return findOpensubjectByMap({});
    },
    {
      formatResult: (res: any) => {
        return res?.result;
      },
    },
  );

  const { data: openFacilitiesData } = useRequest(
    () => {
      return findOpenfacilityByMap({});
    },
    {
      formatResult: (res: any) => {
        return res?.result || [];
      },
    },
  );

  return (
    <PageContainer
      className={styles.mobliePageContainer}
      header={{
        avatar: {
          icon: <IconFont type="icon-zhanting" />,
          size: 36,
          style: { background: '#1890ff' },
        },
        subTitle: '环保设施向公众线上开放',
      }}
      extra={
        false && (
          <Button
            onClick={() => {
              setMore(!more);
            }}
          >
            更多
            {more ? (
              <UpOutlined style={{ color: 'gray' }} />
            ) : (
              <DownOutlined style={{ color: 'gray' }} />
            )}
          </Button>
        )
      }
      content={
        <div>
          <Alert
            message="业务场景：使用环保设施向公众开放沉浸式展厅管理系统依据开放行业、开放设施类型、开放主题、
            开放时段、开放地区等要求进行编辑、发布形成的一个可管理、可访问、可交互的线上VR全景IT信息系统。"
            type="info"
            showIcon
          />

          {more && (
            <Row gutter={24}>
              <Col {...topColResponsiveProps}>
                <Card hoverable>
                  <Meta title="一" description="This is the description" />
                </Card>
              </Col>
              <Col {...topColResponsiveProps}>
                <Card hoverable>
                  <Meta title="二" description="This is the description" />
                </Card>
              </Col>
              <Col {...topColResponsiveProps}>
                <Card hoverable>
                  <Meta title="三" description="This is the description" />
                </Card>
              </Col>
              <Col {...topColResponsiveProps}>
                <Card hoverable>
                  <Meta title="四" description="This is the description" />
                </Card>
              </Col>
            </Row>
          )}
          <div className={styles.pageContentInput}>
            <Space>
              <Input.Search
                placeholder="请输入监测站名称或编号搜索"
                enterButton="搜索"
                size="large"
                onSearch={handleFormSubmit}
                style={{ maxWidth: 722, minWidth: isMobile ? '100%' : '600px', width: '100%' }}
              />
              {isMobile ? (
                <Popover
                  overlayStyle={{ padding: '0 5px' }}
                  placement="bottomRight"
                  title={false}
                  content={
                    <Form
                      ref={formRef}
                      onValuesChange={(_, values) => {
                        onValuesChange(_, values);
                      }}
                      layout="vertical"
                      className={styles.mobileForm}
                    >
                      <Form.Item name="ems_instance_openindustryid" label="开放行业">
                        <div className={styles.checkableTag}>
                          {openIndustryData?.map((item) => (
                            <CheckableTag
                              key={item.ems_openindustry_id}
                              checked={item.ems_openindustry_id === selectedIndustryId}
                              onChange={() => {
                                let id = null;
                                if (item.ems_openindustry_id !== selectedIndustryId) {
                                  id = item.ems_openindustry_id;
                                }
                                setSelectedIndustryId(id);
                                const newParams = {
                                  ...params,
                                  ems_instance_openindustryid: id,
                                };
                                setParams(newParams);
                                queryParams(newParams);
                              }}
                            >
                              {item.ems_openindustry_name}
                            </CheckableTag>
                          ))}
                        </div>
                      </Form.Item>
                      <Form.Item name="ems_instance_openfacilityid" label="开放设施">
                        <div className={styles.checkableTag}>
                          {openFacilitiesData?.map((item) => (
                            <CheckableTag
                              key={item.ems_openfacility_id}
                              checked={item.ems_openfacility_id === selectedTag}
                              onChange={() => {
                                let id = null;
                                if (item.ems_openfacility_id !== selectedTag) {
                                  id = item.ems_openfacility_id;
                                }
                                setSelectedTag(id);
                                const newParams = {
                                  ...params,
                                  ems_instance_openfacilityid: id,
                                };
                                setParams(newParams);
                                queryParams(newParams);
                              }}
                            >
                              {item.ems_openfacility_name}
                            </CheckableTag>
                          ))}
                        </div>
                      </Form.Item>
                      <Form.Item name="ems_instance_opensubjectid" label="开放主题">
                        <div className={styles.checkableTag}>
                          {openThemeData?.map((item) => (
                            <CheckableTag
                              key={item.ems_opensubject_id}
                              checked={item.ems_opensubject_id === selectedThemeId}
                              onChange={() => {
                                let id = null;
                                if (item.ems_opensubject_id !== selectedThemeId) {
                                  id = item.ems_opensubject_id;
                                }
                                setSelectedThemeId(id);
                                const newParams = {
                                  ...params,
                                  ems_instance_opensubjectid: id,
                                };
                                setParams(newParams);
                                queryParams(newParams);
                              }}
                            >
                              {item.ems_opensubject_name}
                            </CheckableTag>
                          ))}
                        </div>
                      </Form.Item>
                      <Form.Item {...formItemLayout} name="adCodes" label="区划">
                        <Cascader
                          options={options}
                          fieldNames={{ label: 'name', value: 'code' }}
                          placeholder="请选择"
                        />
                      </Form.Item>
                      <Form.Item {...formItemLayout} name="times" label="开放时段">
                        <RangePicker status="warning" style={{ width: '100%' }} />
                      </Form.Item>
                    </Form>
                  }
                  trigger="click"
                  getPopupContainer={(triggerNode) => {
                    return triggerNode.parentNode || document.body;
                  }}
                >
                  <a
                    style={{ display: 'flex' }}
                    onClick={() => {
                      setShowSearchFiltere(!showSearchFilter);
                    }}
                  >
                    <span style={{ whiteSpace: 'nowrap' }}>筛选</span>
                    {showSearchFilter ? <UpOutlined /> : <DownOutlined />}
                  </a>
                </Popover>
              ) : (
                <a
                  style={{ display: 'flex' }}
                  onClick={() => {
                    setShowSearchFiltere(!showSearchFilter);
                  }}
                >
                  <span style={{ whiteSpace: 'nowrap' }}>高级筛选</span>
                  {showSearchFilter ? <UpOutlined /> : <DownOutlined />}
                </a>
              )}
            </Space>
          </div>
        </div>
      }
    >
      {showSearchFilter && !isMobile && (
        <ProCard bordered={false} style={{ marginBottom: 12 }}>
          <Form
            className={styles.webForm}
            ref={formRef}
            onValuesChange={(_, values) => {
              onValuesChange(_, values);
            }}
            layout={'inline'}
          >
            <StandardFormRow title="开放行业" block style={{ paddingBottom: 4 }}>
              <Form.Item name="ems_instance_openindustryid">
                <CheckCard.Group size="small" style={{ width: '100%' }}>
                  {openIndustryData?.map((item) => {
                    return (
                      <CheckCard
                        key={item.ems_openindustry_id}
                        avatar={
                          <IconFont type={item.ems_openindustry_icon} className={styles.icon2} />
                        }
                        title={
                          <Tooltip title={item.ems_openindustry_des}>
                            <Text
                              style={{ fontSize: 12, width: '100%' }}
                              ellipsis={item.ems_openindustry_name}
                            >
                              {item.ems_openindustry_name}
                            </Text>
                          </Tooltip>
                        }
                        value={item.ems_openindustry_id}
                      />
                    );
                  })}
                </CheckCard.Group>
              </Form.Item>
            </StandardFormRow>
            <StandardFormRow title="开放设施" block style={{ paddingBottom: 4 }}>
              <Form.Item name="ems_instance_openfacilityid">
                <div className={styles.checkableTag}>
                  {openFacilitiesData?.map((item) => (
                    <CheckableTag
                      key={item.ems_openfacility_id}
                      checked={item.ems_openfacility_id === selectedTag}
                      onChange={() => {
                        let id = null;
                        if (item.ems_openfacility_id !== selectedTag) {
                          id = item.ems_openfacility_id;
                        }
                        setSelectedTag(id);
                        const newParams = {
                          ...params,
                          ems_instance_openfacilityid: id,
                        };
                        setParams(newParams);
                        queryParams(newParams);
                      }}
                    >
                      <Space>
                        <IconFont type={item.ems_openfacility_icon} className={styles.icon2} />
                        <span>{item.ems_openfacility_name}</span>
                      </Space>
                    </CheckableTag>
                  ))}
                </div>
              </Form.Item>
            </StandardFormRow>
            <StandardFormRow title="开放主题" block style={{ paddingBottom: 4 }}>
              <Form.Item name="ems_instance_opensubjectid">
                <div className={styles.checkableTag}>
                  {openThemeData?.map((item) => (
                    <CheckableTag
                      key={item.ems_opensubject_id}
                      checked={item.ems_opensubject_id === selectedThemeId}
                      onChange={() => {
                        let id = null;
                        if (item.ems_opensubject_id !== selectedThemeId) {
                          id = item.ems_opensubject_id;
                        }
                        setSelectedThemeId(id);
                        const newParams = {
                          ...params,
                          ems_instance_opensubjectid: id,
                        };
                        setParams(newParams);
                        queryParams(newParams);
                      }}
                    >
                      {item.ems_opensubject_name}
                    </CheckableTag>
                  ))}
                </div>
              </Form.Item>
            </StandardFormRow>
            <StandardFormRow title="其它选项" grid last>
              <Row gutter={16}>
                <Col lg={6} md={10} sm={10} xs={24}>
                  <Form.Item
                    style={{ marginLeft: 8 }}
                    {...formItemLayout}
                    name="adCodes"
                    label="区划"
                    className={styles.smallLabel}
                  >
                    <Cascader
                      changeOnSelect
                      options={options}
                      fieldNames={{ label: 'name', value: 'code' }}
                      placeholder="请选择"
                      style={{ fontSize: 12 }}
                      className={styles.smallCascader}
                      getPopupContainer={(triggerNode) => {
                        return triggerNode.parentNode || document.body;
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col lg={6} md={10} sm={10} xs={24}>
                  <Form.Item
                    {...formItemLayout}
                    name="times"
                    label="开放时段"
                    className={styles.smallLabel}
                  >
                    <RangePicker
                      status="warning"
                      style={{ fontSize: 12 }}
                      className={styles.smallRange}
                      getPopupContainer={(triggerNode) => {
                        return triggerNode.parentNode || document.body;
                      }}
                    />
                  </Form.Item>
                </Col>
              </Row>
            </StandardFormRow>
          </Form>
        </ProCard>
      )}

      <ProCard bordered={false}>
        <div style={{ display: 'flex', width: '100%', marginBottom: '10px' }}>
          <Segmented
            onChange={changeViewType}
            value={viewType}
            options={[
              {
                label: '图文',
                value: 'card',
                icon: <AppstoreOutlined />,
              },
              {
                label: '列表',
                value: 'list',
                icon: <BarsOutlined />,
              },
            ]}
          />

          {!isMobile && (
            <Space style={{ marginLeft: 'auto' }}>
              <Segmented
                onChange={handleSwitchType}
                options={[
                  {
                    label: '已发布',
                    value: 0,
                    icon: <AppstoreOutlined />,
                  },
                  {
                    label: '在建',
                    value: 1,
                    icon: <BarsOutlined />,
                  },
                  {
                    label: '删除',
                    value: 2,
                    icon: <BarsOutlined />,
                  },
                ]}
              />
              <Button
                type="primary"
                onClick={() => {
                  if (viewType === 'card') {
                    stationCardRef.current.addStation({});
                  } else {
                    stationListRef.current.addStation({});
                  }
                }}
              >
                添加
              </Button>
            </Space>
          )}
        </div>
        {viewType === 'card' && <StationCard ref={stationCardRef} />}
        {viewType === 'list' && <StationList ref={stationListRef} />}
      </ProCard>
    </PageContainer>
  );
};
export default TableList;
