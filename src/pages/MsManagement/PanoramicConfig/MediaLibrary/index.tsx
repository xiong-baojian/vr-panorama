import { Space, Drawer, Tag, Button, Segmented, Table, Image } from 'antd';
import React, { useImperativeHandle, useState, useRef } from 'react';
import { DrawerForm, ProFormDatePicker, ProTable, LightFilter } from '@ant-design/pro-components';
import { FileImageOutlined, FileJpgOutlined, VideoCameraOutlined } from '@ant-design/icons';
import dayjs from 'dayjs';
import styles from './index.less';

const MediaLibrary = React.forwardRef((props: any, ref: any) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [selectedRowKeys, setSelectedRowKeys] = useState<any>([]);
  const rowSelection = {
    selectedRowKeys,
    onChange: (keys: any[]) => setSelectedRowKeys(keys),
  };
  const [submmitLoading, setSubmmitLoading] = useState(false);

  const tableListDataSource: any[] = [];

  for (let i = 0; i < 5; i += 1) {
    tableListDataSource.push({
      key: i,
      name: i,
      createdAt: dayjs('2019-11-16 12:50:26').valueOf() - Math.floor(Math.random() * 2000),
    });
  }

  const columns = [
    {
      title: '根目录',
      dataIndex: 'name',
      render: (_) => (
        <Space>
          <Image
            width={40}
            src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
          ></Image>
          <span>{_}</span>
        </Space>
      ),
    },
    {
      title: '时间',
      dataIndex: 'createdAt',
      valueType: 'dateTime',
      align: 'right',
    },
    // {
    //   title: '操作',
    //   key: 'option',
    //   width: 120,
    //   valueType: 'option',
    //   render: () => [<a key="link">链路</a>],
    // },
  ];

  const handleOk = async () => {
    setSubmmitLoading(false);
    setIsModalOpen(false);
    props.refreshList();
    return true;
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  useImperativeHandle(ref, () => ({
    async show(result: any) {
      setIsModalOpen(true);
    },
  }));
  return (
    <>
      <DrawerForm
        width={600}
        title={
          <Space>
            <span>素材库 - 空间容量 </span>
            <Tag>30MB / 1GB</Tag>
          </Space>
        }
        open={isModalOpen}
        loading={submmitLoading}
        onOpenChange={setIsModalOpen}
        onFinish={handleOk}
        // okText="提交"
        // cancelText="取消"
      >
        <Segmented
          block
          options={[
            {
              label: '全景图片',
              value: 0,
              icon: <FileImageOutlined />,
            },
            {
              label: '全景视频',
              value: 1,
              icon: <VideoCameraOutlined />,
            },
            {
              label: '超高清图片',
              value: 2,
              icon: <FileJpgOutlined />,
            },
          ]}
        />
        <ProTable
          className={styles.mediaLibraryTable}
          rowSelection={{
            // 自定义选择项参考: https://ant.design/components/table-cn/#components-table-demo-row-selection-custom
            // 注释该行则默认不显示下拉选项
            selections: [Table.SELECTION_ALL, Table.SELECTION_INVERT],
            // defaultSelectedRowKeys: [1],
          }}
          options={false}
          columns={columns}
          request={(params, sorter, filter) => {
            // 表单搜索项会从 params 传入，传递给后端接口。
            console.log(params, sorter, filter);
            return Promise.resolve({
              data: tableListDataSource,
              success: true,
            });
          }}
          toolbar={{
            search: {
              onSearch: (value: string) => {
                alert(value);
              },
            },
            actions: [
              <Button
                key="primary"
                type="primary"
                onClick={() => {
                  alert('add');
                }}
              >
                添加
              </Button>,
            ],
          }}
          rowKey="key"
          search={false}
        />
      </DrawerForm>
    </>
  );
});

export default MediaLibrary;
