import { Upload, message } from 'antd';
import React, { useImperativeHandle, useState, useRef, useEffect } from 'react';
import { useParams } from 'umi';
import { DrawerForm, ProFormText } from '@ant-design/pro-components';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import {
  insertAerialview,
  updateAerialview,
  uploadAerialviewFile,
} from '@/services/swagger/aerialviewController';
import { isEmpty } from 'lodash';
const { Dragger } = Upload;

const EditAerialView = React.forwardRef((props: any, ref: any) => {
  const formRef = useRef<any>();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [result, setResult] = useState<any>({});
  const [fileList, setFileList] = useState<any>([]);
  const [loading, setLoading] = useState(false);
  const [submmitLoading, setSubmmitLoading] = useState(false);
  const params: any = useParams();
  const { instanceId } = params;

  const onPanoramaChange = (info: any) => {
    setFileList(info.fileList);
  };

  const handleOk = async () => {
    await formRef.current?.validateFields();
    const form = formRef?.current?.getFieldsValue();
    if (!(fileList && fileList.length > 0)) {
      message.info('请上传鸟瞰图！');
      return;
    }
    setSubmmitLoading(true);
    let params = {
      ems_aerialview_name: form.ems_aerialview_name,
      ems_aerialview_instanceid: instanceId,
      ems_aerialview_fileid: fileList[0].uid,
      ems_aerialview_type: 1, // 平面图
    };
    let res: any;
    if (result && result.id) {
      params.ems_aerialview_id = result.id;
      res = await updateAerialview(params);
    } else {
      res = await insertAerialview(params);
    }

    if (res.code == 200) {
      message.success(res.message);
    }
    setSubmmitLoading(false);
    setIsModalOpen(false);
    props.refreshList();
    return true;
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  useEffect(() => {
    if (isModalOpen) {
      formRef.current?.setFieldValue('ems_aerialview_name', result.name);
    } else {
      formRef.current?.setFieldValue('ems_aerialview_name', {});
    }
  }, [isModalOpen]);

  useImperativeHandle(ref, () => ({
    async show(result: any) {
      setIsModalOpen(true);
      setFileList([]);
      if (result && !isEmpty(result)) {
        const file = {
          uid: result.file.ems_sysfile_id,
          name: result.file.ems_sysfile_name,
          status: 'done',
          url: `/systemfile${result.file.ems_sysfile_path}`,
          thumbUrl: `/systemfile${result.file.ems_sysfile_path}`,
        };
        setResult(result);
        setFileList([file]);
      } else {
        setResult({});
      }
    },
  }));
  return (
    <>
      <DrawerForm
        formRef={formRef}
        width={600}
        title={`${result.id ? '编辑' : '添加'}鸟瞰图图片`}
        open={isModalOpen}
        loading={submmitLoading}
        onOpenChange={setIsModalOpen}
        onFinish={handleOk}
        layout="horizontal"
      >
        <ProFormText
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 20 }}
          name="ems_aerialview_name"
          label="鸟瞰图名称"
          placeholder="请输入名称"
          rules={[{ required: true, message: '请输入名称' }]}
        />
        <Dragger
          height={250}
          multiple={false}
          listType="picture"
          onChange={onPanoramaChange}
          fileList={fileList}
          beforeUpload={(file) => {
            const isPNG = file.type === 'image/png' || file.type === 'image/jpeg';
            if (!isPNG) {
              message.error(`请上传图片形式文件！`);
            }
            return isPNG || Upload.LIST_IGNORE;
          }}
          customRequest={async (options) => {
            setLoading(true);
            const { onSuccess, onError, file }: any = options;
            const { result, code }: any = await uploadAerialviewFile({ file: file });
            if (code == 200) {
              const res = {
                uid: result.ems_sysfile_id,
                name: result.ems_sysfile_name,
                status: 'done',
                url: `/systemfile${result.ems_sysfile_path}`,
                thumbUrl: `/systemfile${result.ems_sysfile_path}`,
              };
              setFileList([res]);
              onSuccess('上传成功');
            } else {
              message.success('上传失败');
              onError('上传失败');
            }
            setLoading(false);
          }}
        >
          <p className="ant-upload-drag-icon">{loading ? <LoadingOutlined /> : <PlusOutlined />}</p>
          <p className="ant-upload-text">
            {loading ? '正在上传中...' : '单击或拖动文件到此区域进行上传'}
          </p>
          <p className="ant-upload-hint"></p>
        </Dragger>
      </DrawerForm>
    </>
  );
});

export default EditAerialView;
