import React, { useState, useImperativeHandle, useEffect } from 'react';
import { Popconfirm, Space, message, Typography, List, Avatar, Collapse } from 'antd';
import {
  createFromIconfontCN,
  DoubleLeftOutlined,
  CaretRightOutlined,
  FolderOpenOutlined,
  DeleteOutlined,
  FolderOutlined,
  FilterOutlined,
} from '@ant-design/icons';
import { deleteLayout } from '@/services/swagger/layoutController';
import { useParams } from 'umi';
const IconFont = createFromIconfontCN({
  scriptUrl: '/iconfont/iconfont.js',
});
import styles from './index.less';

const { Panel } = Collapse;
const { Text } = Typography;

const MarkerList = React.forwardRef((props: any, ref: any) => {
  const { changeMark, refreshData } = props;
  const [isActive, setIsActive] = useState<boolean>(true);

  const [dataSource, setDataSource] = useState<any[]>([]); // 标记管理list
  const [currentMark, setCurrentMark] = useState<any>({});

  const params: any = useParams();
  const { instanceId } = params;

  /**
   * 标记管理展开
   * @param keys
   */
  const onChange = (keys: string | string[]) => {
    if (keys.includes('1')) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  };

  /**
   * 选中单个全景标记
   * @param item
   */
  const handleSelect = (item) => {
    let mark = item;
    if (item.id === currentMark.id) {
      mark = {};
    }
    setCurrentMark(mark);
    changeMark(mark);
  };

  const handleDelete = async (item) => {
    const params = {
      ems_layout_id: item.id,
    };
    const res = await deleteLayout(params);
    if (res.code === 200) {
      message.success(res.message);
    }
    refreshData();
    changeMark({});
  };

  useEffect(() => {}, []);

  useImperativeHandle(ref, () => ({
    // 列表数据显示
    async setViewData(viewData: any) {
      if (viewData.markList && viewData.markList.length > 0) {
        setDataSource(viewData.markList);
      } else {
        setDataSource([]);
      }
    },
    async setMarkData(mark) {
      setCurrentMark(mark);
    },
  }));

  return (
    <div className={styles.markerList}>
      <Collapse
        defaultActiveKey={['1']}
        expandIcon={({ isActive }) => {
          return <CaretRightOutlined rotate={isActive ? 90 : 0} />;
        }}
        ghost
        onChange={onChange}
        className={styles.markerListCollapse}
      >
        <Panel
          header={
            <Space>
              {isActive ? <FolderOpenOutlined /> : <FolderOutlined />}
              <span className="title">标记管理</span>
            </Space>
          }
          key="1"
        >
          <List
            size="small"
            split={false}
            dataSource={dataSource}
            renderItem={(item: any) => (
              <List.Item
                className={`${currentMark?.id === item.id ? 'seletedLabel' : ''}`}
                onClick={() => {
                  handleSelect(item);
                }}
              >
                <List.Item.Meta
                  title={
                    <Space style={{ width: '100%' }}>
                      <IconFont type="icon-quanjingxiangji" />
                      <Text style={{ width: '120px' }} ellipsis>
                        {item.name}
                      </Text>
                      <Popconfirm
                        title="是否删除该项！"
                        okText="是"
                        cancelText="否"
                        onConfirm={(event) => {
                          event.stopPropagation();
                          handleDelete(item);
                        }}
                        onCancel={(event) => {
                          event.stopPropagation();
                        }}
                      >
                        <DeleteOutlined
                          style={{ color: 'red' }}
                          onClick={(event) => {
                            event.stopPropagation();
                          }}
                        />
                      </Popconfirm>
                    </Space>
                  }
                ></List.Item.Meta>
              </List.Item>
            )}
          />
        </Panel>
      </Collapse>
    </div>
  );
});
export default MarkerList;
