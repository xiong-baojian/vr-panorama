import React, { useState, useEffect, useRef } from 'react';
import { Typography, Tooltip, Divider, Space, message } from 'antd';
import { createFromIconfontCN } from '@ant-design/icons';
import ProCard from '@ant-design/pro-card';
import { useModel, useParams } from 'umi';
import { findLayoutByMap } from '@/services/swagger/layoutController';
import AerialViewTypeMananger from './AerialViewTypeMananger';
import MarkerList from './MarkerList';
import CenterMap from './CenterMap';
import RightOptions from './RightOptions';
import Compass from './Compass';
import { isEmpty } from 'lodash';

const IconFont = createFromIconfontCN({
  scriptUrl: ['/iconfont/iconfont.js'],
});

const { Paragraph } = Typography;

const AerialViewConfig: React.FC = (props: any) => {
  const params: any = useParams();
  const mapRef = useRef<any>(null);
  const optionsRef = useRef<any>(null);
  const markerListRef = useRef<any>(null);
  const [viewData, setViewData] = useState<any>({});
  const { instanceData } = props;
  const { instanceId } = params;

  const changeView = async (view: any) => {
    let viewData = view;
    setViewData(viewData);
    changeMark({});
    if (view && !isEmpty(view)) {
      findLayoutByMap({
        ems_layout_aerialviewid: view.id,
      }).then((res) => {
        const list = res.result?.list || [];
        viewData = {
          ...view,
          markList: list?.map((item) => {
            const data = JSON.parse(item.ems_layout_data || '{}') || {};
            const name = data.title || '';
            return {
              id: item.ems_layout_id,
              name: name,
              panoramaName: item.ems_layout_panoramaname,
              panoramaId: item.ems_layout_panoramaid,
              data: data,
            };
          }),
        };
        mapRef.current.setMapData(viewData);
        optionsRef.current.setOptionsData(viewData);
        markerListRef.current.setViewData(viewData);
      });
    } else {
      mapRef.current.setMapData(viewData);
      optionsRef.current.setOptionsData(viewData);
      markerListRef.current.setViewData(viewData);
    }
  };

  const refreshData = async () => {
    changeView(viewData);
  };

  /**
   * 选中标记
   * @param mark
   */
  const changeMark = (mark: any) => {
    optionsRef.current.setMarkData(mark);
    mapRef.current.setMarkData(mark);
    markerListRef.current.setMarkData(mark);
  };

  const changeMarkCoordinate = (id, coordinate) => {
    optionsRef.current.setMarkCoordData(coordinate);
    // setViewData()
  };

  return (
    <>
      <ProCard.Group title={false} extra={false} split="vertical" bodyStyle={{ padding: 0 }}>
        {/* 左侧模块 */}
        <ProCard colSpan="15%" bodyStyle={{ padding: '0 12px' }}>
          <Space direction="vertical" style={{ width: '100%', height: '640px' }}>
            <div style={{ height: '320px' }}>
              <AerialViewTypeMananger changeView={changeView} />
            </div>
            <Divider style={{ margin: 0 }} />
            <div>
              <MarkerList ref={markerListRef} changeMark={changeMark} refreshData={refreshData} />
            </div>
          </Space>
        </ProCard>
        {/* 中间模块 */}
        <ProCard colSpan="70%" title={false} bodyStyle={{ padding: '0 10px' }}>
          <div style={{ width: '100%', height: '640px', position: 'relative' }}>
            <CenterMap
              ref={mapRef}
              changeMark={changeMark}
              changeMarkCoordinate={changeMarkCoordinate}
            />
            <Compass />
          </div>
        </ProCard>
        {/* 右侧模块 */}
        <ProCard colSpan="15%" title={false}>
          <RightOptions ref={optionsRef} refreshData={refreshData} />
        </ProCard>
      </ProCard.Group>
    </>
  );
};
export default AerialViewConfig;
