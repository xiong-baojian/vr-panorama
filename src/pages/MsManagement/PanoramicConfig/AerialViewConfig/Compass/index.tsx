import { useEffect, useRef } from 'react';
import { useModel, useParams } from 'umi';
import { Camera } from '@/utils/three/xThree';
import { createFromIconfontCN } from '@ant-design/icons';
import styles from './index.less';
import CompassConfig from './CompassConfig';
const IconFont = createFromIconfontCN({
  scriptUrl: '/iconfont/iconfont.js',
});

const Compass = () => {
  const compassConfigRef = useRef<any>(null);
  const params: any = useParams();
  const { instanceId } = params;

  const { panoramaData, setToPanoramaId } = useModel('Panorama.data', (ret) => ({
    panoramaData: ret.panoramaData,
    setToPanoramaId: ret.setToPanoramaId,
  }));

  const resetView = () => {
    compassConfigRef?.current?.show();
  };

  useEffect(() => {
    if (window.viewer) {
      // Camera.getCameraTheta(window.viewer, (theta) => {
      //   if (theta) {
      //     const compass: any = document.getElementById('compass');
      //     if (compass) {
      //       compass.style.transform = 'rotateZ(' + theta + 'deg)';
      //     }
      //   }
      // });
    }
  }, [window.viewer]);

  return (
    <>
      <div className={styles.compassBox} onClick={resetView}>
        <div className={styles.compass} id="compass"></div>
      </div>
      <CompassConfig ref={compassConfigRef} />
    </>
  );
};

export default Compass;
