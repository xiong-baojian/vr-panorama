import { Space, message, Col, InputNumber, Row, Slider } from 'antd';
import React, { useImperativeHandle, useState, useRef } from 'react';
import { DrawerForm } from '@ant-design/pro-components';
import styles from './index.less';

const CompassConfig = React.forwardRef((props: any, ref: any) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const [submmitLoading, setSubmmitLoading] = useState(false);

  const [inputValue, setInputValue] = useState(0);

  const onChange = (value: number) => {
    if (isNaN(value)) {
      return;
    }

    setInputValue(value);
    const compass: any = document.getElementById('compassConfig');
    if (compass) {
      compass.style.transform = 'rotateZ(' + value + 'deg)';
    }
  };

  const handleOk = async () => {
    props.refreshList();
    return true;
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  useImperativeHandle(ref, () => ({
    async show(result: any) {
      setIsModalOpen(true);
    },
  }));
  return (
    <>
      <DrawerForm
        width={600}
        title="指北针设置"
        open={isModalOpen}
        loading={submmitLoading}
        onOpenChange={setIsModalOpen}
        onFinish={handleOk}
      >
        <Space style={{ width: '100%' }} direction="vertical">
          <div className={styles.compassBox} onClick={() => {}}>
            <div className={styles.compass} id="compassConfig"></div>
          </div>
          <Row style={{ width: '90%', margin: '20px auto' }}>
            <Col span={20}>
              <Slider
                min={0}
                max={360}
                onChange={onChange}
                value={typeof inputValue === 'number' ? inputValue : 0}
                step={1}
              />
            </Col>
            <Col span={4}>
              <InputNumber
                min={0}
                max={360}
                style={{ margin: '0 16px' }}
                step={1}
                value={inputValue}
                onChange={onChange}
              />
            </Col>
          </Row>
        </Space>
      </DrawerForm>
    </>
  );
});

export default CompassConfig;
