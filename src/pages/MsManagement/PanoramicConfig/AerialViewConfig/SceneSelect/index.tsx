import { DrawerForm, ProFormItem, CheckCard, ProFormTreeSelect } from '@ant-design/pro-components';
import { useParams } from 'umi';
import { findGroupByInstance } from '@/services/swagger/InstancePanoramaGroupController';
import { findPanoramaByMapV1 } from '@/services/swagger/panoramaController';
import { findLayoutByMap, batchInsertLayout } from '@/services/swagger/layoutController';
import { isNil } from 'lodash';
import { getMapCenter } from '@/utils/openlayers';
import React, { useState, useImperativeHandle, useRef, useEffect } from 'react';
import { Typography, message } from 'antd';
import styles from './index.less';

const { Text } = Typography;
const SceneSelect = React.forwardRef((props: any, ref: any) => {
  const params: any = useParams();
  const { instanceId } = params;
  const [layoutId, setLayoutId] = useState<any>(null);
  const [selectedItems, setSelectedItems] = useState<any>([]);
  const [groupId, setGroupId] = useState<any>(null);
  const [panoramaList, setPanoramaList] = useState<any>(null);
  const [selectedList, setSelectedList] = useState<any>([]);
  const [visible, setVisible] = useState(false);
  const formRef = useRef<any>();

  /**
   * 完成添加多个标记点
   * @param values
   * @returns
   */
  const handleFinish = async (values) => {
    const { sceneList } = values;
    const params = sceneList?.map((item) => {
      const name =
        panoramaList?.find((jtem) => jtem.ems_panorama_id === item)?.ems_panorama_name || '';

      const coordinate = getMapCenter();
      const jsonData = {
        title: name,
        titleShow: true,
        coordinate: coordinate,
      };
      return {
        ems_layout_aerialviewid: layoutId,
        ems_layout_data: JSON.stringify(jsonData),
        ems_layout_isview: 1, // 是否显示标题
        ems_layout_panoramaid: item,
      };
    });
    const res = await batchInsertLayout(params);
    if (res.code == 200) {
      message.success(res.message);
    }
    props.refresh();
    return true;
  };

  /**
   * 全景列表
   */
  useEffect(() => {
    if (!isNil(groupId)) {
      findPanoramaByMapV1({
        ems_panorama_groupid: groupId,
        ems_panorama_instanceid: instanceId,
      }).then((res) => {
        const list = res?.result?.list || [];
        setPanoramaList(list);
      });
    }
  }, [groupId]);

  useImperativeHandle(ref, () => ({
    async show(layoutId) {
      setVisible(true);
      findLayoutByMap({
        ems_layout_aerialviewid: layoutId,
      }).then((res) => {
        const list = res.result?.list || [];
        const items = list?.map((item) => {
          return item.ems_layout_panoramaid;
        });
        setSelectedItems(items);
      });
      setLayoutId(layoutId);
      setSelectedList([]);
    },
  }));

  return (
    <DrawerForm
      width={600}
      formRef={formRef}
      layout="horizontal"
      grid={true}
      rowProps={{
        gutter: [0, 8],
      }}
      labelCol={{ span: 4 }}
      wrapperCol={{ span: 20 }}
      onOpenChange={setVisible}
      title={'选择场景'}
      open={visible}
      onFinish={handleFinish}
    >
      <ProFormTreeSelect
        label="场景分组"
        name="ems_instance_panorama_group_id"
        placeholder="请选择"
        allowClear={false}
        secondary
        request={async () => {
          const res = await findGroupByInstance({
            ems_instance_panorama_group_instanceid: instanceId,
          });
          const list = res.result || [];
          formRef.current?.setFieldValue(
            'ems_instance_panorama_group_id',
            list[0]?.ems_instance_panorama_group_id,
          );
          setGroupId(list[0]?.ems_instance_panorama_group_id);
          return res.result || [];
        }}
        fieldProps={{
          treeIcon: true,
          treeLine: true,
          filterTreeNode: true,
          showSearch: true,
          autoClearSearchValue: true,
          treeNodeFilterProp: 'ems_instance_panorama_group_name',
          fieldNames: {
            label: 'ems_instance_panorama_group_name',
            value: 'ems_instance_panorama_group_id',
            children: 'childrens',
          },
          onChange(value, labelList, extra) {
            setGroupId(value);
          },
        }}
      />
      <ProFormItem
        style={{ width: '100%' }}
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 20 }}
        label="选择场景"
        name="sceneList"
        rules={[{ required: true, message: '场景为必选项!' }]}
      >
        <CheckCard.Group
          multiple
          value={selectedList}
          onChange={(e) => {
            setSelectedList(e);
          }}
        >
          {panoramaList?.map((item) => {
            return (
              <CheckCard
                disabled={selectedItems?.includes(item.ems_panorama_id)}
                key={item.ems_panorama_id}
                value={item.ems_panorama_id}
                size="small"
                className={styles.selectCard}
                style={{
                  width: 80,
                }}
                cover={
                  <div className={styles.selectedCard}>
                    <img
                      alt="example"
                      height={200}
                      src={`/systemfile${item.ems_panorama_cover.ems_sysfile_path}`}
                    />
                    <Text
                      style={{
                        position: 'absolute',
                        textAlign: 'center',
                        background: 'rgba(0, 0, 0, 0.3)',
                        left: 0,
                        bottom: 0,
                        width: '100%',
                        height: 25,
                        color: '#fff',
                        fontSize: '14px',
                      }}
                      color="#fff"
                      strong
                      ellipsis={{ tooltip: item.ems_panorama_name }}
                    >
                      {item.ems_panorama_name}
                    </Text>
                  </div>
                }
              />
            );
          })}
        </CheckCard.Group>
      </ProFormItem>
    </DrawerForm>
  );
});

export default SceneSelect;
