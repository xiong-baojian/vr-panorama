/** 右侧面板 */
import React, { useState, useEffect, useRef, useImperativeHandle } from 'react';
import { Viewer, Panorama } from '@/utils/three/xThree';
import { ProFormText, ProFormSwitch, ProForm, ProCard } from '@ant-design/pro-components';
import { message, Space, Button } from 'antd';
import { getPanoramaViewV1 } from '@/services/swagger/panoramaController';
import type { ProFormInstance } from '@ant-design/pro-components';
import { addMarker } from '@/utils/openlayers';
import { updateLayout } from '@/services/swagger/layoutController';
import SceneSelect from '../SceneSelect';
import { isEmpty, uniqueId } from 'lodash';
import styles from './index.less';

let panorama: any;
const RightOptions = React.forwardRef((props: any, ref: any) => {
  const { refreshData } = props;
  const [point, setPoint] = useState<any>({});
  const sceneSelectRef = useRef<any>(null);
  const formRef = useRef<ProFormInstance>();
  const [currentView, setCurrentView] = useState<any>({});

  const handleFinish = async (values) => {
    if (point && !isEmpty(point)) {
      const { data, id, panoramaId } = point;
      const { title, titleShow } = values;
      const jsonData = data;
      jsonData.title = title;
      jsonData.titleShow = titleShow;
      jsonData.coordinate = data.coordinate;
      const params = {
        ems_layout_id: id,
        ems_layout_panoramaid: panoramaId,
        ems_layout_data: JSON.stringify(jsonData),
      };
      const res = await updateLayout(params);
      if (res.code === 200) {
        message.success(res.message);
      }
      refresh();
    } else {
      message.info('请选择标记点！');
    }
  };

  const refresh = () => {
    refreshData();
  };

  const setCameraView = () => {};

  useEffect(() => {
    const viewer = new Viewer({
      domId: 'markPanorama',
    });
    viewer.viewHelper.visible = false;
    panorama = new Panorama(viewer);

    return () => {
      panorama?.dispose();
    };
  }, []);

  useImperativeHandle(ref, () => ({
    async setOptionsData(data: any) {
      setCurrentView(data);
    },
    async setMarkCoordData(coordinate) {
      const data = point.data;
      data.coordinate = coordinate;
      setPoint({
        ...point,
        data,
      });
    },
    async setMarkData(markData: any) {
      if (markData && !isEmpty(markData)) {
        const res = await getPanoramaViewV1({ ems_panorama_id: markData.panoramaId });
        const preview = res.result?.ems_panorama_previewfile;
        if (preview && preview.ems_sysfile_path) {
          const panoramaImgs = {
            preview: '/systemfile' + preview.ems_sysfile_path,
          };
          panorama.removeAll();
          await panorama.setPanoramaImg(panoramaImgs, null);
        }
        if (markData.data && !isEmpty(markData.data)) {
          formRef.current?.setFieldsValue(markData.data);
        } else {
          formRef.current?.resetFields();
        }
      } else {
        formRef.current?.resetFields();
      }
      setPoint(markData);
    },
  }));

  return (
    <div className={styles.rightOptions}>
      <Space direction="vertical" style={{ width: '100%' }}>
        <Button
          onClick={() => {
            if (currentView && !isEmpty(currentView)) {
              sceneSelectRef.current?.show(currentView.id);
            } else {
              message.info('添加标记点前请先选择鸟瞰图！');
            }
          }}
        >
          在图中添加标记点
        </Button>
        <div id="markPanorama" className={styles.markPanorama}>
          <div className={styles.name}>{point.panoramaName}</div>
          {/* <div className={styles.btn}>
            <Button onClick={setCameraView}>设置当前视角为正北方向</Button>
          </div> */}
        </div>
        <ProForm formRef={formRef} colon={false} onFinish={handleFinish}>
          <ProCard
            title="标题设置"
            ghost
            extra={
              <ProFormSwitch
                initialValue={false}
                name="titleShow"
                noStyle
                checkedChildren={'显示'}
                unCheckedChildren={'隐藏'}
              />
            }
          >
            <ProFormText
              name="title"
              label=""
              rules={[
                {
                  required: true,
                },
              ]}
            />
          </ProCard>
        </ProForm>
      </Space>

      <SceneSelect ref={sceneSelectRef} refresh={refresh} />
    </div>
  );
});
export default RightOptions;
