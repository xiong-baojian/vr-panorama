import React, { useEffect, useRef, useImperativeHandle, useState } from 'react';
import {
  initMap,
  setMapViewByType,
  addMarker,
  unloadMap,
  setMapLayerByType,
  findMapLayersByType,
  clearLayer,
  selectEventListener,
  setMarkerSelectedById,
  modifyendEventListener,
  clearMapLayer,
  addInteractions,
} from '@/utils/openlayers';
import 'ol/ol.css';
import { isEmpty } from 'lodash';
let markDataList: any = [];
const CenterMap = React.forwardRef((props: any, ref: any) => {
  const { changeMark, changeMarkCoordinate } = props;

  const [view, setView] = useState<any>({});

  useEffect(() => {
    initMap('olMap');
    addInteractions();
    selectEventListener((id) => {
      const mark = markDataList?.find((item) => item.id === id) || {};
      changeMark(mark);
    });
    modifyendEventListener((id, coordinate: []) => {
      changeMarkCoordinate(id, coordinate);
    });
    return () => {
      unloadMap();
    };
  }, []);

  useImperativeHandle(ref, () => ({
    async setMapData(viewData: any) {
      if (viewData && !isEmpty(viewData)) {
        markDataList = viewData.markList;
        const type = Number(viewData.type);
        const imgUrl = viewData.imgUrl;
        // 地图 or 图片
        if (viewData.id !== view.id) {
          if (type === 0) {
            setMapViewByType(0);
            setMapLayerByType(0);
          } else if (type === 1 && imgUrl) {
            setMapViewByType(1);
            await setMapLayerByType(1, imgUrl);
          }
        }
        // 清除图层标记
        const layer = findMapLayersByType('markerLayer')[0];
        clearLayer(layer);
        // 添加标记
        if (viewData.markList && viewData.markList.length > 0) {
          viewData.markList?.forEach((item) => {
            addMarker({
              id: item.id,
              name: item.name,
              coordinate: item.data.coordinate,
            });
          });
        }
      } else {
        const layerArr = findMapLayersByType('all');
        clearMapLayer(layerArr);
      }
      setView(viewData);
    },
    async setMarkData(markData: any) {
      if (markData && !isEmpty(markData)) {
        setMarkerSelectedById(markData.id);
      } else {
        setMarkerSelectedById(null);
      }
    },
  }));

  return (
    <>
      <div id="olMap" style={{ width: '100%', height: '100%' }}></div>
    </>
  );
});
export default CenterMap;
