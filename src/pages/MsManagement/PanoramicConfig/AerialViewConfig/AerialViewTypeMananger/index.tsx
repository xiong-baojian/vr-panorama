import React, { useState, useRef, useEffect } from 'react';
import { ProList, CheckCard, ProFormList } from '@ant-design/pro-components';
import { Tabs, message, Button, Typography, Popconfirm, Avatar } from 'antd';
import {
  createFromIconfontCN,
  PlusOutlined,
  EditOutlined,
  DeleteOutlined,
  StarOutlined,
  StarFilled,
} from '@ant-design/icons';
import {
  deleteAerialview,
  insertAerialview,
  updateAerialview,
  setDefault,
} from '@/services/swagger/aerialviewController';
import { findAerialviewByMap } from '@/services/swagger/aerialviewController';
import { isNil } from 'lodash';
import EditAerialView from '../EditAerialView';
import { useParams } from 'umi';

const IconFont = createFromIconfontCN({
  scriptUrl: '/iconfont/iconfont.js',
});

import styles from './index.less';

const { Text } = Typography;

const AerialViewTypeMananger = (props: any) => {
  const { changeView } = props;

  const editAerialViewRef = useRef<any>();
  const [view, setView] = useState<any>({});
  const [dataSource, setDataSource] = useState<any[]>([]);
  const [items, setItems] = useState<any>([
    {
      key: '0',
      label: (
        <span>
          <IconFont type="icon-dituleiditu" />
          地图
        </span>
      ),
    },
    {
      key: '1',
      label: (
        <span>
          <IconFont type="icon-pingmiantu" />
          平面图
        </span>
      ),
    },
    {
      key: '2',
      label: (
        <span>
          <IconFont type="icon-a-3Dmoxing2" />
          3D模型
        </span>
      ),
    },
  ]);
  const [activeKey, setActiveKey] = useState<string>('0');
  const params: any = useParams();
  const { instanceId } = params;

  const onChange = (activeKey: string) => {
    setActiveKey(activeKey);
  };

  const handleChangeId = (id) => {
    let currentView = {};
    if (!isNil(id) && id != view.id) {
      currentView = dataSource?.find((item) => item.id == id);
    }
    setView(currentView);
    changeView(currentView);
  };

  const handleAdd = async () => {
    switch (activeKey) {
      case '0':
        let params1 = {
          ems_aerialview_name: '默认地图设计',
          ems_aerialview_instanceid: instanceId,
          ems_aerialview_type: 0, // 地图
        };
        let res1 = await insertAerialview(params1);
        if (res1.code == 200) {
          message.success(res1.message);
        }
        refresh(activeKey);
        break;
      case '1':
        editAerialViewRef.current.show();
        break;
      case '2':
        let params2 = {
          ems_aerialview_name: '默认3D模型设计',
          ems_aerialview_instanceid: instanceId,
          ems_aerialview_type: 2, // 模型
        };
        let res2 = await insertAerialview(params2);
        if (res2.code == 200) {
          message.success(res2.message);
        }
        refresh(activeKey);
        break;
      default:
        break;
    }
  };
  const handleDelete = async (item) => {
    let res = await deleteAerialview({ ems_aerialview_id: item.id });
    if (res.code == 200) {
      message.success(res.message);
    } else {
      message.error(res.message);
    }
    refresh(activeKey);
  };

  const handleEdit = (e: Event, item) => {
    e.stopPropagation();
    editAerialViewRef.current.show(item);
  };

  const handleSetDefault = (e: Event, item) => {
    e.stopPropagation();
    setDefault({ ems_aerialview_id: item.id }).then((res) => {
      if (res.code === 200) {
        message.success('设置成功！');
      }
      refresh(activeKey);
    });
  };

  const refresh = (activeKey: string | number) => {
    findAerialviewByMap({
      ems_aerialview_type: Number(activeKey),
      ems_aerialview_instanceid: instanceId,
    }).then((res) => {
      const list = res.result?.list || [];
      const dataSourceArr = list?.map((item) => {
        const url = item.ems_aerialview_file?.ems_sysfile_path
          ? `/systemfile${item.ems_aerialview_file?.ems_sysfile_path}`
          : null;
        return {
          id: item.ems_aerialview_id,
          name: item.ems_aerialview_name,
          imgUrl: url,
          type: activeKey,
          file: item.ems_aerialview_file,
          isDefault: item.ems_aerialview_isdefault,
        };
      });
      setDataSource(dataSourceArr);
    });
  };

  useEffect(() => {
    refresh(activeKey);
  }, [activeKey]);

  return (
    <div className={styles.aerialViewTypeMananger}>
      <div style={{ width: '100%', display: 'flex' }}>
        <Tabs onChange={onChange} items={items} activeKey={activeKey} style={{ width: '100%' }} />
      </div>
      {(activeKey == '0' || activeKey == '2') && (
        <ProList
          rowKey="id"
          headerTitle={false}
          dataSource={dataSource}
          showActions="hover"
          editable={{
            onSave: async (key, record, originRow) => {
              let params = {
                ems_aerialview_id: record.id,
                ems_aerialview_name: record.name,
                ems_aerialview_instanceid: instanceId,
                ems_aerialview_type: Number(activeKey),
              };
              let res = await updateAerialview(params);
              if (res.code == 200) {
                message.success(res.message);
              }
              refresh(activeKey);
              return true;
            },
            onDelete: async (key, record) => {
              let params = {
                ems_aerialview_id: record.id,
              };
              let res = await deleteAerialview(params);
              if (res.code == 200) {
                message.success(res.message);
              }
              refresh(activeKey);
              changeView({});
              return true;
            },
          }}
          // rowSelection={{}}
          // grid={{ gutter: 16, column: 1 }}
          onItem={(record: any) => {
            return {
              onClick: () => {
                handleChangeId(record.id);
              },
            };
          }}
          rowClassName={(row) => {
            return row.id === view.id ? styles.selectedItem : '';
          }}
          onDataSourceChange={setDataSource}
          metas={{
            title: {
              dataIndex: 'name',
            },
            avatar: {
              editable: false,
              render: (index, row) => {
                return (
                  <Avatar
                    onClick={(e) => {
                      handleSetDefault(e, row);
                    }}
                    size="small"
                    src={
                      row.isDefault === 1 ? (
                        <StarFilled style={{ color: '#1890ff', fontSize: 18 }} />
                      ) : (
                        <StarOutlined style={{ color: '#1890ff', fontSize: 18 }} />
                      )
                    }
                  />
                );
              },
            },
            actions: {
              render: (text, row, index, action) => [
                <a
                  onClick={() => {
                    action?.startEditable(row.id);
                  }}
                  key="edit"
                >
                  <EditOutlined />
                </a>,
              ],
            },
          }}
        />
      )}
      {activeKey == '1' && (
        <CheckCard.Group className={styles.CheckCardlist} onChange={handleChangeId}>
          {dataSource.map((item: any) => {
            return (
              <CheckCard
                key={item.id}
                style={{
                  background: 'rgba(33, 33, 33, 0.01)',
                  width: '100%',
                }}
                title={
                  <div style={{ width: '100%', display: 'flex', alignItems: 'center' }}>
                    <Avatar
                      onClick={(e) => {
                        handleSetDefault(e, item);
                      }}
                      style={{ width: '12%', marginRight: '3%' }}
                      size="small"
                      src={
                        item.isDefault === 1 ? (
                          <StarFilled style={{ color: '#1890ff', fontSize: 18 }} />
                        ) : (
                          <StarOutlined style={{ color: '#1890ff', fontSize: 18 }} />
                        )
                      }
                    />
                    <Text ellipsis style={{ width: '60%', display: 'inline-block' }}>
                      {item.name}
                    </Text>
                    <a style={{ display: 'inline-block', width: '13%', textAlign: 'right' }}>
                      <EditOutlined
                        onClick={(e) => {
                          handleEdit(e, item);
                        }}
                      />
                    </a>
                    <a style={{ display: 'inline-block', width: '12%', textAlign: 'right' }}>
                      <Popconfirm
                        title="是否删除该项！"
                        okText="是"
                        cancelText="否"
                        onConfirm={(event) => {
                          event.stopPropagation();
                          handleDelete(item);
                          changeView({});
                        }}
                        onCancel={(event) => {
                          event.stopPropagation();
                        }}
                      >
                        <DeleteOutlined
                          style={{ color: 'red' }}
                          onClick={(event) => {
                            event.stopPropagation();
                          }}
                        />
                      </Popconfirm>
                    </a>
                  </div>
                }
                value={item.id}
              />
            );
          })}
        </CheckCard.Group>
      )}

      <Button
        type="dashed"
        block
        icon={<PlusOutlined />}
        className={styles.addBtn}
        onClick={handleAdd}
      >
        添加鸟瞰图
      </Button>

      <EditAerialView
        ref={editAerialViewRef}
        typeId={activeKey}
        instanceId={instanceId}
        refreshList={() => {
          refresh(activeKey);
        }}
      />
    </div>
  );
};
export default AerialViewTypeMananger;
