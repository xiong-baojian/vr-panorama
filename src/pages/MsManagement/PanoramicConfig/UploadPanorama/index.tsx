import { Space, Select, Upload, message, Input, Button } from 'antd';
import React, { useImperativeHandle, useState, useRef } from 'react';
import { DrawerForm } from '@ant-design/pro-components';
import { LoadingOutlined, PlusOutlined, UploadOutlined } from '@ant-design/icons';
import { loadImage } from '@/utils/panoramaToCubmap.js';
import styles from './index.less';
import { batchUploadPanoramaFileV1 } from '@/services/swagger/panoramaController';
import { insertPanorama } from '@/services/swagger/panoramaController';
const { Dragger } = Upload;
const { Option } = Select;

const UploadPanorama = React.forwardRef((props: any, ref: any) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [fileList, setFileList] = useState<any>([]);
  const [fileListData, setFileListData] = useState<any>([]);
  const [watermark, setWaterMark] = useState('');
  const [waterType, setWaterType] = useState('text');
  const [loading, setLoading] = useState(false);
  const [submmitLoading, setSubmmitLoading] = useState(false);

  const onPanoramaChange = (info: any) => {
    setFileList(info.fileList);
  };

  /**
   * 上传前限制
   * @param file
   * @returns
   */
  const beforeUpload = (file: any) => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('只能上传 JPG/PNG 文件!');
    }
    return isJpgOrPng;

    // const isLt2M = file.size / 1024 / 1024 < 2;
    // if (!isLt2M) {
    //   message.error('Image must smaller than 2MB!');
    // }
    // return isJpgOrPng && isLt2M;
  };

  const handleOk = async () => {
    setSubmmitLoading(true);
    const coverFile = fileListData?.find((item) => item.ems_sysfile_type === 'cover');
    const formData = {
      ems_panorama_name: fileListData[0].ems_sysfile_name,
      ems_panorama_groupid: props.groupId,
      ems_panorama_instanceid: props.instanceId,
      ems_panorama_coverfile: coverFile,
      ems_panorama_slicefiles: fileListData,
    };
    await insertPanorama(formData);
    setSubmmitLoading(false);
    setIsModalOpen(false);
    props.refreshList();
    return true;
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  useImperativeHandle(ref, () => ({
    async show(result: any) {
      setIsModalOpen(true);
      setFileListData([]);
      setFileList([]);
    },
  }));
  return (
    <>
      {loading && <div className={styles.drawerMask}></div>}
      <DrawerForm
        width={600}
        title="全景上传"
        open={isModalOpen}
        loading={submmitLoading}
        onOpenChange={setIsModalOpen}
        onFinish={handleOk}
        // okText="提交"
        // cancelText="取消"
      >
        <Space style={{ marginBottom: '10px' }}>
          <span>水印处理：</span>
          <Space>
            <Select
              style={{ width: 100 }}
              value={waterType}
              onChange={(value) => {
                setWaterType(value);
              }}
            >
              <Option value="text">文字</Option>
              <Option value="img">图片</Option>
            </Select>
            {waterType === 'text' && (
              <Input
                placeholder="例如：武汉博感空间科技有限公司"
                style={{ width: 250 }}
                value={watermark}
                onChange={(e) => {
                  setWaterMark(e.target.value);
                }}
              />
            )}
            {waterType === 'img' && (
              <Upload beforeUpload={beforeUpload} maxCount={1} className={styles.uoloadBtn}>
                <Button icon={<UploadOutlined />}>点击上传水印图片</Button>
              </Upload>
            )}
          </Space>
        </Space>
        <Dragger
          height={250}
          maxCount={1}
          listType="picture"
          onChange={onPanoramaChange}
          fileList={fileList}
          beforeUpload={beforeUpload}
          customRequest={async (options) => {
            const { onSuccess, onError, file }: any = options;
            setLoading(true);
            loadImage(file, (list: any) => {
              let params = list;
              params.unshift(file);
              batchUploadPanoramaFileV1({}, params).then((res: any) => {
                setLoading(false);
                const { code, result } = res;
                if (code == 200) {
                  let panoramaList = result;
                  const newArr: any = [
                    {
                      uid: panoramaList[0]?.ems_sysfile_id,
                      name: panoramaList[0]?.ems_sysfile_name,
                      status: 'done',
                      url: `/systemfile${panoramaList[0]?.ems_sysfile_path}`,
                      thumbUrl: `/systemfile${panoramaList[0]?.ems_sysfile_path}`,
                    },
                  ];
                  setFileList(newArr);
                  setFileListData([...panoramaList]);
                  onSuccess('上传成功');
                } else {
                  message.success('上传失败');
                  onError('上传失败');
                }
              });
            });
          }}
        >
          <p className="ant-upload-drag-icon">{loading ? <LoadingOutlined /> : <PlusOutlined />}</p>
          <p className="ant-upload-text">
            {loading ? (
              <p>
                正在上传中...<p>已禁止其他操作，等待上传完成！</p>
              </p>
            ) : (
              '单击或拖动文件到此区域进行上传'
            )}
          </p>
          <p className="ant-upload-hint"></p>
        </Dragger>
        <div id="cubemap" style={{ display: 'none' }}>
          <b id="generating" style={{ visibility: 'hidden' }}>
            Generating...
          </b>
          <output id="faces"></output>
        </div>
      </DrawerForm>
    </>
  );
});

export default UploadPanorama;
