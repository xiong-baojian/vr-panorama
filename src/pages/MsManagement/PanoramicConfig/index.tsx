import {
  Tag,
  Alert,
  Card,
  Button,
  List,
  Table,
  Popconfirm,
  Image,
  Typography,
  Tooltip,
  Segmented,
  Space,
  message,
} from 'antd';
import { ProTable } from '@ant-design/pro-components';
import { findByViewangleMap } from '@/services/swagger/ViewangleController';
import { PageContainer } from '@ant-design/pro-layout';
import ProCard from '@ant-design/pro-card';
import {
  createFromIconfontCN,
  EditOutlined,
  DeleteOutlined,
  SettingOutlined,
  DownloadOutlined,
  CloudUploadOutlined,
  FolderOpenOutlined,
} from '@ant-design/icons';
import React, { useState, useEffect, useRef } from 'react';
import { history, useParams, useRequest } from 'umi';
import { findPanoramaByMapV1, deletePanorama } from '@/services/swagger/panoramaController';
import { findInstanceByMap } from '@/services/swagger/instanceController';
import SceneManager from './SceneManager';
import styles from './index.less';
import AddOrEditPanorama from './AddOrEditPanorama';
import UploadPanorama from './UploadPanorama';
import MediaLibrary from './MediaLibrary';
import ShareConfig from './ShareConfig';
import AerialViewConfig from './AerialViewConfig';

const { Paragraph, Title, Text } = Typography;
const IconFont = createFromIconfontCN({
  scriptUrl: ['/iconfont/iconfont.js'],
});
const PanoramicConfig: React.FC = (props) => {
  const params: any = useParams();
  const { instanceId } = params;

  const [viewId, setViewId] = useState<any>(null);
  const actionRef = useRef<any>();
  const panoramaRef = useRef<any>();
  const uploadRef = useRef<any>();
  const mediaLibraryRef = useRef<any>();
  const [groupId, setGroupId] = useState<any>(null);
  const [instanceData, setInstanceData] = useState<API.InstanceVo>({});
  const [selectedRowKeys, setSelectedRowKeys] = useState<any>([]);
  const [activeKey, setActiveKey] = useState<string | undefined>('1');

  const tabList = [
    {
      key: '1',
      tab: 'VR全景设计',
    },
    {
      key: '2',
      tab: '鸟瞰图设计',
    },
    {
      key: '3',
      tab: '发布管理',
    },
  ];

  const goToParanomicTool = (panorama: API.PanoramaViewVo) => {
    window.open(
      `/msManagement/panoramicConfig/${panorama.ems_panorama_instanceid}/${panorama.ems_panorama_id}`,
      '_blank',
    );
  };

  /**
   * 下载图片
   * @param record
   */
  const downloadImg = async (record) => {
    if (record?.ems_panorama_sourceimage) {
      const link = document.createElement('a');
      link.href = `/systemfile${record?.ems_panorama_sourceimage?.ems_sysfile_path}`;
      link.download = record.ems_panorama_name;
      link.click();
    } else {
      message.info('暂无原图下载！');
    }
  };

  const columns = [
    {
      dataIndex: 'ems_panorama_id',
      hideInForm: true,
      hideInSearch: true,
      hideInTable: true,
      hideInDescriptions: true,
    },
    {
      title: '场景名称',
      key: 'ems_panorama_name',
      dataIndex: 'ems_panorama_name',
      render: (_, record) => (
        <>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <Space size={24}>
              <Image
                width={100}
                height={50}
                preview={{
                  src: `/systemfile${record?.ems_panorama_sourceimage?.ems_sysfile_path}`,
                }}
                src={`/systemfile${record?.ems_panorama_cover?.ems_sysfile_path}`}
              />
              <Text
                style={{ width: '100px', fontSize: '16px' }}
                ellipsis={{ tooltip: record?.ems_panorama_name }}
              >
                <a
                  onClick={() => {
                    goToParanomicTool(record);
                  }}
                >
                  {record?.ems_panorama_name}
                </a>
              </Text>
              <Tag color="#108ee9">{record?.ems_panorama_groupname}</Tag>
            </Space>
          </div>
        </>
      ),
    },
    {
      title: '开放视角',
      key: 'ems_panorama_viewangles',
      dataIndex: 'ems_panorama_viewangles',
      render: (_, record) => (
        <>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <Space>
              {record?.ems_panorama_viewangles?.map((item) => {
                return <Tag>{item?.ems_viewangle_name}</Tag>;
              })}
            </Space>
          </div>
        </>
      ),
    },
    {
      title: '创建时间',
      key: 'ems_panorama_createtime',
      dataIndex: 'ems_panorama_createtime',
      valueType: 'dateTime',
    },
    {
      title: '操作',
      fixed: 'right',
      dataIndex: 'option',
      valueType: 'option',
      render: (_, record) => [
        <a
          key={`edit-${record.ems_panorama_id}`}
          onClick={(e) => {
            if (e && e.stopPropagation) {
              e.stopPropagation();
              panoramaRef?.current?.show(record);
            }
          }}
        >
          <Tooltip title="编辑">
            <EditOutlined />
          </Tooltip>
        </a>,
        <a key={`delete-${record.ems_panorama_id}`}>
          <Tooltip title="删除">
            <Popconfirm
              title="是否删除该项！"
              okText="是"
              cancelText="否"
              onConfirm={() => {
                deletePanorama({ ems_panorama_id: record.ems_panorama_id }).then((res) => {
                  if (res.code == 200) {
                    message.success(res.message);
                  } else {
                    message.error(res.message);
                  }
                  actionRef.current.reload();
                });
              }}
            >
              <DeleteOutlined />
            </Popconfirm>
          </Tooltip>
        </a>,
        <a key={`opts-${record.ems_panorama_id}`}>
          <Tooltip title="配置">
            <SettingOutlined
              onClick={() => {
                goToParanomicTool(record);
              }}
            />
          </Tooltip>
        </a>,
        <a key={`download-${record.ems_panorama_id}`}>
          <Tooltip title="下载原图">
            <DownloadOutlined
              onClick={() => {
                downloadImg(record);
              }}
            />
          </Tooltip>
        </a>,
      ],
    },
  ];

  const request = async (
    // 第一个参数 params 查询表单和 params 参数的结合,表单搜索项会从 params 传入，
    // 第一个参数中一定会有 pageSize 和  current ，这两个参数是 antd 的规范
    params: any & {
      pageSize: number;
      current: number;
    },
    sort,
    filter,
  ) => {
    //const hide = message.loading('正在查询');
    try {
      // 这里需要返回一个 Promise,在返回之前你可以进行数据转化
      // 如果需要转化参数可以在这里进行修改
      const queryParams = {
        ems_panorama_viewangleid: viewId,
        ems_panorama_groupid: groupId,
        ems_panorama_instanceid: instanceId,
        ...params,
        pageNum: params.current,
        ...sort,
        ...filter,
      };
      if (!groupId || groupId === -1) {
        delete queryParams.ems_panorama_groupid;
      }
      const response = await findPanoramaByMapV1(queryParams);

      const list = {
        data: response?.result?.list,
        // success 请返回 true，
        // 不然 table 会停止解析数据，即使有数据
        success: response.code === 200 ? true : false,
        /** 列表的内容总数 */
        total: response?.result?.total,
      };

      return list;
    } catch (error) {
      return false;
    }
  };

  const onTabChange = (key: string) => {
    setActiveKey(key);
  };

  const changGroupId = (id: number) => {
    setGroupId(id);
  };

  /**
   * 改变视角
   * @param id
   */
  const handleChangeView = (id) => {
    setViewId(id);
  };

  /**
   * 视角列表数据
   */

  const { data: viewDataList } = useRequest(
    () => {
      return findByViewangleMap({ ems_viewangle_instanceid: instanceId });
    },
    {
      formatResult: (res: any) => {
        const list = res.result?.list || [];
        return [{ ems_viewangle_id: null, ems_viewangle_name: '全部' }, ...list];
      },
    },
  );

  useEffect(() => {
    findInstanceByMap({ ems_instance_id: instanceId }).then((res) => {
      const list = res.result?.list || [];
      const instanceData = list[0] || {};
      setInstanceData(instanceData);
    });
  }, []);

  return (
    <PageContainer
      className="PanoramicConfig"
      onBack={() => {
        history.push(`/msManagement`);
      }}
      header={{
        avatar: { src: `/systemfile${instanceData?.ems_instance_picfile?.ems_sysfile_path}` },
        title: (
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <span style={{ marginRight: 8, marginLeft: 8 }}>{instanceData.ems_instance_name}</span>
          </div>
        ),
        tags: <Tag color="blue">{instanceData.ems_instance_no}</Tag>,
      }}
      content={
        <div>
          {instanceData.ems_instance_des && (
            <Alert message={`简述 : ${instanceData.ems_instance_des}`} type="info" showIcon />
          )}
        </div>
      }
      style={{
        background: '#F4F7FA',
      }}
    >
      <Card
        style={{ width: '100%' }}
        title={false}
        extra={false}
        tabList={tabList}
        activeTabKey={activeKey}
        onTabChange={(key) => {
          onTabChange(key);
        }}
      >
        {activeKey === '1' && (
          <>
            <Typography>
              <Title level={4}>VR全景设计</Title>
              <Paragraph>
                包含了站点各楼层的全景布置、转场的设计、标签热点布置、实时工况设备绑定、图像数据融合等。
              </Paragraph>
            </Typography>
            <ProCard.Group split="vertical">
              {/* 左侧模块 */}
              <ProCard colSpan="20%" className={styles.leftCardList} title="场景管理">
                <SceneManager changeId={changGroupId} />
              </ProCard>
              {/* 右侧模块 */}
              <ProCard
                bodyStyle={{ paddingInline: 0 }}
                title={
                  <Segmented
                    options={[
                      {
                        label: '列表',
                        value: 0,
                      },
                      {
                        label: '关系图',
                        value: 1,
                      },
                    ]}
                  />
                }
                extra={
                  <Space className="view">
                    <Segmented
                      value={viewId}
                      onChange={handleChangeView}
                      options={viewDataList?.map((item) => {
                        return {
                          label: item.ems_viewangle_name,
                          value: item.ems_viewangle_id,
                        };
                      })}
                    />
                    <Button
                      type="primary"
                      icon={<FolderOpenOutlined />}
                      onClick={(e) => {
                        if (e && e.stopPropagation) {
                          e.stopPropagation();
                          mediaLibraryRef?.current?.show();
                        }
                      }}
                    >
                      从素材库添加
                    </Button>
                    <Button
                      type="primary"
                      icon={<CloudUploadOutlined />}
                      onClick={(e) => {
                        if (e && e.stopPropagation) {
                          e.stopPropagation();
                          uploadRef?.current?.show();
                        }
                      }}
                    >
                      上传本地文件
                    </Button>
                  </Space>
                }
              >
                <ProTable
                  request={request}
                  actionRef={actionRef}
                  className="stationTable"
                  size="large"
                  options={{ fullScreen: false, reload: false, setting: false, density: false }}
                  rowKey="ems_panorama_id"
                  params={{ ems_panorama_groupid: groupId, ems_panorama_viewangleid: viewId }}
                  search={false}
                  defaultSize="middle"
                  columns={columns}
                  rowSelection={{
                    selections: [Table.SELECTION_ALL, Table.SELECTION_INVERT],
                    defaultSelectedRowKeys: [],
                    onChange: (_, selectedRows) => {
                      setSelectedRowKeys(selectedRows);
                    },
                  }}
                  tableAlertRender={({ selectedRowKeys, selectedRows, onCleanSelected }) => (
                    <Space size={24}>
                      <span>
                        已选 {selectedRowKeys.length} 项
                        <a style={{ marginLeft: 8 }} onClick={onCleanSelected}>
                          取消选择
                        </a>
                      </span>
                    </Space>
                  )}
                  tableAlertOptionRender={() => {
                    return (
                      <Space size={16}>
                        <a>批量删除</a>
                      </Space>
                    );
                  }}
                  pagination={{
                    pageSize: 10,
                  }}
                />
              </ProCard>
            </ProCard.Group>
            <AddOrEditPanorama
              instanceid={instanceId}
              ref={panoramaRef}
              refreshList={() => {
                actionRef.current.reload();
              }}
            />
            <MediaLibrary ref={mediaLibraryRef} />
            <UploadPanorama
              ref={uploadRef}
              groupId={groupId}
              instanceId={instanceId}
              refreshList={() => {
                actionRef.current.reload();
              }}
            />
          </>
        )}
        {activeKey === '2' && <AerialViewConfig instanceData={instanceData} />}
        {activeKey === '3' && <ShareConfig instanceData={instanceData} />}
      </Card>
    </PageContainer>
  );
};

export default PanoramicConfig;
