import {
  DrawerForm,
  ProFormText,
  ProFormTextArea,
  ProForm,
  ProFormItem,
  ProFormDateTimePicker,
} from '@ant-design/pro-components';
import { vrPreview as editVrPreview } from '@/services/swagger/vrPreviewController';
import { message, Input, Upload, Avatar } from 'antd';
import React, { useImperativeHandle, useState, useRef } from 'react';
import { LoadingOutlined, PlusOutlined, createFromIconfontCN } from '@ant-design/icons';

const { Dragger } = Upload;
const IconFont = createFromIconfontCN({
  scriptUrl: ['/iconfont/iconfont.js'],
});
type propsType = {
  refreshList: Function;
};

const AddOrEditPanorama = React.forwardRef((props: propsType, ref: any) => {
  const formRef = useRef<any>();
  const [vrPreview, setVrPreview] = useState<any>({});
  const [drawerVisit, setDrawerVisit] = useState(false);

  // 方法编写, 例如一些提交保存等操作
  const handleConfirm = async (form) => {
    try {
      await formRef.current?.validateFields();
      const formData: any = {
        ems_vrpreview_endtime: form.ems_vrpreview_endtime,
        ems_vrpreview_id: vrPreview.ems_vrpreview_id,
      };
      if (vrPreview?.ems_vrpreview_id) {
        const result = await editVrPreview({
          ...formData,
        });
        if (result.code == 200) {
          message.success(result.message);
        }
      }
      onVisibleChange(false);
      props.refreshList();
    } catch (error: any) {
      if (error) {
        console.log(error);
      }
    }
  };

  const onVisibleChange = (e: boolean) => {
    setDrawerVisit(e);
  };

  useImperativeHandle(ref, () => ({
    async show(result: any) {
      setDrawerVisit(true);
      setTimeout(() => {
        setVrPreview(result);
        if (result) formRef.current?.setFieldsValue(result);
      }, 0);
    },
  }));

  return (
    <>
      <DrawerForm
        formRef={formRef}
        visible={drawerVisit}
        onVisibleChange={onVisibleChange}
        width={500}
        title={`编辑分享链接`}
        onFinish={handleConfirm}
        layout="horizontal"
        grid={true}
        rowProps={{
          gutter: [0, 6],
        }}
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 20 }}
      >
        {/* <h4 className={styles.hTitle}>基本信息</h4> */}
        <ProFormText
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 19 }}
          name="ems_vrpreview_accessurl"
          label="链接地址"
          allowClear={false}
          // readonly
          disabled
          placeholder=""
        />
        <ProFormDateTimePicker
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 19 }}
          name="ems_vrpreview_createtime"
          readonly
          label="创建时间"
        />
        <ProFormDateTimePicker
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 19 }}
          name="ems_vrpreview_endtime"
          dataFormat="YYYY/MM/DD HH:mm:ss"
          label="结束时间"
          placeholder="请输入结束时间"
          rules={[{ required: true, message: '请输入结束时间' }]}
        />
      </DrawerForm>
    </>
  );
});
export default AddOrEditPanorama;
