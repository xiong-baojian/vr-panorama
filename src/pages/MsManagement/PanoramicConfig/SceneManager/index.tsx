import React, { useState, useRef, useEffect } from 'react';
import { ProList, CheckCard } from '@ant-design/pro-components';
import { Tabs, Space, Tag, Typography, Tooltip, Avatar } from 'antd';
import {
  createFromIconfontCN,
  MoreOutlined,
  EditOutlined,
  DeleteOutlined,
  SettingOutlined,
  CopyOutlined,
  FieldTimeOutlined,
} from '@ant-design/icons';
import { findGroupByInstance } from '@/services/swagger/InstancePanoramaGroupController';
import { useModel, useParams } from 'umi';
import EditScene from './EditScene';

import './index.less';

const { Paragraph, Title, Text } = Typography;

const defaultData = [
  {
    id: '1',
    name: '默认分组',
  },
];

type DataItem = (typeof defaultData)[number];

const SceneManager: React.FC = (props: any) => {
  const { changeId } = props;
  const editSceneRef = useRef<any>();
  const [rootId, setRootId] = useState<any>(null);
  const [group, setGroup] = useState<any>([]);
  const [dataSource, setDataSource] = useState<DataItem[]>([]);
  const [items, setItems] = useState<any>([]);
  const [activeKey, setActiveKey] = useState<string>('');
  const [checkedTag, setCheckedTag] = useState<number | null>(null);
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const params: any = useParams();
  const { instanceId } = params;

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const onChange = (activeKey: string) => {
    setActiveKey(activeKey);
    changeDataSoure(activeKey);
    setSelectedRowKeys([]);
    changeId(Number(activeKey));
    setRootId(null);
    setCheckedTag(null);
  };

  const handleChangeId = (id) => {
    setRootId(id);
    changeId(id || Number(activeKey));
  };

  const changeDataSoure = (activeKey: string, data = group) => {
    let dataSourceArr: any = [];
    if (activeKey == '-1') {
      data?.forEach((item) => {
        dataSourceArr.push(...item.childrens);
      });
    } else {
      data?.forEach((item) => {
        if (item.ems_instance_panorama_group_id === Number(activeKey)) {
          dataSourceArr = item.childrens;
        }
      });
    }
    setDataSource(dataSourceArr);
  };

  const refresh = () => {
    findGroupByInstance({
      ems_instance_panorama_group_instanceid: instanceId,
    }).then((res) => {
      const tabItems: any = [
        {
          label: '全部',
          children: '',
          key: -1,
        },
      ];
      res.result?.forEach((item) => {
        tabItems.push({
          label: item.ems_instance_panorama_group_name,
          children: '',
          key: item.ems_instance_panorama_group_id,
        });
      });
      setItems(tabItems);
      setActiveKey(tabItems[0].key);
      setGroup(res.result || []);

      changeDataSoure('-1', res.result || []);
    });
  };
  useEffect(() => {
    refresh();
  }, []);

  return (
    <>
      <EditScene ref={editSceneRef} refresh={refresh} />
      {/* 场景管理 */}
      <div style={{ width: '100%', display: 'flex' }}>
        <Tabs onChange={onChange} items={items} activeKey={activeKey} />
        {/* 添加 */}
        <MoreOutlined
          className="sceneManagerList-moreIcon"
          onClick={() => {
            editSceneRef.current.show();
          }}
        />
      </div>
      <CheckCard.Group
        value={rootId}
        style={{ width: '100%' }}
        onChange={(value) => {
          setCheckedTag(null);
          handleChangeId(value);
        }}
      >
        {dataSource?.map((item: any) => {
          return (
            <CheckCard
              style={{
                background: 'rgba(33, 33, 33, 0.01)',
                width: '100%',
                // backgroundImage: 'url("/sky.jpg")',
                // backgroundSize: 'cover',
                // backgroundPosition: 'center',
              }}
              title={
                <Space>
                  <Avatar
                    size="small"
                    src="https://gw.alipayobjects.com/zos/antfincdn/UCSiy1j6jx/xingzhuang.svg"
                  />
                  <Text>{item.ems_instance_panorama_group_name}</Text>
                </Space>
              }
              description={
                <Space wrap style={{ marginTop: 5 }}>
                  {item.childrens?.map((item) => {
                    return (
                      <Tag
                        key={item.ems_instance_panorama_group_id}
                        color={
                          item.ems_instance_panorama_group_id === checkedTag ? '#108ee9' : '#5BD8A6'
                        }
                        onClick={(e) => {
                          e.stopPropagation();
                          setCheckedTag(item.ems_instance_panorama_group_id);
                          handleChangeId(item.ems_instance_panorama_group_id);
                        }}
                      >
                        {item.ems_instance_panorama_group_name}
                      </Tag>
                    );
                  })}
                </Space>
              }
              value={item.ems_instance_panorama_group_id}
            />
          );
        })}
      </CheckCard.Group>
    </>
  );
};
export default SceneManager;
