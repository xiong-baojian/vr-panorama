import {
  DrawerForm,
  ProFormDependency,
  ProFormTreeSelect,
  ProFormText,
  ProFormRadio,
} from '@ant-design/pro-components';
import { useParams } from 'umi';
import {
  findGroupByInstance,
  insertInstancePanoramaGroup,
  updateInstancePanoramaGroup,
  deleteInstancePanoramaGroup,
} from '@/services/swagger/InstancePanoramaGroupController';
import React, { useState, useImperativeHandle, useRef, useEffect } from 'react';
import { message } from 'antd';
const EditScene = React.forwardRef((props: any, ref: any) => {
  const params: any = useParams();
  const { instanceId } = params;
  const [group, setGroup] = useState<any>([]);
  const [visible, setVisible] = useState(false);
  const [menu, setMenu] = useState<any>({});
  const formRef = useRef<any>();

  useEffect(() => {
    if (visible) {
      if (menu?.ems_item_parentid === 0) {
        menu.ems_item_parentid = null;
      }
      formRef.current?.setFieldsValue(menu);
    } else {
      formRef.current?.resetFields();
    }
  }, [visible]);

  useImperativeHandle(ref, () => ({
    async show(menu: any) {
      setMenu(menu);
      setVisible(true);
    },
  }));

  return (
    <DrawerForm
      width={600}
      formRef={formRef}
      layout="horizontal"
      grid={true}
      rowProps={{
        gutter: [0, 8],
      }}
      labelCol={{ span: 4 }}
      wrapperCol={{ span: 20 }}
      onOpenChange={setVisible}
      title={'场景管理'}
      open={visible}
      onFinish={async (values) => {
        const {
          type,
          ems_instance_panorama_group_pid,
          ems_instance_panorama_group_id,
          ems_instance_panorama_group_name,
        } = values;
        let params: any = {
          ems_instance_panorama_group_instanceid: instanceId,
        };
        let res: any;
        if (type === 0) {
          params.ems_instance_panorama_group_pid = ems_instance_panorama_group_pid || 0;
          params.ems_instance_panorama_group_name = ems_instance_panorama_group_name;
          res = await insertInstancePanoramaGroup(params);
          const { code } = res;
          if (code === 200) {
            message.success(res.message);
          } else {
            message.error(res.message);
          }
        } else if (type === 1) {
          params.ems_instance_panorama_group_pid = ems_instance_panorama_group_pid || 0;
          params.ems_instance_panorama_group_id = ems_instance_panorama_group_id;
          params.ems_instance_panorama_group_name = ems_instance_panorama_group_name;
          res = await updateInstancePanoramaGroup(params);
          const { code } = res;
          if (code === 200) {
            message.success(res.message);
          } else {
            message.error(res.message);
          }
        } else if (type === 2) {
          params.ems_instance_panorama_group_id = ems_instance_panorama_group_id;
          res = await deleteInstancePanoramaGroup(params);
          const { code } = res;
          if (code === 200) {
            message.success(res.message);
          } else {
            message.error(res.message);
          }
        }
        props.refresh();
        return true;
      }}
    >
      <ProFormRadio.Group
        name="type"
        label="操作类型"
        initialValue={0}
        options={[
          {
            label: '添加分组',
            value: 0,
          },
          {
            label: '编辑分组',
            value: 1,
          },
          {
            label: '删除分组',
            value: 2,
          },
        ]}
        fieldProps={{
          onChange(e) {
            formRef.current.setFieldValue('ems_instance_panorama_group_name', null);
            formRef.current.setFieldValue('ems_instance_panorama_group_pid', null);
          },
        }}
      />

      <ProFormDependency name={['type']}>
        {({ type }) => {
          if (type === 0) {
            return (
              <>
                <ProFormTreeSelect
                  label="父级分组"
                  name="ems_instance_panorama_group_pid"
                  placeholder="请选择父级分组,不选择默认根节点!"
                  allowClear
                  secondary
                  request={async () => {
                    const res = await findGroupByInstance({
                      ems_instance_panorama_group_instanceid: instanceId,
                    });
                    return res.result || [];
                  }}
                  fieldProps={{
                    treeIcon: true,
                    treeLine: true,
                    filterTreeNode: true,
                    showSearch: true,
                    // labelInValue: true,
                    autoClearSearchValue: true,
                    treeNodeFilterProp: 'ems_instance_panorama_group_name',
                    fieldNames: {
                      label: 'ems_instance_panorama_group_name',
                      value: 'ems_instance_panorama_group_id',
                      children: 'childrens',
                    },
                  }}
                />
                <ProFormText
                  name="ems_instance_panorama_group_name"
                  label="分组名称"
                  placeholder="请输入分组名称"
                  rules={[{ required: true, message: '分组名称为必填项' }]}
                />
              </>
            );
          } else if (type === 1) {
            return (
              <>
                <ProFormTreeSelect
                  label="场景分组"
                  name="ems_instance_panorama_group_id"
                  placeholder="请选择"
                  allowClear
                  secondary
                  request={async () => {
                    const res = await findGroupByInstance({
                      ems_instance_panorama_group_instanceid: instanceId,
                    });
                    setGroup(res.result || []);
                    return res.result || [];
                  }}
                  fieldProps={{
                    treeIcon: true,
                    treeLine: true,
                    filterTreeNode: true,
                    showSearch: true,
                    // labelInValue: true,
                    autoClearSearchValue: true,
                    treeNodeFilterProp: 'ems_instance_panorama_group_name',
                    fieldNames: {
                      label: 'ems_instance_panorama_group_name',
                      value: 'ems_instance_panorama_group_id',
                      children: 'childrens',
                    },
                    onChange(value, labelList, extra) {
                      const label = labelList[0];
                      function findNodeById(nodes, id) {
                        for (let node of nodes) {
                          if (node.ems_instance_panorama_group_id === id) {
                            return node;
                          }
                          if (node.childrens && node.childrens.length > 0) {
                            const result = findNodeById(node.childrens, id);
                            if (result) {
                              return result;
                            }
                          }
                        }
                        return null;
                      }
                      const item = findNodeById(group, value);
                      if (item && item.ems_instance_panorama_group_pid) {
                        formRef.current.setFieldValue(
                          'ems_instance_panorama_group_pid',
                          item.ems_instance_panorama_group_pid,
                        );
                      }
                      formRef.current.setFieldValue('ems_instance_panorama_group_name', label);
                    },
                  }}
                  rules={[{ required: true, message: '场景分组为必选项!' }]}
                />
                <ProFormTreeSelect
                  label="父级分组"
                  name="ems_instance_panorama_group_pid"
                  placeholder="请选择"
                  allowClear
                  secondary
                  request={async () => {
                    const res = await findGroupByInstance({
                      ems_instance_panorama_group_instanceid: instanceId,
                    });
                    return res.result || [];
                  }}
                  fieldProps={{
                    treeIcon: true,
                    treeLine: true,
                    filterTreeNode: true,
                    showSearch: true,
                    // labelInValue: true,
                    autoClearSearchValue: true,
                    treeNodeFilterProp: 'ems_instance_panorama_group_name',
                    fieldNames: {
                      label: 'ems_instance_panorama_group_name',
                      value: 'ems_instance_panorama_group_id',
                      children: 'childrens',
                    },
                  }}
                />
                <ProFormText
                  name="ems_instance_panorama_group_name"
                  label="分组名称"
                  placeholder="请输入分组名称"
                  rules={[{ required: true, message: '分组名称为必填项' }]}
                />
              </>
            );
          } else {
            return (
              <>
                <ProFormTreeSelect
                  label="场景分组"
                  name="ems_instance_panorama_group_id"
                  placeholder="请选择"
                  allowClear
                  secondary
                  request={async () => {
                    const res = await findGroupByInstance({
                      ems_instance_panorama_group_instanceid: instanceId,
                    });
                    return res.result || [];
                  }}
                  fieldProps={{
                    treeIcon: true,
                    treeLine: true,
                    filterTreeNode: true,
                    showSearch: true,
                    // labelInValue: true,
                    autoClearSearchValue: true,
                    treeNodeFilterProp: 'ems_instance_panorama_group_name',
                    fieldNames: {
                      label: 'ems_instance_panorama_group_name',
                      value: 'ems_instance_panorama_group_id',
                      children: 'childrens',
                    },
                  }}
                  rules={[{ required: true, message: '场景分组为必选项!' }]}
                />
              </>
            );
          }
        }}
      </ProFormDependency>
    </DrawerForm>
  );
});

export default EditScene;
