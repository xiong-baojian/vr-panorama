import {
  DrawerForm,
  ProFormText,
  ProFormTextArea,
  ProForm,
  CheckCard,
  ProFormTreeSelect,
} from '@ant-design/pro-components';
import { useRequest, useParams } from 'umi';
import styles from './index.less';
import { message, Divider, Upload, Tooltip, Typography } from 'antd';
import React, { useImperativeHandle, useState, useRef } from 'react';
import { LoadingOutlined, PlusOutlined, createFromIconfontCN } from '@ant-design/icons';
import { findGroupByInstance } from '@/services/swagger/InstancePanoramaGroupController';
import { findByViewangleMap } from '@/services/swagger/ViewangleController';
import {
  insertPanorama,
  uploadCover,
  getPanoramaView,
  getPanoramaViewV1,
  updatePanorama,
  batchUploadPanoramaFileV1,
} from '@/services/swagger/panoramaController';
import { loadImage } from '@/utils/panoramaToCubmap.js';
const { Text } = Typography;

const { Dragger } = Upload;
const IconFont = createFromIconfontCN({
  scriptUrl: ['/iconfont/iconfont.js'],
});
type propsType = {
  refreshList: Function;
  instanceid: any;
};

const AddOrEditPanorama = React.forwardRef((props: propsType, ref: any) => {
  const params: any = useParams();
  const { instanceId } = params;
  const formRef = useRef<any>();
  const [panorama, setPanorama] = useState<any>(null);
  const [drawerVisit, setDrawerVisit] = useState(false);
  const [loading, setLoading] = useState(false);
  const [posterLoading, setPosterLoading] = useState(false);
  const [panoramaImgFile, setPanoramaImgFile] = useState(null);
  const [defaultFileList, setDefaultFileList] = useState<any>([]);
  const [posterFile, setPosterFile] = useState<any>({});
  const [defaultPosterFileList, setDefaultPosterFileList] = useState<any>([]);

  const onPosterChange = (info: any) => {
    setDefaultPosterFileList(info.fileList);
  };

  const onPanoramaChange = (info: any) => {
    setDefaultFileList(info.fileList);
    if (info.file.status === 'done') {
      setLoading(true);
      loadImage(info.file?.originFileObj, (fileList: any) => {
        let params = fileList;
        params.unshift(info.file.originFileObj);
        batchUploadPanoramaFileV1({}, params).then((res: any) => {
          if (res.code == 200) {
            setPanoramaImgFile(res.result);
            message.success(`上传成功！`);
          } else {
            message.error(`上传失败！`);
          }
          setLoading(false);
        });
      });
    }
  };
  const beforeUpload = (file: any) => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('请上传图片形式文件！');
    }
    return isJpgOrPng;
  };

  // 方法编写, 例如一些提交保存等操作
  const handleConfirm = async () => {
    try {
      await formRef.current?.validateFields();
      const form = formRef?.current?.getFieldsValue();
      delete form.panoramaImgs;
      const formData: any = {
        ...form,
        ems_panorama_instanceid: props.instanceid,
        ems_panorama_coverfile: posterFile,
        ems_panorama_slicefiles: panoramaImgFile,
      };
      // 修改和新增
      if (panorama?.ems_panorama_id) {
        const result = await updatePanorama({
          ems_panorama_id: panorama.ems_panorama_id,
          ...formData,
        });
        if (result.code == 200) {
          message.success(result.message);
        }
      } else {
        const result = await insertPanorama(formData);
        if (result.code == 200) {
          message.success(result.message);
        }
      }
      setDefaultFileList([]);
      onVisibleChange(false);
      props.refreshList();
    } catch (error: any) {
      if (error) {
        console.log(error);
      }
    }
  };

  /**
   * 视角列表数据
   */

  const { data: viewDataList } = useRequest(
    () => {
      return findByViewangleMap({ ems_viewangle_instanceid: instanceId });
    },
    {
      formatResult: (res: any) => {
        return res.result?.list || [];
      },
    },
  );

  const onVisibleChange = (e: boolean) => {
    setDrawerVisit(e);
    setDefaultFileList([]);
    setDefaultPosterFileList([]);
  };

  useImperativeHandle(ref, () => ({
    async show(result: any) {
      setDrawerVisit(true);
      setPanorama(result);
      formRef.current?.resetFields();
      if (result)
        await getPanoramaViewV1({ ems_panorama_id: result?.ems_panorama_id }).then((res) => {
          const { result } = res;
          if (result && result.ems_panorama_cover) {
            result.ems_panorama_cover = [
              {
                uid: result?.ems_panorama_cover?.ems_sysfile_id,
                name: result?.ems_panorama_cover?.ems_sysfile_name,
                status: 'done',
                url: `/systemfile${result?.ems_panorama_cover?.ems_sysfile_path}`,
                thumbUrl: `/systemfile${result?.ems_panorama_cover?.ems_sysfile_path}`,
              },
            ];
            setDefaultPosterFileList(result.ems_panorama_cover);
          }
          if (result && result.ems_panorama_sourceimage) {
            const fileList: any = [
              {
                uid: result?.ems_panorama_sourceimage?.ems_sysfile_id,
                name: result?.ems_panorama_sourceimage?.ems_sysfile_name,
                status: 'done',
                url: `/systemfile${result?.ems_panorama_sourceimage?.ems_sysfile_path}`,
                thumbUrl: `/systemfile${result?.ems_panorama_sourceimage?.ems_sysfile_path}`,
              },
            ];
            setDefaultFileList(fileList);
          }
          result.ems_panorama_viewangleids = result?.ems_panorama_viewangles?.map((item) => {
            return item.ems_viewangle_id;
          });
          formRef.current?.setFieldsValue(result);
        });
    },
  }));

  return (
    <>
      {loading && <div className={styles.drawerMask}></div>}
      <DrawerForm
        formRef={formRef}
        visible={drawerVisit}
        onVisibleChange={onVisibleChange}
        width={600}
        title={`${panorama ? '编辑' : '新增'}全景场景`}
        onFinish={handleConfirm}
        layout="horizontal"
        grid={true}
        rowProps={{
          gutter: [0, 2],
        }}
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 20 }}
      >
        <h4 className={styles.hTitle}>基本信息</h4>
        <ProFormText
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 20 }}
          name="ems_panorama_name"
          label="场景名称"
          placeholder="请输入场景名称"
          rules={[{ required: true, message: '请输入场景名称' }]}
        />
        <ProFormTreeSelect
          label="场景位置"
          name="ems_panorama_groupid"
          placeholder="请选择场景位置!"
          rules={[{ required: true, message: '场景位置为必选项' }]}
          allowClear
          secondary
          request={async () => {
            const res = await findGroupByInstance({
              ems_instance_panorama_group_instanceid: props.instanceid,
            });
            return res.result || [];
          }}
          fieldProps={{
            treeIcon: true,
            treeLine: true,
            filterTreeNode: true,
            showSearch: true,
            // labelInValue: true,
            autoClearSearchValue: true,
            treeNodeFilterProp: 'ems_instance_panorama_group_name',
            fieldNames: {
              label: 'ems_instance_panorama_group_name',
              value: 'ems_instance_panorama_group_id',
              children: 'childrens',
            },
          }}
        />
        <ProForm.Item
          style={{ width: '100%' }}
          name="ems_panorama_viewangleids"
          label="开放视角"
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 20 }}
        >
          <CheckCard.Group size="small" style={{ width: '100%' }} multiple>
            {viewDataList?.map((item) => {
              return (
                <CheckCard
                  key={item.ems_viewangle_id}
                  title={
                    <Tooltip title={item.ems_viewangle_name}>
                      <Text ellipsis={item.ems_viewangle_name}>{item.ems_viewangle_name}</Text>
                    </Tooltip>
                  }
                  value={item.ems_viewangle_id}
                />
              );
            })}
          </CheckCard.Group>
        </ProForm.Item>
        <Divider style={{ margin: '0 0 10px' }} />
        <h4 className={styles.hTitle}>文件上传</h4>
        <ProForm.Item
          style={{ width: '100%' }}
          name="panoramaImgs"
          label="全景图片"
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 19 }}
        >
          <Dragger
            listType="picture"
            maxCount={1}
            onChange={onPanoramaChange}
            fileList={defaultFileList}
            beforeUpload={beforeUpload}
            customRequest={async (options) => {
              const { onSuccess }: any = options;
              onSuccess('上传成功');
              return true;
            }}
          >
            <p className="ant-upload-drag-icon">
              {loading ? <LoadingOutlined /> : <PlusOutlined />}
            </p>
            <p className="ant-upload-text">
              {loading ? (
                <p>
                  正在上传中...<p>已禁止其他操作，等待上传完成！</p>
                </p>
              ) : (
                '单击或拖动文件到此区域进行上传'
              )}
            </p>
            <p className="ant-upload-hint"></p>
          </Dragger>
        </ProForm.Item>
        <ProForm.Item
          style={{ width: '100%' }}
          name="ems_panorama_file"
          label="封面图片"
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 19 }}
        >
          <Dragger
            listType="picture"
            maxCount={1}
            onChange={onPosterChange}
            fileList={defaultPosterFileList}
            beforeUpload={beforeUpload}
            customRequest={async (options) => {
              setPosterLoading(true);
              const { onSuccess, onError, file }: any = options;
              const { result, code }: any = await uploadCover({}, file);
              if (code == 200) {
                const fileList: any = [
                  {
                    uid: result.ems_sysfile_id,
                    name: result.ems_sysfile_name,
                    status: 'done',
                    url: `/systemfile${result.ems_sysfile_path}`,
                    thumbUrl: `/systemfile${result.ems_sysfile_path}`,
                  },
                ];
                message.success('上传成功');
                setPosterLoading(false);
                setPosterFile(result);
                setDefaultPosterFileList(fileList);
                onSuccess('上传成功');
              } else {
                setPosterLoading(false);
                message.success('上传失败');
                onError('上传失败');
              }
            }}
          >
            <p className="ant-upload-drag-icon">
              {posterLoading ? <LoadingOutlined /> : <PlusOutlined />}
            </p>
            <p className="ant-upload-text">
              {posterLoading ? '正在上传中...' : '单击或拖动文件到此区域进行上传'}
            </p>
            <p className="ant-upload-hint"></p>
          </Dragger>
        </ProForm.Item>
        <div id="cubemap" style={{ display: 'none' }}>
          <b id="generating" style={{ visibility: 'hidden' }}>
            Generating...
          </b>
          <output id="faces"></output>
        </div>
        <Divider style={{ margin: '0 0 10px' }} />
        <h4 className={styles.hTitle}>更多信息</h4>
        <ProFormTextArea width="lg" label="场景描述" name="ems_panorama_des" />
      </DrawerForm>
    </>
  );
});
export default AddOrEditPanorama;
