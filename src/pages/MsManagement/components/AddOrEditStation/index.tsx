import {
  DrawerForm,
  ProFormText,
  ProFormItem,
  ProForm,
  CheckCard,
  ProFormCascader,
  ProFormDateTimeRangePicker,
  ProFormTextArea,
  ProFormList,
  ProFormGroup,
} from '@ant-design/pro-components';
import styles from './index.less';
import { message, Space, Upload, Row, Col, Tag, Tooltip, Typography, Avatar } from 'antd';
import React, { useImperativeHandle, useState, useRef, useEffect } from 'react';
import {
  LoadingOutlined,
  PlusOutlined,
  createFromIconfontCN,
  CloseCircleOutlined,
} from '@ant-design/icons';
import { insertInstance, updateInstance } from '@/services/swagger/instanceController';
import { uploadInstanceFile } from '@/services/swagger/instanceController';
import { findOpenindustryByMap } from '@/services/swagger/openindustryController';
import { findOpenfacilityByMap } from '@/services/swagger/openfacilityController';
import { findOpensubjectByMap } from '@/services/swagger/opensubjectController';
import { findByViewangleMap } from '@/services/swagger/ViewangleController';
import { useRequest } from 'umi';

const { Text } = Typography;
const IconFont = createFromIconfontCN({
  scriptUrl: ['/iconfont/iconfont.js'],
});
type propsType = {
  refreshList: Function;
};

const AddOrEditStation = React.forwardRef((props: propsType, ref: any) => {
  const actionRef = useRef<any>();
  const formRef = useRef<any>();
  const [instanceId, setInstanceId] = useState(null);
  const [exhibitionHallData, setExhibitionHallData] = useState<any>({});
  const [drawerVisit, setDrawerVisit] = useState(false);
  const [logoFile, setLogoFile] = useState<any>({});
  const [uploadLoading, setUploadLoading] = useState(false);

  const uploadButton = (
    <div>
      {uploadLoading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>上传</div>
    </div>
  );

  const { data: openIndustryData } = useRequest(
    () => {
      return findOpenindustryByMap({});
    },
    {
      formatResult: (res: any) => {
        return res?.result;
      },
    },
  );

  const { data: openFacilitiesData } = useRequest(
    () => {
      return findOpenfacilityByMap({});
    },
    {
      formatResult: (res: any) => {
        return res?.result || [];
      },
    },
  );

  const { data: openThemeData } = useRequest(
    () => {
      return findOpensubjectByMap({});
    },
    {
      formatResult: (res: any) => {
        return res?.result;
      },
    },
  );

  const permissionData = [
    {
      id: 0,
      title: '公开开放',
      des: '对所有用户开放，任何人都可以自由访问，无需身份验证或权限控制。',
      icon: 'icon-kaifangpingtai',
    },
    {
      id: 1,
      title: '仅对自己开放',
      des: '仅对特定用户或系统开发者开放，避免外部访问',
      icon: 'icon-quanxianguanli',
    },
    {
      id: 2,
      title: '限定访问',
      des: '对特定用户群体开放，限定访问要求用户通过身份验证才能使用',
      icon: 'icon-permissions-user',
    },
  ];

  const { data: adOptions } = useRequest(
    async () => {
      return fetch('/json/pca-code.json')
        .then((response) => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        })
        .then((data) => {
          return data;
        });
    },
    {
      formatResult: (res: any) => {
        return res;
      },
    },
  );

  // 方法编写, 例如一些提交保存等操作
  const handleConfirm = async () => {
    try {
      await formRef.current?.validateFields();
      let form = formRef?.current?.getFieldsValue();

      if (form.adcode) {
        form.ems_instance_adcode = JSON.stringify(form.adcode);
      }
      if (form.handlefacility) {
        form.ems_instance_handlefacility = JSON.stringify(form.handlefacility);
      }
      if (form.economicindicators) {
        form.ems_instance_economicindicators = JSON.stringify(form.economicindicators);
      }
      const formData: any = {
        ...form,
        ems_instance_picfile: logoFile,
      };
      if (exhibitionHallData?.ems_instance_id) {
        formData.ems_instance_id = exhibitionHallData?.ems_instance_id;
        await updateInstance(formData).then((res) => {
          if (res.code == 200) message.success(res.message);
        });
      } else {
        await insertInstance(formData).then((res) => {
          if (res.code == 200) message.success(res.message);
        });
      }
      onVisibleChange(false);
      props.refreshList();
    } catch (error: any) {
      if (error) {
        console.log(error);
      }
    }
  };

  const onVisibleChange = (visible: boolean) => {
    setDrawerVisit(visible);
  };

  useEffect(() => {
    if (drawerVisit) {
      const data = exhibitionHallData;
      const picFile = data?.ems_instance_picfile || {};
      setLogoFile(picFile);
      if (data?.ems_instance_adcode) {
        data.adcode = JSON.parse(data.ems_instance_adcode);
      }
      if (data?.ems_instance_economicindicators) {
        data.economicindicators = JSON.parse(data.ems_instance_economicindicators);
      }
      if (data?.ems_instance_handlefacility) {
        data.handlefacility = JSON.parse(data.ems_instance_handlefacility);
      }
      if (data?.ems_instance_viewangle) {
        data.viewangle = data?.ems_instance_viewangle;
      }
      formRef.current?.setFieldsValue(data);
      setInstanceId(data?.ems_instance_id);
    } else {
      setInstanceId(null);
      setExhibitionHallData({});
      setLogoFile({});
      formRef.current?.resetFields();
    }
  }, [drawerVisit]);

  useImperativeHandle(ref, () => ({
    async show(result: any) {
      setDrawerVisit(true);
      setExhibitionHallData(result);
    },
  }));

  return (
    <DrawerForm
      className={styles.drawerForm}
      formRef={formRef}
      open={drawerVisit}
      onOpenChange={onVisibleChange}
      width={600}
      title={`${exhibitionHallData?.ems_instance_id ? '编辑' : '新增'}展厅`}
      onFinish={handleConfirm}
      layout="horizontal"
      grid={true}
      rowProps={{
        gutter: [0, 0],
      }}
      labelCol={{ span: 4 }}
      wrapperCol={{ span: 20 }}
    >
      <h4 className={styles.hTitle}>基本信息</h4>
      <Row>
        <Col span={16}>
          <Row gutter={[0, 12]}>
            <ProFormText
              labelCol={{ span: 6 }}
              wrapperCol={{ span: 18 }}
              label="展厅名称"
              name="ems_instance_name"
              placeholder="请输入展厅名称"
              rules={[{ required: true, message: '请输入展厅名称' }]}
            />
            <ProFormText
              labelCol={{ span: 6 }}
              wrapperCol={{ span: 18 }}
              name="ems_instance_no"
              label="展厅编号"
              placeholder="请输入展厅编号"
            />
          </Row>
        </Col>
        <Col span={8}>
          <ProFormItem
            label="展厅封面"
            name="ems_instance_picfile"
            labelCol={{ span: 10 }}
            wrapperCol={{ span: 14 }}
          >
            <Upload
              listType="picture-card"
              showUploadList={false}
              maxCount={1}
              className="avatar-uploader"
              customRequest={async (options) => {
                setUploadLoading(true);
                const { onSuccess, onError, file }: any = options;
                const { result, code }: any = await uploadInstanceFile({ file: file });
                setUploadLoading(false);
                if (code == 200) {
                  setLogoFile(result);
                  onSuccess('上传成功');
                } else {
                  message.success('上传失败');
                  onError('上传失败');
                }
              }}
            >
              {logoFile?.ems_sysfile_path ? (
                <img style={{ width: '100px' }} src={`/systemfile${logoFile?.ems_sysfile_path}`} />
              ) : (
                uploadButton
              )}
            </Upload>
          </ProFormItem>
        </Col>
      </Row>
      <h4 className={styles.hTitle}>配置</h4>
      <ProForm.Item
        rules={[{ required: true, message: '请选择开放行业' }]}
        style={{ width: '100%' }}
        name="ems_instance_openindustryid"
        label="开放行业"
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 20 }}
      >
        <CheckCard.Group size="small" style={{ width: '100%' }}>
          {openIndustryData?.map((item) => {
            return (
              <CheckCard
                className={styles.checkCard}
                key={item.ems_openindustry_id}
                avatar={<IconFont type={item.ems_openindustry_icon} className={styles.icon2} />}
                title={
                  <Tooltip title={item.ems_openindustry_des}>
                    <Text
                      style={{ fontSize: 12, width: '100%' }}
                      ellipsis={item.ems_openindustry_name}
                    >
                      {item.ems_openindustry_name}
                    </Text>
                  </Tooltip>
                }
                value={item.ems_openindustry_id}
              />
            );
          })}
        </CheckCard.Group>
      </ProForm.Item>
      <ProForm.Item
        style={{ width: '100%' }}
        name="ems_instance_openfacilityid"
        label="开放设施"
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 20 }}
      >
        <CheckCard.Group size="small" style={{ width: '100%' }}>
          {openFacilitiesData?.map((item) => {
            return (
              <CheckCard
                className={styles.checkCard_facilities}
                key={item.ems_openfacility_id}
                avatar={<IconFont type={item.ems_openindustry_icon} />}
                title={
                  <Tooltip title={item.ems_openfacility_name}>
                    <Text
                      style={{ fontSize: 12, width: '100%' }}
                      ellipsis={item.ems_openfacility_name}
                    >
                      {item.ems_openfacility_name}
                    </Text>
                  </Tooltip>
                }
                value={item.ems_openfacility_id}
              />
            );
          })}
        </CheckCard.Group>
      </ProForm.Item>
      <ProForm.Item
        style={{ width: '100%' }}
        name="ems_instance_opensubjectid"
        label="开放主题"
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 20 }}
      >
        <CheckCard.Group size="small" style={{ width: '100%' }}>
          {openThemeData?.map((item) => {
            return (
              <CheckCard
                className={styles.checkCard_theme}
                key={item.ems_opensubject_id}
                avatar={<IconFont type={item.ems_openindustry_icon} />}
                title={
                  <Tooltip title={item.ems_opensubject_name}>
                    <Text
                      style={{ fontSize: 12, width: '100%' }}
                      ellipsis={item.ems_opensubject_name}
                    >
                      {item.ems_opensubject_name}
                    </Text>
                  </Tooltip>
                }
                value={item.ems_opensubject_id}
              />
            );
          })}
        </CheckCard.Group>
      </ProForm.Item>
      <ProForm.Item
        style={{ width: '100%' }}
        name="permission"
        label="权限类型"
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 20 }}
      >
        <CheckCard.Group size="small" style={{ width: '100%' }}>
          {permissionData?.map((item) => {
            return (
              <CheckCard
                className={styles.checkCard_permission}
                key={item.id}
                avatar={<IconFont type={item.icon} />}
                title={
                  <Tooltip title={item.des}>
                    <Text style={{ fontSize: 12, width: '100%' }} ellipsis={item.title}>
                      {item.title}
                    </Text>
                  </Tooltip>
                }
                value={item.id}
              />
            );
          })}
        </CheckCard.Group>
      </ProForm.Item>
      <ProFormCascader
        label="所在区域"
        fieldProps={{
          options: adOptions,
          fieldNames: {
            label: 'name',
            value: 'code',
          },
          changeOnSelect: true,
        }}
        name="adcode"
      />
      <ProFormDateTimeRangePicker name="time" label="开放时段" width="xl" />
      <ProFormList
        actionRef={actionRef}
        name="viewangle"
        label="开放视角"
        deleteIconProps={{
          Icon: CloseCircleOutlined,
          tooltipText: '删除',
        }}
        copyIconProps={false}
        alwaysShowItemLabel
        creatorButtonProps={{
          creatorButtonText: '新增视角',
        }}
      >
        {(meta, index, action, count) => {
          return <ProFormText name="ems_viewangle_name" key="ems_viewangle_id" />;
        }}
      </ProFormList>

      <h4 className={styles.hTitle}>其他</h4>
      <ProForm.Item
        style={{ width: '100%' }}
        label="经营指标"
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 20 }}
      >
        <ProFormList
          className={styles.proformlist}
          actionRef={actionRef}
          name="economicindicators"
          label=""
          deleteIconProps={{
            Icon: CloseCircleOutlined,
            tooltipText: '删除',
          }}
          copyIconProps={false}
          alwaysShowItemLabel
          creatorButtonProps={{
            creatorButtonText: '新增经营指标',
          }}
        >
          <ProFormGroup key="group">
            <Space>
              <ProFormText name="key" label="指标名称" />
              <ProFormText name="value" label="指标数值" />
            </Space>
          </ProFormGroup>
        </ProFormList>
      </ProForm.Item>
      <ProForm.Item
        style={{ width: '100%' }}
        label="处理能力"
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 20 }}
      >
        <ProFormList
          className={styles.proformlist}
          actionRef={actionRef}
          name="handlefacility"
          label=""
          deleteIconProps={{
            Icon: CloseCircleOutlined,
            tooltipText: '删除',
          }}
          copyIconProps={false}
          alwaysShowItemLabel
          creatorButtonProps={{
            creatorButtonText: '新增处置能力',
          }}
        >
          <ProFormGroup key="group">
            <Space>
              <ProFormText name="key" label="处置设施" />
              <ProFormText name="value" label="处理能力" />
            </Space>
          </ProFormGroup>
        </ProFormList>
      </ProForm.Item>

      <ProFormText
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 20 }}
        name="ems_instance_address"
        label="详细地址"
        placeholder="请输入详细地址"
      />
      <ProFormTextArea name="ems_instance_des" label="展厅描述" placeholder="请输入展厅描述" />
    </DrawerForm>
  );
});
export default AddOrEditStation;
