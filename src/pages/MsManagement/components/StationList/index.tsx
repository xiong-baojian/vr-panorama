import React, { useImperativeHandle, useState, useRef } from 'react';
import { ProTable } from '@ant-design/pro-components';
import { Table, Button, Tooltip, Popconfirm, Badge, Space, Tag, Avatar, Image } from 'antd';
import { EditOutlined, DeleteOutlined, SettingOutlined } from '@ant-design/icons';
import { history, useRequest, useModel } from 'umi';
import { findInstanceByMap, deleteInstance } from '@/services/swagger/instanceController';
import AddOrEditStation from '../AddOrEditStation';
import './index.less';

const StationList = React.forwardRef((props: any, ref: any) => {
  const [params, setParams] = useState({});
  const hallDrawerRef = useRef<any>();
  const actionRef = useRef<any>();

  const goToParanomicConfig = (id: any) => {
    history.push(`/msManagement/panoramicConfig/${id}`);
  };
  const handleView = (id, no) => {
    window.open(`/msManagement/panorama/${id}?no=${no}`, '_blank');
  };

  const columns = [
    {
      dataIndex: 'ems_instance_id',
      hideInForm: true,
      hideInSearch: true,
      hideInTable: true,
      hideInDescriptions: true,
    },
    {
      title: '监测站名称',
      key: 'ems_instance_name',
      dataIndex: 'ems_instance_name',
      tip: '监测站名称',
      width: 350,
      render: (_, record) => (
        <>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <Avatar
              src={<Image src={`/systemfile${record?.ems_instance_picfile?.ems_sysfile_path}`} />}
            />
            <span style={{ marginRight: 8, marginLeft: 8 }}>
              <a
                onClick={() => {
                  handleView(record.ems_instance_id, record.ems_instance_no);
                }}
              >
                {record.ems_instance_name}
              </a>
            </span>
            <Tag>{record.ems_instance_no}</Tag>
          </div>
        </>
      ),
    },
    {
      title: '开放行业',
      key: 'ems_instance_openindustryname',
      dataIndex: 'ems_instance_openindustryname',
      width: 200,
    },
    {
      title: '开放设施',
      key: 'ems_instance_openfacilityname',
      dataIndex: 'ems_instance_openfacilityname',
      width: 200,
    },
    {
      title: '开放主题',
      key: 'ems_instance_opensubjectname',
      dataIndex: 'ems_instance_opensubjectname',
      width: 200,
    },
    {
      title: '建设地址',
      width: 320,
      dataIndex: 'ems_instance_address',
      // render: (dom, record) => (
      //   <Tooltip title="浙江省杭州市临安市青山殿村">浙江省杭州市临安市青山殿村</Tooltip>
      // ),
    },
    {
      title: '建设时间',
      dataIndex: 'ems_instance_createtime',
      valueType: 'dateTime',
    },
    {
      title: '操作',
      fixed: 'right',
      dataIndex: 'option',
      valueType: 'option',
      render: (_, record) => [
        <a key={`edit-${record.ems_instance_id}`}>
          <Tooltip title="编辑">
            <EditOutlined
              onClick={() => {
                hallDrawerRef?.current?.show(record);
              }}
            />
          </Tooltip>
        </a>,
        <a key={`delete-${record.ems_instance_id}`}>
          <Tooltip title="删除">
            <Popconfirm title="是否删除该项！" okText="是" cancelText="否">
              <DeleteOutlined />
            </Popconfirm>
          </Tooltip>
        </a>,
        <a key={`opts-${record.ems_instance_id}`}>
          <Tooltip title="配置">
            <SettingOutlined
              onClick={() => {
                goToParanomicConfig(record.ems_instance_id);
              }}
            />
          </Tooltip>
        </a>,
      ],
    },
  ];

  /**
   * 请求列表
   * @param params
   * @returns
   */
  const requestList = async (params: { pageSize: number; current: number }) => {
    try {
      const queryParams = {
        ...params,
        pageNum: params.current,
        pageSize: params.pageSize,
      };
      if (queryParams.current) delete queryParams.current;
      const response = await findInstanceByMap(queryParams);
      const list = response?.result?.list || [];
      const dataSource = {
        data: list,
        success: response.code === 200 ? true : false,
        total: response?.result?.total,
      };
      return dataSource;
    } catch (error) {
      message.error('查询失败请重试！');
      return false;
    }
  };

  useImperativeHandle(ref, () => ({
    async addStation() {
      hallDrawerRef?.current?.show();
    },
    async refresh(params) {
      setParams(params);
    },
  }));

  return (
    <>
      <ProTable
        actionRef={actionRef}
        params={params}
        request={requestList}
        className="stationTable"
        size="large"
        // headerTitle="监测站列表"
        options={{ fullScreen: false, reload: false, setting: false, density: false }}
        rowKey="ems_instance_id"
        search={false}
        defaultSize="middle"
        columns={columns}
        pagination={{
          pageSize: 10,
        }}
      />
      <AddOrEditStation
        ref={hallDrawerRef}
        refreshList={() => {
          actionRef.current.reload();
        }}
      />
    </>
  );
});
export default StationList;
