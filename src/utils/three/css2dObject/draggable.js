import { Common } from '@/utils/three/xThree';
class Draggable {
  static currentDraggable = null; // 用来存储当前的可拖拽对象

  constructor(viewer, cssObject) {
    this.viewer = viewer;
    this.cssObject = cssObject;
    this.element = cssObject.element;
    this.offsetX = 0;
    this.offsetY = 0;
    this.isDragging = false;

    // 事件处理函数绑定
    this.onMouseDown = this.onMouseDown.bind(this);
    this.onTouchStart = this.onTouchStart.bind(this);
    this.onMouseMove = this.onMouseMove.bind(this);
    this.onTouchMove = this.onTouchMove.bind(this);
    this.onMouseUp = this.onMouseUp.bind(this);
    this.onTouchEnd = this.onTouchEnd.bind(this);

    // 确保每次实例化时，只有一个可拖拽对象
    if (Draggable.currentDraggable) {
      Draggable.currentDraggable.disable(); // 禁用当前拖拽对象
    }

    Draggable.currentDraggable = this; // 设置当前拖拽对象为当前实例

    // 绑定鼠标和触摸事件
    this.element.addEventListener('mousedown', this.onMouseDown);
    this.element.addEventListener('touchstart', this.onTouchStart, { passive: false });
    document.addEventListener('mousemove', this.onMouseMove);
    document.addEventListener('touchmove', this.onTouchMove, { passive: false });
    document.addEventListener('mouseup', this.onMouseUp);
    document.addEventListener('touchend', this.onTouchEnd);
  }

  // 鼠标按下事件
  onMouseDown(event) {
    event.preventDefault(); // 阻止默认事件，防止浏览器行为干扰
    this.offsetX = event.clientX - this.element.offsetLeft;
    this.offsetY = event.clientY - this.element.offsetTop;
    this.isDragging = true;
  }

  // 触摸开始事件
  onTouchStart(event) {
    event.preventDefault(); // 阻止默认事件，防止浏览器行为干扰
    this.offsetX = event.touches[0].clientX - this.element.offsetLeft;
    this.offsetY = event.touches[0].clientY - this.element.offsetTop;
    this.isDragging = true;
  }

  // 鼠标移动事件
  onMouseMove(event) {
    if (this.isDragging) {
      // 计算新的位置
      const left = event.clientX - this.offsetX;
      const top = event.clientY - this.offsetY;

      // 更新元素位置
      let position = Common.getPointByScreen(this.viewer, event.clientX, event.clientY);
      if (!position) {
        return;
      }
      this.cssObject.position.copy(position);
    }
  }

  // 触摸移动事件
  onTouchMove(event) {
    if (this.isDragging) {
      const touch = event.touches[0]; // 获取触摸的第一个手指
      let position = Common.getPointByScreen(this.viewer, touch.clientX, touch.clientY);
      if (!position) {
        return;
      }
      this.cssObject.position.copy(position);
    }
  }

  // 鼠标松开事件
  onMouseUp(event) {
    this.isDragging = false;
  }

  // 触摸结束事件
  onTouchEnd(event) {
    this.isDragging = false;
  }

  // 禁用当前的拖拽功能
  disable() {
    this.element.removeEventListener('mousedown', this.onMouseDown);
    this.element.removeEventListener('touchstart', this.onTouchStart);
    document.removeEventListener('mousemove', this.onMouseMove);
    document.removeEventListener('touchmove', this.onTouchMove);
    document.removeEventListener('mouseup', this.onMouseUp);
    document.removeEventListener('touchend', this.onTouchEnd);
  }
}

export { Draggable };
