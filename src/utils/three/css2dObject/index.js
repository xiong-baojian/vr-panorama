import * as THREE from 'three';
import { CSS2DObject } from 'three/examples/jsm/renderers/CSS2DRenderer.js';
import { Draggable } from './draggable';
import { Common } from '@/utils/three/xThree';

class Css2dObject {
  constructor(viewer) {
    this.viewer = viewer;
    this.meshs = null;
    this.draggableMeshs = [];
    this.init();
  }
  init() {
    this.meshs = new THREE.Group();
    this.meshs.name = 'cssObjectGroup';
    this.viewer.scene.add(this.meshs);
  }

  // 新添加的方法 position = {x:0,y:0,z:0,}
  add(element, { name, position, draggable = false, show = true }) {
    element.classList.add(name);
    const cssObject = new CSS2DObject(element);
    cssObject.name = name;
    cssObject.visible = show;
    cssObject.draggable = draggable;
    const x = this.viewer.container?.offsetWidth / 2;
    const y = this.viewer.container?.offsetHeight / 2;
    const point = position || Common.getPointByScreen(this.viewer, x, y);
    cssObject.position.set(point.x, point.y, point.z);
    this.meshs.add(cssObject);
    if (draggable) {
      const draggableMesh = new Draggable(this.viewer, cssObject);
      this.draggableMeshs.push(draggableMesh);
    }
    return cssObject;
  }

  select(callback) {
    this.deselect();
    let isMouseDown = false;
    let mouseDownTime = 0;
    const MIN_TIME_THRESHOLD = 100; // 设置最小时间阈值为 200 毫秒

    // 监听鼠标按下事件，记录按下时间
    const mouseDownHandler = (event) => {
      isMouseDown = true;
      mouseDownTime = new Date().getTime(); // 记录按下时间
    };

    // 监听鼠标抬起事件，计算按下和抬起的时间差
    const mouseUpHandler = (event) => {
      if (isMouseDown) {
        const mouseUpTime = new Date().getTime();
        const timeDifference = mouseUpTime - mouseDownTime;

        if (timeDifference > MIN_TIME_THRESHOLD) {
          // 如果按下和抬起的时间差小于阈值，不触发 clickMesh
          return;
        }

        // 如果时间差符合条件，触发 clickMesh
        clickMesh(event);
      }
      isMouseDown = false; // 重置鼠标按下状态
    };

    // clickMesh 处理点击事件
    const clickMesh = (event) => {
      let flag = false;

      const elements = document.elementsFromPoint(event.clientX, event.clientY);
      for (let i = 0; i < elements.length; i++) {
        const element = elements[i];
        if (element.classList.contains('lm')) {
          flag = true;
          callback(element);
        }
      }
      if (!flag) {
        callback(null);
      }
    };

    // 绑定事件
    this.viewer.container.addEventListener('mousedown', mouseDownHandler);
    this.viewer.container.addEventListener('mouseup', mouseUpHandler);
    this.mouseDownHandler = mouseDownHandler;
    this.mouseUpHandler = mouseUpHandler;
  }

  deselect() {
    if (this.mouseDownHandler || this.mouseUpHandler) {
      this.viewer.container.removeEventListener('mousedown', this.mouseDownHandler);
      this.viewer.container.removeEventListener('mouseup', this.mouseUpHandler);
      // 清空保存的事件处理函数
      this.mouseDownHandler = null;
      this.mouseUpHandler = null;
    }
  }

  findMeshElementById(id) {
    let el;
    this.meshs.children.forEach((item) => {
      const classList = item.element.classList?.value?.split(' ');
      if (classList?.find((item) => item.includes(id))) {
        el = item.element;
      }
    });
    return el;
  }

  show(key, name, visible = false) {
    this.meshs.children.forEach((item) => {
      if (item[key] == name) {
        item.visible = visible;
        if (!visible) {
          const x = this.viewer.container?.offsetWidth / 2;
          const y = this.viewer.container?.offsetHeight / 2;
          const point = Common.getPointByScreen(this.viewer, x, y);
          item.position.copy(point);
        }
      }
    });
  }

  remove(key, name) {
    for (var i = this.meshs.children.length - 1; i >= 0; i--) {
      if (this.meshs.children[i][key] == name) {
        this.meshs.remove(this.meshs.children[i]);
      }
    }
  }

  removeAll() {
    for (var i = this.meshs.children.length - 1; i >= 0; i--) {
      this.meshs.remove(this.meshs.children[i]);
    }
    // this.viewer.scene.remove(this.meshs);
  }

  showAll(visible) {
    this.meshs.children.forEach((item) => {
      item.visible = visible;
    });
  }
}
export { Css2dObject };
