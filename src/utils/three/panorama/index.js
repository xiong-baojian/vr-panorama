import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { CSS2DObject } from 'three/examples/jsm/renderers/CSS2DRenderer.js';
import { CSS3DObject } from 'three/examples/jsm/renderers/CSS3DRenderer.js';
import { DragControls } from 'three/examples/jsm/controls/DragControls.js';
import { HTMLMesh } from 'three/examples/jsm/interactive/HTMLMesh.js';
import TWEEN from '@tweenjs/tween.js';
import { GUI } from 'three/examples/jsm/libs/lil-gui.module.min.js';
import { Drag, Resize, Common } from '../xThree';
import lineImg from '@/assets/images/common/arrow-right.png';
import { LineMaterial } from 'three/examples/jsm/lines/LineMaterial.js';
import { LineGeometry } from 'three/examples/jsm/lines/LineGeometry.js';
import { Line2 } from 'three/examples/jsm/lines/Line2.js';
import { MeshLine, MeshLineMaterial } from '../xThree';
import { Smoke } from '../effects/smoke';
import { renderTiles } from './renderTiles';
import { setMaterial, setTextures, renderPanoramaImgs } from './renderPanoramaImg';
let material;
let canvasArr = [];
let textureArr = [];
let time = 0;
class Panorama {
  constructor(viewer) {
    this.viewer = viewer;
    this.panoramaGroups = null; //全景集合
    this.panoramaBox = null; // 全景box
    this.panoramaImgs = []; // 渲染全景所需照片
    this.textureArray = [];
    this.toTextureArray = [];
    this.materialArray = []; // 全景材质数组 6面
    this.materialIndexs = []; // 全景材质 6面下标 px nx py ny pz nz
    this.dragMeshs = []; // 可拖拽mesh集合
    this.dragControl = null; // 拖拽控制器
    this.loadingManager = null; // 加载器
    this.selectControl = null;
    this.resize = null;
    this.currentObject = null;
    this.raycaster = new THREE.Raycaster();

    this.minFov = 1;
    this.maxFov = 100;

    this.minRotateSpeed = -0.1;
    this.maxRotateSpeed = -0.5;

    this._init();
    this.init();
  }

  _init() {
    this.panoramaGroups = new THREE.Group();
    this.panoramaGroups.name = 'panoramaGroup';
    this.viewer.orbitControls.enablePan = false;
    this.viewer.orbitControls.maxDistance = 20;
    this.viewer.orbitControls.rotateSpeed = -0.5;
    this.viewer.scene.add(this.panoramaGroups);
    this.addMouseWheel();
    this.addTouch();
  }

  // 添加全景
  init() {
    const geometry = new THREE.BoxGeometry(50, 50, 50);
    geometry.scale(1, 1, -1);
    document.time = 0;
    this.panoramaBox = new THREE.Mesh(geometry, this.materialArray);
    this.panoramaBox.name = 'panorama';
    this.panoramaGroups.add(this.panoramaBox);
    // setMaterial(this.panoramaBox);
  }

  addMeta() {
    const video = document.createElement('video');
    video.preload = 'auto';
    video.crossOrigin = 'anonymous';
    video.src = 'meta.mp4';
    video.loop = true;
    video.muted = true;
    video.play();
    const material = new THREE.ShaderMaterial({
      uniforms: {
        videoTexture: {
          value: new THREE.VideoTexture(video),
        },
        threshold: {
          value: 0.05,
        },
      },
      depthTest: false,
      side: THREE.DoubleSide,
      transparent: true,
      vertexShader: `
  varying vec2 vUv;
	void main() {
		vUv = uv;
		gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
	}
    `,
      fragmentShader: `
   uniform sampler2D videoTexture;
	uniform float threshold;
	varying vec2 vUv;
	void main() {
		vec4 color = texture2D(videoTexture, vUv);
		float greenScreen = color.g - max(color.r, color.b);
    float blackScreen = max(color.r, max(color.g, color.b));
    
		float alpha = smoothstep(threshold - 0.05, threshold + 0.05, blackScreen);
    gl_FragColor = vec4(color.rgb, alpha);
	}
    `,
    });
    const geometry = new THREE.PlaneGeometry(16, 16);
    const contentBox = new THREE.Mesh(geometry, material);
    contentBox.position.set(-5.69, 0.51, -25);
    contentBox.rotation.y = (10 * Math.PI) / 2;
    this.panoramaGroups.add(contentBox);
  }

  addWall() {
    this.deltaTime = {
      value: 0,
    };
    this.clock = new THREE.Clock();
    const geometry = new THREE.CylinderGeometry(22, 22, 3, 4, 64, true);
    const material = new THREE.ShaderMaterial({
      side: THREE.DoubleSide,
      transparent: true,
      // depthWrite: false,
      depthTest: false,
      uniforms: {
        uTime: this.deltaTime,
      },
      vertexShader: `
                    varying vec2 vUv; 
                    void main(){
                        vUv = uv;
                        gl_Position = projectionMatrix * modelViewMatrix * vec4(position,1.0);
                    }
                    `,
      fragmentShader: `
                    uniform float uTime;
                    varying vec2 vUv;
                    #define PI 3.14159265

                    void main(){

                    vec4 baseColor = vec4(0.0,1.0,1.0,1.);
        
                    vec4 finalColor;
                        
                    float amplitude = 1.;
                    float frequency = 10.;
                    
                    float x = vUv.x;

                    float y = sin(x * frequency) ;
                    float t = 0.01*(-uTime*130.0);
                    y += sin(x*frequency*2.1 + t)*4.5;
                    y += sin(x*frequency*1.72 + t*1.121)*4.0;
                    y += sin(x*frequency*2.221 + t*0.437)*5.0;
                    y += sin(x*frequency*3.1122+ t*4.269)*2.5;
                    y *= amplitude*0.06;
                    y /= 3.;
                    y += 0.55;

                    vec4 color = gl_FragColor.rgba;

                    float r = step(0.5, fract(vUv.y - uTime));

                    baseColor.a = step(vUv.y,y) * (y-vUv.y)/y;
                    
                    gl_FragColor = baseColor;

                    }
                    `,
    });
    const cylinder = new THREE.Mesh(geometry, material);
    cylinder.position.set(-2, -12, 25);
    cylinder.rotation.y = (Math.PI / 180) * 48;
    // cylinder.rotation.x = (Math.PI / 180) * 5;
    this.panoramaGroups.add(cylinder);

    this.addAnimation(() => {
      this.deltaTime.value = this.clock.getElapsedTime();
    });
  }

  addModel() {
    // 加载模型并开启阴影和接受阴影
    const gltfLoader = new GLTFLoader();
    let url = require('@/assets/model/glb/factory_low.glb');
    gltfLoader.load(url, (gltf) => {
      // gltf.scene.rotation.y = Math.PI;
      let model = gltf.scene;
      model.traverse(function (object) {
        if (object.isMesh) {
          object.castShadow = true;
          object.receiveShadow = true;
          object.material.depthTest = true;
        }
      });
      model.scale.set(0.001, 0.001, 0.001);
      model.position.set(24, -7, 7);
      model.rotation.set(0, (90 / 180) * Math.PI, 0);

      this.panoramaGroups.add(model);
    });
  }

  addSmoke(options = {}) {
    this.smoke = new Smoke(this.viewer, options);
  }

  addEffects() {
    this.addWall();
    this.addSmoke();
  }

  addEffects_252() {
    this.addSmoke({ position: { x: 9.64, y: -11.01, z: 25 } });
  }

  addHtmlMesh(params) {
    const { element, position, rotation, width, height, name } = params;
    const mesh = new HTMLMesh(element);
    mesh.position.copy(position);
    mesh.rotation.set(rotation.x, rotation.y, rotation.z);
    mesh.scale.set(width / 7.2, height / 10.8, 1);
    mesh.material.side = THREE.DoubleSide;
    mesh.material.depthTest = false;
    this.panoramaGroups.add(mesh);
    return mesh;
    // const controls = new DragControls(
    //   [mesh],
    //   this.viewer.camera,
    //   this.viewer.css3dRenderer.domElement,
    // );
  }

  add3dObject(params) {
    let { data, position } = params;
    let { width, height, text, type } = data;
    const element = document.createElement('div');
    element.draggable = false;
    element.id = data?.id;
    element.className = 'css3dObject';

    element.style.width = (width || 30) + 'px';
    element.style.height = (height || 30) + 'px';

    // 添加可拖拽点
    const pointRight = document.createElement('span');
    pointRight.className = 'css3dObject-pointRight';
    element.appendChild(pointRight);
    // 需优化
    this.resize = new Resize(pointRight, element);

    // 添加图片框元素
    const imgbox = document.createElement('div');
    imgbox.className = 'css3dObject-imgbox';
    imgbox.draggable = false;
    element.appendChild(imgbox);
    const img = document.createElement('img');
    img.draggable = false;
    img.style.width = '100%';
    img.style.height = '100%';
    img.src = data?.url;
    imgbox.appendChild(img);
    if (data && data.imgType == 'FrameAnimation') {
      img.className = 'frame-animation';
    }

    // 添加数据展示
    const dataBox = document.createElement('div');
    dataBox.classList.add('data-box');
    element.appendChild(dataBox);

    // 添加文字框元素
    const textBox = document.createElement('div');
    textBox.classList.add('text-box');
    element.appendChild(textBox);
    textBox.innerText = text || '';

    // 添加iframe
    if (type == 'iframe') {
      const iframeBox = document.createElement('iframe');
      iframeBox.classList.add('iframe-box');
      element.appendChild(iframeBox);
    }
    // 添加video
    if (type == 'video') {
      const videoBox = document.createElement('video');
      videoBox.classList.add('video-box');
      videoBox.setAttribute('x5-video-player-type', 'h5-page');
      videoBox.setAttribute('webkit-playsinline', 'true');
      videoBox.setAttribute('playsinline', 'true');
      element.appendChild(videoBox);
    }
    // 添加cover
    if (type == 'image') {
      const cover = document.createElement('img');
      cover.draggable = false;
      cover.classList.add('cover-box');
      element.appendChild(cover);
    }

    // threejs 添加3dobject
    const object = new CSS3DObject(element);
    object.data = data;
    object.scale.set(0.05, 0.05, 0.05);
    object.position.copy(position);
    object.rotation.copy(this.viewer.camera.rotation);
    this.panoramaGroups.add(object);
    this.dragMeshs.push(object);
    this.currentObject = object;
    return object;
  }

  addElement(params) {
    const { element, position, rotation, name } = params;
    const object = new CSS3DObject(element);
    object.scale.set(0.03, 0.03, 0.03);
    object.position.copy(position);
    object.rotation.set(rotation._x, rotation._y, rotation._z);
    object.name = name;
    this.panoramaGroups.add(object);
    return object;
  }

  addScaleAnimation(object) {
    // 定义缩放动画
    let scaleDirection = 1; // 用于控制缩放方向的标志，1表示放大，-1表示缩小
    const animation = () => {
      const scaleSpeed = 0.0001; // 缩放速度
      if (object.scale.x > 0.032) scaleDirection = -1; // 如果对象太大，则开始缩小
      else if (object.scale.x < 0.028) scaleDirection = 1; // 如果对象太小，则开始放大
      // 根据缩放方向调整缩放比例
      object.scale.x += scaleSpeed * scaleDirection;
      object.scale.y += scaleSpeed * scaleDirection;
      object.scale.z += scaleSpeed * scaleDirection;
    };

    let animaObject = {
      fun: animation,
      content: this,
    };
    this.viewer.addAnimate(animaObject);
  }

  removeObejctByName(name) {
    for (var i = this.panoramaGroups.children.length - 1; i >= 0; i--) {
      if (this.panoramaGroups.children[i].name === name) {
        this.panoramaGroups.remove(this.panoramaGroups.children[i]);
      }
    }
  }

  initDragControl(callback) {
    if (this.dragControl) this.dragControl.dispose();
    this.dragControl = new Drag(this.dragMeshs, this.viewer, this.viewer.css3dRenderer.domElement);
    this.dragControl.addEventListener('drag', (event) => {
      this.currentObject = event.object;
      callback(this.currentObject);
    });
    this.dragControl.addEventListener('dragstart', (event) => {
      this.currentObject = event.object;
      callback(this.currentObject);
      this.viewer.orbitControls.enableRotate = false;
    });
    this.dragControl.addEventListener('dragend', (event) => {
      this.currentObject = event.object;
      this.viewer.orbitControls.enableRotate = true;
    });
  }

  addTouch() {
    this.viewer.orbitControls.enableZoom = false;
    var scaleRatio = 0;
    var start = 0;
    document.addEventListener(
      'touchstart',
      (event) => {
        if (event.touches.length == 2) {
          this.viewer.orbitControls.enabled = false;
          var first = event.touches[0];
          var second = event.touches[1];
          var distance = Math.sqrt(
            Math.pow(first.clientX - second.clientX, 2) +
              Math.pow(first.clientY - second.clientY, 2),
          );
          start = distance / 100;
        }
      },
      false,
    );
    document.addEventListener(
      'touchmove',
      (event) => {
        if (event.touches.length == 2) {
          var first = event.touches[0];
          var second = event.touches[1];
          var distance = Math.sqrt(
            Math.pow(first.clientX - second.clientX, 2) +
              Math.pow(first.clientY - second.clientY, 2),
          );
          scaleRatio = distance / 100;
          let y = 1;
          if (scaleRatio > start) {
            y = -1;
          } else {
            y = 1;
          }
          this.viewer.orbitControls.enabled = false;
          const camera = this.viewer.camera;
          const fov = camera.fov + y;
          camera.fov = THREE.MathUtils.clamp(fov, 10, 120);
          camera.updateProjectionMatrix();

          const speed = this.viewer.orbitControls.rotateSpeed - y * 0.05;
          this.viewer.orbitControls.rotateSpeed = THREE.MathUtils.clamp(
            speed,
            this.maxRotateSpeed,
            this.minRotateSpeed,
          );
        } else {
          this.viewer.orbitControls.enabled = true;
        }
      },
      false,
    );
    document.addEventListener(
      'touchend',
      (event) => {
        if (event.touches.length == 2) {
          var first = event.touches[0];
          var second = event.touches[1];
          var distance = Math.sqrt(
            Math.pow(first.clientX - second.clientX, 2) +
              Math.pow(first.clientY - second.clientY, 2),
          );
          scaleRatio = distance / 100;
          this.viewer.orbitControls.enabled = true;
        }
      },
      false,
    );
    // 缩放比例
  }

  /**
   * 选中css3Dobject实体事件
   * @param {*} callback
   */
  handleClickSelected(callback) {
    const selectControl = (event) => {
      const elements = document.elementsFromPoint(event.clientX, event.clientY);
      for (let i = 0; i < elements.length; i++) {
        const element = elements[i];
        if (
          element.classList.contains('css3dObject-pointRight') ||
          element.classList.contains('alarm-popup')
        ) {
          return;
        }
        if (element.classList.contains('css3dObject')) {
          const _selected = this.dragMeshs.find((item) => {
            return item.data.id == element.id;
          });
          this.currentObject = _selected;
          callback(this.currentObject);
          return;
        }
      }
    };
    let _domElement = this.viewer.css3dRenderer.domElement;
    this.selectControl = selectControl;
    _domElement.addEventListener('click', selectControl);
  }

  cancelClickSelected() {
    let _domElement = this.viewer.css3dRenderer.domElement;
    _domElement.removeEventListener('click', this.selectControl);
  }

  disposeDragControl() {
    if (this.dragControl) this.dragControl.dispose();
  }

  getCurrentObject() {
    return this.currentObject;
  }

  // 相机控制器参数
  changeCameraParams(params) {
    let { fovs, azimuthAngle, polarAngle } = params;
    this.minFov = fovs[0];
    this.maxFov = fovs[1];
    // this.viewer.camera.fov = this.maxFov;
    const controls = this.viewer.orbitControls;
    // 设置最大水平和垂直角度
    if (azimuthAngle[0] == -180 && azimuthAngle[1] == 180) {
      this.viewer.orbitControls.minAzimuthAngle = -Infinity;
      this.viewer.orbitControls.maxAzimuthAngle = Infinity;
    } else {
      controls.maxAzimuthAngle = azimuthAngle[1] * (Math.PI / 180);
      controls.minAzimuthAngle = azimuthAngle[0] * (Math.PI / 180);
    }
    controls.maxPolarAngle = (polarAngle[1] + 90) * (Math.PI / 180);
    controls.minPolarAngle = (polarAngle[0] + 90) * (Math.PI / 180);
  }

  // 滚轮移动事件 需优化
  addMouseWheel() {
    this.viewer.orbitControls.enableZoom = false;
    this.viewer.container.addEventListener('wheel', (event) => {
      event.preventDefault();
      const camera = this.viewer.camera;
      const fov = camera.fov + event.deltaY * 0.02;
      camera.fov = THREE.MathUtils.clamp(fov, this.minFov, this.maxFov);
      camera.updateProjectionMatrix();

      const speed = this.viewer.orbitControls.rotateSpeed - event.deltaY * 0.0005;

      this.viewer.orbitControls.rotateSpeed = THREE.MathUtils.clamp(
        speed,
        this.maxRotateSpeed,
        this.minRotateSpeed,
      );
    });
    this.viewer.orbitControls.addEventListener('end', () => {
      const camera = this.viewer.camera;
      const fov = camera.fov;
      if (fov < 30) {
      }
    });
  }

  // 三点获取当前视角的面
  getFaceMaterialIndex() {
    const getIndex = (point) => {
      this.raycaster.setFromCamera(point, this.viewer.camera);
      const intersects = this.raycaster.intersectObject(this.panoramaBox, true);
      if (intersects.length > 0) {
        const res = intersects.filter(function (res) {
          return res && res.object;
        })[0];
        if (res && res.object) {
          return res.face.materialIndex;
        }
      }
    };
    let index1 = getIndex(new THREE.Vector2(-1, 0));
    let index2 = getIndex(new THREE.Vector2(0, 0));
    let index3 = getIndex(new THREE.Vector2(1, 0));
    return Array.from(new Set([index1, index2, index3]));
  }

  // 全景初始视角
  setCameraView(position) {
    if (position && position.x && position.y && position.z) {
      this.viewer.camera.position.set(position.x, position.y, position.z);
    } else {
      this.viewer.camera.position.set(0, 0, 1);
    }
  }

  /**
   * 清除资源
   */
  cleanResource() {
    if (textureArr && textureArr.length > 0) {
      textureArr.forEach((texture) => {
        if (texture) texture.dispose();
      });
      textureArr = [];
    }
    // if (canvasArr && canvasArr.length) {
    //   const arr = canvasArr.flat();
    //   arr.forEach((canvas) => {
    //     if (canvas) {
    //       const ctx = canvas.getContext('2d');
    //       ctx.clearRect(0, 0, canvas.width, canvas.height);
    //       canvas.width = 0;
    //     }
    //   });
    //   canvasArr = [];
    // }

    if (canvasArr && canvasArr.length > 1) {
      canvasArr[0].forEach((canvas) => {
        if (canvas) {
          const ctx = canvas.getContext('2d');
          ctx.clearRect(0, 0, canvas.width, canvas.height);
          canvas.width = 0;
        }
      });
      canvasArr.shift();
    }
  }

  /**
   * 全景图设置
   * @param {*} panoramaImgs
   * @returns
   */
  setPanoramaImg(panoramaImgs, cameraPosition) {
    if (!panoramaImgs) {
      return;
    }
    const previewImg = panoramaImgs.preview;
    const tileUrls = panoramaImgs.tileUrls;
    this.cleanResource();
    for (let i = 0; i < 6; i++) {
      const texture = new THREE.TextureLoader().load();
      textureArr.push(texture);
    }
    return new Promise((resolve) => {
      new THREE.ImageLoader().load(previewImg, (image) => {
        let canvas, context;
        const tileWidth = 1800;
        const canvass = [];
        for (let i = 0; i < textureArr.length; i++) {
          canvas = document.createElement('canvas');
          context = canvas.getContext('2d');
          canvas.height = tileWidth;
          canvas.width = tileWidth;
          const width = image.width;
          context.drawImage(image, 0, width * i, width, width, 0, 0, tileWidth, tileWidth);
          textureArr[i].image = canvas;
          textureArr[i].needsUpdate = true;
          canvass.push(canvas);
        }

        canvasArr.push(canvass);
        const animationFunc = () => {
          for (let i = 0; i < textureArr.length; i++) {
            textureArr[i].needsUpdate = true;
          }
        };
        this.addAnimation(animationFunc);

        this.setPanoramaMaterial_noblending(textureArr);
        resolve('预览图加载完成');
        // 需渲染图片结束后再调整视角
        this.setCameraView(cameraPosition);
        renderTiles(tileUrls, canvass);
      });
    });
  }

  setMaterial(textureArray) {
    const materials = [];
    for (let i = 0; i < 6; i++) {
      textureArray[i].wrapS = THREE.ClampToEdgeWrapping;
      textureArray[i].wrapT = THREE.ClampToEdgeWrapping;
      textureArray[i].colorSpace = THREE.SRGBColorSpace;
      materials.push(new THREE.MeshBasicMaterial({ map: textureArray[i] }));
    }
    this.panoramaBox.material = materials;
  }

  createWebWorker(tileUrls, textureArr) {
    const worker = new Worker('/js/renderTilesWorker.js');
    const _this = this;
    worker.postMessage({
      canvasArr: canvasArr,
    });
    // worker.postMessage({
    //   viewer: this.viewer,
    //   panoramaBox: this.panoramaBox,
    //   tileUrls: tileUrls,
    //   canvasArr: canvasArr,
    // });

    // // 设置监听消息接收
    // worker.onmessage = function (e) {
    //   const message = e.data;
    //   console.log(message); // 打印 Worker 完成的结果
    // };

    // // 错误处理
    // worker.onerror = function (error) {
    //   console.error('Worker 错误:', error);
    // };
  }

  // no blending
  setPanoramaMaterial_noblending(textureArray) {
    const materialArray = [];
    for (let i = 0; i < 6; i++) {
      // textureArray[i].wrapS = THREE.ClampToEdgeWrapping;
      // textureArray[i].wrapT = THREE.ClampToEdgeWrapping;

      // textureArray[i].wrapS = THREE.RepeatWrapping;
      // textureArray[i].wrapT = THREE.RepeatWrapping;

      // textureArray[i].minFilter = THREE.NearestFilter; // 最近邻过滤（缩小时）
      // textureArray[i].magFilter = THREE.NearestFilter; // 最近邻过滤（放大时）
      materialArray.push(
        new THREE.ShaderMaterial({
          uniforms: {
            texture1: { value: textureArray[i] },
          },
          vertexShader: `
          varying vec2 vUv;
          void main(){
              vUv = uv;
              gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4( position, 1.0 );
          }
        `,
          fragmentShader: `
          uniform sampler2D texture1;
          uniform float opacity;
          uniform vec3 color;
          varying vec2 vUv;
          void main() {
              vec4 texColor = texture2D(texture1, vUv);
              gl_FragColor = texColor;
          }
        `,
          side: THREE.DoubleSide,
        }),
      );
    }

    this.panoramaBox.material = materialArray;
  }

  // white flash
  setPanoramaMaterial_whiteflash(textureArray, halfWayCallback, resolve) {
    const tweenOut = new TWEEN.Tween({ opacity: 1 })
      .to({ opacity: 0.5 }, 200)
      .onUpdate((e) => {
        const opacity = e.opacity;
        for (let i = 0; i < 6; i++) {
          const material = this.panoramaBox.material[i];
          if (material) {
            material.uniforms.opacity.value = opacity;
          }
        }
        this.viewer.renderer.render(this.viewer.scene, this.viewer.camera);
      })
      .easing(TWEEN.Easing.Linear.None);

    const tweenIn = new TWEEN.Tween({ opacity: 0.5 })
      .to({ opacity: 1 }, 300)
      .onUpdate((e) => {
        const opacity = e.opacity;
        for (let i = 0; i < 6; i++) {
          const material = this.panoramaBox.material[i];
          if (material) {
            material.uniforms.opacity.value = opacity;
          }
        }
        this.viewer.renderer.render(this.viewer.scene, this.viewer.camera);
      })
      .easing(TWEEN.Easing.Linear.None);

    tweenOut.onComplete(() => {
      const materialArray = [];
      for (let i = 0; i < 6; i++) {
        materialArray.push(
          new THREE.ShaderMaterial({
            uniforms: {
              texture1: { value: textureArray[i] },
              opacity: { value: 0.5 },
            },
            vertexShader: `
            varying vec2 vUv;
            void main(){
                vUv = uv;
                gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4( position, 1.0 );
            }
          `,
            fragmentShader: `
            uniform sampler2D texture1;
            uniform float opacity;
            uniform vec3 color;
            varying vec2 vUv;
            void main() {
                vec4 texColor = texture2D(texture1, vUv);
                texColor.a = opacity;
                gl_FragColor = texColor;
            }
          `,
            side: THREE.DoubleSide,
          }),
        );
      }
      this.panoramaBox.material = materialArray;
      tweenIn.start();
      halfWayCallback(1);
    });
    tweenIn.onComplete(() => {
      resolve(1);
    });
    // 启动渐出动画
    tweenOut.start();
  }

  // black out
  setPanoramaMaterial_blackout(textureArray, halfWayCallback, resolve) {
    const tweenOut = new TWEEN.Tween({ opacity: 0 })
      .to({ opacity: 1 }, 300)
      .onUpdate((e) => {
        const opacity = e.opacity;
        for (let i = 0; i < 6; i++) {
          const material = this.panoramaBox.material[i];
          if (material) {
            material.uniforms.opacity.value = opacity;
          }
        }
        this.viewer.renderer.render(this.viewer.scene, this.viewer.camera);
      })
      .easing(TWEEN.Easing.Linear.None);

    const tweenIn = new TWEEN.Tween({ opacity: 1 })
      .to({ opacity: 0 }, 800)
      .onUpdate((e) => {
        const opacity = e.opacity;
        for (let i = 0; i < 6; i++) {
          const material = this.panoramaBox.material[i];
          if (material) {
            material.uniforms.opacity.value = opacity;
          }
        }
        this.viewer.renderer.render(this.viewer.scene, this.viewer.camera);
      })
      .easing(TWEEN.Easing.Linear.None);

    tweenOut.onComplete(() => {
      const materialArray = [];
      for (let i = 0; i < 6; i++) {
        materialArray.push(
          new THREE.ShaderMaterial({
            uniforms: {
              texture1: { value: textureArray[i] },
              opacity: { value: 1 },
              color: { value: new THREE.Color(0, 0, 0) },
            },
            vertexShader: `
            varying vec2 vUv;
            void main(){
                vUv = uv;
                gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4( position, 1.0 );
            }
          `,
            fragmentShader: `
            uniform sampler2D texture1;
            uniform float opacity;
            uniform vec3 color;
            varying vec2 vUv;
            void main() {
                vec4 texColor = texture2D(texture1, vUv);
                texColor.rgb = mix(texColor.rgb, color, opacity);
                gl_FragColor = texColor;
            }
          `,
            side: THREE.DoubleSide,
          }),
        );
      }
      this.panoramaBox.material = materialArray;
      tweenIn.start();
      halfWayCallback(1);
    });
    tweenIn.onComplete(() => {
      resolve(1);
    });
    // 启动渐出动画
    tweenOut.start();
  }

  getLoadingManager() {
    return this.loadingManager;
  }

  // 移除全景
  dispose() {
    for (var i = this.panoramaGroups.children.length - 1; i >= 0; i--) {
      this.panoramaGroups.remove(this.panoramaGroups.children[i]);
    }
    this.dragMeshs.splice(0, this.dragMeshs.length);
    this.materialArray = [];
  }

  // 移除全景内所有实体
  removeAll() {
    for (var i = this.panoramaGroups.children.length - 1; i >= 0; i--) {
      if (this.panoramaGroups.children[i].name !== 'panorama') {
        this.panoramaGroups.remove(this.panoramaGroups.children[i]);
      }
    }
    this.dragMeshs.splice(0, this.dragMeshs.length);
    // 移除动画
    this.viewer.animateEventList = [];
  }

  /**
   * 添加球体全景盒子
   * @param {*} panoramaImg
   */
  addSphere(panoramaImg) {
    const geometry = new THREE.SphereGeometry(100, 100, 50);
    const texture = new THREE.TextureLoader().load(panoramaImg);
    let material = new THREE.ShaderMaterial({
      wireframe: false,
      side: THREE.DoubleSide,
      map: texture,
      uniforms: {
        tex_0: new THREE.Uniform(texture),
      },
      vertexShader: `
      precision highp float;
      varying vec2 v_uv;
      void main() {
        gl_Position = projectionMatrix * modelViewMatrix * vec4(position.xyz, 1.0);
        v_uv = uv;
      }
      `,
      fragmentShader: `
      precision highp float;
      varying vec2 v_uv;
      uniform sampler2D tex_0;
      void main() {
        vec4 texColor = texture2D(tex_0, vec2(1. - v_uv.x, v_uv.y));
        gl_FragColor = texColor;
      }
      `,
    });
    const mesh = new THREE.Mesh(geometry, material);
    mesh.rotation.y = Math.PI / 2;
    this.viewer.scene.add(mesh);
  }

  // 更新材质
  updateMaterial(materialIndex) {
    const uniforms = {};
    const loadingManager = new THREE.LoadingManager(() => {
      this.materialArray[materialIndex] = material;
      this.panoramaBox.materials = this.materialArray;
    });
    let nameMap = new Map([
      [0, 'px'],
      [1, 'nx'],
      [2, 'py'],
      [3, 'ny'],
      [4, 'pz'],
      [5, 'nz'],
    ]);
    for (var i = 1; i < 5; i++) {
      let name = nameMap.get(materialIndex) + '-' + i;
      uniforms[`texture${i}`] = {
        value: new THREE.TextureLoader(loadingManager).load(this.panoramaImgs.get(name)),
      };
    }
    const material = new THREE.ShaderMaterial({
      uniforms: uniforms,
      vertexShader: `
      varying vec2 vUv;
      uniform float offsetX;
      uniform float offsetY;
      uniform float offsetZ;
      void main(){
          vUv = uv;
          gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4( position.x, position.y, position.z, 1.0 );
      }
      `,
      fragmentShader: `
      uniform sampler2D texture1;
      uniform sampler2D texture2;
      uniform sampler2D texture3;
      uniform sampler2D texture4;
      varying vec2 vUv;
      vec2 offset1 = vec2(0.0 , -1.0);
      vec2 offset2 = vec2(-1.0 , -1.0);
      vec2 offset3 = vec2(0.0 , 0.0);
      vec2 offset4 = vec2(-1.0 , 0.0);
      void main() {
        if(vUv.x <= 0.5 && vUv.y >= 0.5  ){
          gl_FragColor = texture2D( texture1, vUv * 2.0  + offset1);
        }else if(vUv.x > 0.5 && vUv.y >= 0.5  ){
          gl_FragColor = texture2D( texture2, vUv * 2.0 + offset2 );
        }else if(vUv.x < 0.5 && vUv.y < 0.5  ){
          gl_FragColor = texture2D( texture3, vUv * 2.0 + offset3 );
        }else if(vUv.x >= 0.5 && vUv.y < 0.5  ){
          gl_FragColor = texture2D( texture4, vUv * 2.0 + offset4  );
        }
       }

      `,
      side: THREE.DoubleSide,
    });
  }

  /**
   *
   * @param {*} params
   */
  addImg(params) {
    let { width, height, position, rotation, name, url } = params;
    const geometry = new THREE.PlaneGeometry(4, 3);
    var texture = new THREE.TextureLoader().load(url);
    const plane = new THREE.Mesh(
      geometry,
      new THREE.MeshBasicMaterial({
        depthTest: false,
        side: THREE.DoubleSide, // 双面显示
        transparent: true,
        color: 0xffffff,
        map: texture,
      }),
    );
    plane.center = new THREE.Vector2(0.5, 0.5);
    plane.scale.set(width, height, 1);
    plane.position.set(position.x, position.y, position.z);
    plane.rotation.set(
      this.viewer.camera.rotation.x,
      this.viewer.camera.rotation.y,
      this.viewer.camera.rotation.z,
    );
    this.panoramaGroups.add(plane);
    this.dragMeshs.push(plane);
  }

  /**
   * 添加动画
   */
  addAnimation(animation) {
    this.animaObject = {
      fun: animation,
      content: this,
    };
    this.viewer.addAnimate(this.animaObject);
  }

  /**
   * 移除动画
   */
  removeAnimation(animation) {
    this.animaObject = {
      fun: animation,
      content: this,
    };
    this.viewer.removeAnimate(this.animaObject);
  }
}
export { Panorama };
