import * as THREE from 'three';
let canvasTextures = [];
let canvasArr = [];
let loadedFaces = [];
let raycaster = new THREE.Raycaster();

export async function renderTiles(viewer, panoramaBox, tileUrls, canvasArr) {
  clean();
  // 优先加载可视范围内的图片。(未完善)
  const indexs = getFaceMaterialIndex(viewer, panoramaBox);
  if (indexs && indexs.length > 0) {
    const renderPromises = indexs.map(async (item) => {
      await renderFaceTiles(panoramaBox, item, canvasArr[item], tileUrls[item]);
    });
    await Promise.all(renderPromises);
  }
  tileUrls?.forEach((item, index) => {
    if (!indexs.includes(index)) {
      renderFaceTiles(panoramaBox, index, canvasArr[index], item);
    }
  });
}

/**
 * 渲染每面
 * @param {*} panorama 全景载体
 * @param {*} index 每个面的index
 * @param {*} blurredImage 每面初始渲染图片
 * @param {*} imageLeafParts 16张图片
 */
export async function renderFaceTiles(panorama, index, canvas, imageLeafParts) {
  async function loadImagePart(url) {
    return new Promise((resolve, reject) => {
      const image = new Image();
      image.src = url;
      image.onload = () => {
        resolve(image);
      };
      image.onerror = () => {
        reject(`图片加载失败: ${url}`);
      };
    });
  }
  async function loadImages() {
    for (let i = 0; i < 4; i++) {
      const x = (i % 2) * (width / 2);
      const y = Math.floor(i / 2) * (height / 2);
      await loadLeafImages(x, y, i);
    }
  }
  async function loadLeafImages(x, y, baseImageIndex) {
    const leafWidth = width / 4;
    const leafHeight = height / 4;
    const startLeafIndex = baseImageIndex * 4;

    for (let i = 0; i < 4; i++) {
      try {
        const leafImage = await loadImagePart(imageLeafParts[startLeafIndex + i]);
        const leafX = x + (i % 2) * leafWidth;
        const leafY = y + Math.floor(i / 2) * leafHeight;
        const ctx = canvas.getContext('2d');
        ctx.drawImage(leafImage, leafX, leafY, leafWidth, leafHeight);
        // const canvasTexture = new THREE.CanvasTexture(canvas);
        // panorama.material[index].uniforms.texture1.value = canvasTexture;
        // canvasTextures.push(canvasTexture);
      } catch (error) {
        console.error(error);
      }
    }

    // const leafPromises = [];
    // for (let i = 0; i < 4; i++) {
    //   leafPromises.push(
    //     loadImagePart(imageLeafParts[startLeafIndex + i])
    //       .then((leafImage) => {
    //         const leafX = x + (i % 2) * leafWidth;
    //         const leafY = y + Math.floor(i / 2) * leafHeight;
    //         ctx.drawImage(leafImage, leafX, leafY, leafWidth, leafHeight);
    //       })
    //       .catch((error) => {
    //         console.error(error);
    //       }),
    //   );
    // }
    // // 等待所有图片加载完成
    // await Promise.all(leafPromises);
    // // 影响control操作
    const canvasTexture = new THREE.CanvasTexture(canvas);
    canvasTexture.needsUpdate = true;
    panorama.material[index].uniforms.texture1.value = canvasTexture;
    canvasTextures.push(canvasTexture);
  }

  canvasArr.push(canvas);
  const width = 3600;
  const height = 3600;
  await loadImages();
}

/**
 * 清除方法 （防止内存泄漏丢失上下文）
 */
export function clean() {
  if (canvasTextures && canvasTextures.length > 0) {
    canvasTextures.forEach((texture) => {
      texture.dispose();
    });
    canvasTextures = [];
  }

  // if (canvasArr && canvasArr.length > 0) {
  //   canvasArr.forEach((canvas) => {
  //     const ctx = canvas.getContext('2d');
  //     ctx.clearRect(0, 0, canvas.width, canvas.height);
  //     canvas.width = 0;
  //   });
  //   canvasArr = [];
  // }
}

/**
 * 获取当前画面与cube 6面的交点
 * @param {*} viewer
 * @param {*} panoramaBox
 * @returns
 */
export function getFaceMaterialIndex(viewer, panoramaBox) {
  viewer.camera.updateMatrixWorld();
  viewer.camera.updateProjectionMatrix();
  viewer.renderer.render(viewer.scene, viewer.camera);
  const getIndex = (point) => {
    raycaster.setFromCamera(point, viewer.camera);
    const intersects = raycaster.intersectObject(panoramaBox, true);
    if (intersects.length > 0) {
      const res = intersects.filter(function (res) {
        return res && res.object;
      })[0];
      if (res && res.object) {
        return res.face.materialIndex;
      }
    }
  };
  let index0 = getIndex(new THREE.Vector2(-1, 1));
  let index1 = getIndex(new THREE.Vector2(1, 1));
  let index2 = getIndex(new THREE.Vector2(1, -1));
  let index3 = getIndex(new THREE.Vector2(-1, -1));
  let index4 = getIndex(new THREE.Vector2(0, 0));
  return Array.from(new Set([index0, index1, index2, index3, index4]));
}
