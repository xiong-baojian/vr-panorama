import * as THREE from 'three';
import { MeshLine, MeshLineMaterial, Common } from '../xThree';
class Draw {
  constructor(viewer) {
    this.viewer = viewer;
    this.camera = viewer.camera;
    this.scene = viewer.scene;
    this.renderer = viewer.renderer;
    this.orbitControls = this.viewer.orbitControls;

    this.splineArray = [];
    this.line = null;
    this.group = new THREE.Group();
    this.init();
  }

  init() {
    const existingGroup = this.scene.getObjectByName('shape');
    if (existingGroup) {
      this.scene.remove(existingGroup);
      existingGroup.clear();
    }
    this.group.name = 'shape';
    this.scene.add(this.group);
  }

  start() {
    this.clear();
    this.deleteLine();
    const line = new MeshLine();
    const positions = [];
    line.setPoints(positions);
    const material = new MeshLineMaterial({
      useMap: 0,
      resolution: new THREE.Vector2(
        this.viewer.container.offsetWidth,
        this.viewer.container.offsetHeight,
      ),
      lineWidth: 5,
      sizeAttenuation: 0,
      color: '#ffffff',
      opacity: 1,
      transparent: true,
      // dashOffset: 0,
      // repeat: new THREE.Vector2(140, 1), /
      // dashArray: 0.1,
      // dashRatio: 0.5,
      // map: texture,
      side: THREE.DoubleSide,
      depthTest: false,
    });

    this.line = line;
    this.lineMaterial = material;

    const meshLine = new THREE.Mesh(line.geometry, material);
    meshLine.name = 'activeLine';
    this.meshLine = meshLine;
    this.group.add(meshLine);

    // 面
    const points = [];
    const faceGeometry = new THREE.BufferGeometry().setFromPoints(points);
    const indices = [0, 1, 2, 0, 2, 3];
    faceGeometry.setIndex(indices);
    const transparentMaterial = new THREE.MeshBasicMaterial({
      color: 0xffffff,
      opacity: 0.5,
      transparent: true,
      side: THREE.DoubleSide,
      depthTest: false,
    });
    const meshFace = new THREE.Mesh(faceGeometry, transparentMaterial);
    meshFace.name = 'activeFace';
    this.meshFace = meshFace;
    this.meshFace.visible = false;
    this.group.add(meshFace);
    this.downEvent = this.onPointerDown.bind(this);
    this.upEvent = this.onPointerUp.bind(this);
    this.moveEvent = this.onPointerMove.bind(this);
    this.dbEvent = this.clear.bind(this);
    this.viewer.container.addEventListener('pointerdown', this.downEvent, false);
    this.viewer.container.addEventListener('pointerup', this.upEvent, false);
    this.viewer.container.addEventListener('pointermove', this.moveEvent, false);
    this.viewer.container.addEventListener('dblclick', this.dbEvent, false);
  }

  onPointerDown(event) {
    const Sx = event.offsetX;
    const Sy = event.offsetY;
    const point = Common.getPointByScreen(this.viewer, Sx, Sy);
    const position = {
      x: parseFloat(point.x.toFixed(2)),
      y: parseFloat(point.y.toFixed(2)),
      z: parseFloat(point.z.toFixed(2)),
    };
    if (this.splineArray.length === 0) {
      this.splineArray.push(position);
    }
    this.splineArray.push(position);
    this.update();
  }

  onPointerMove(event) {
    if (this.splineArray.length === 0) {
      return;
    }
    const Sx = event.offsetX;
    const Sy = event.offsetY;
    const point = Common.getPointByScreen(this.viewer, Sx, Sy);
    const position = {
      x: parseFloat(point.x.toFixed(2)),
      y: parseFloat(point.y.toFixed(2)),
      z: parseFloat(point.z.toFixed(2)),
    };
    this.splineArray.pop();
    this.splineArray.push(position);
    this.update();
  }

  onPointerUp(event) {}

  update() {
    const positions = [];
    const points = [];
    for (let j = 0; j < this.splineArray.length; j++) {
      positions.push(this.splineArray[j].x, this.splineArray[j].y, this.splineArray[j].z);
      points.push(this.splineArray[j]);
    }
    this.line.setPoints(positions);
    this.line.originalPositions = positions;

    if (this.meshFace && points.length > 2) {
      const geometry = new THREE.BufferGeometry().setFromPoints(points);
      const indices = [];
      for (let i = 0; i < points.length - 2; i++) {
        indices.push(0, i + 1, i + 2); // 从第一个点开始，形成三角形索引
      }

      geometry.setIndex(indices);
      this.meshFace.geometry.dispose();
      this.meshFace.geometry = geometry;
    }
  }

  clear() {
    if (this.splineArray.length >= 2) {
      this.splineArray.pop();
      this.splineArray.pop();
      this.update();
    }
    if (this.downEvent) {
      this.viewer.container.removeEventListener('pointerdown', this.downEvent, false);
    }
    if (this.upEvent) {
      this.viewer.container.removeEventListener('pointerup', this.upEvent, false);
    }
    if (this.moveEvent) {
      this.viewer.container.removeEventListener('pointermove', this.moveEvent, false);
    }
    if (this.dbEvent) {
      this.viewer.container.removeEventListener('dblclick', this.dbEvent, false);
    }
  }

  getLinePoints(name, type = 'add') {
    const points = [];
    if (type === 'add') {
      if (!this.meshLine) {
        return;
      }
      this.meshLine.name = 'meshLine' + name;
      this.meshFace.name = 'meshFace' + name;
      this.splineArray = [];
      const originalPositions = this.meshLine.geometry.originalPositions;
      for (let i = 0; i < originalPositions.length; i += 3) {
        const x = originalPositions[i];
        const y = originalPositions[i + 1];
        const z = originalPositions[i + 2];
        points.push(new THREE.Vector3(x, y, z));
      }
      this.deleteLine(); // 清空activeLine
    } else {
      const line = this.group.getObjectByName('meshLine' + name);
      const originalPositions = line.geometry.originalPositions;
      for (let i = 0; i < originalPositions.length; i += 3) {
        const x = originalPositions[i];
        const y = originalPositions[i + 1];
        const z = originalPositions[i + 2];
        points.push(new THREE.Vector3(x, y, z));
      }
    }
    return points;
  }

  deleteLine(id) {
    const line = this.group.getObjectByName('activeLine');
    const face = this.group.getObjectByName('activeFace');
    if (line) {
      this.group.remove(line);
    }
    if (face) {
      this.group.remove(face);
    }
    if (id) {
      const line = this.group.getObjectByName('meshLine' + id);
      const face = this.group.getObjectByName('meshFace' + id);
      if (line) {
        this.group.remove(line);
      }
      if (face) {
        this.group.remove(face);
      }
    }
    this.line = null;
    this.splineArray = [];
    this.meshLine = null;
    this.meshFace = null;
  }

  /**
   * 绘制点
   * @param {*} position
   * @returns
   */
  createPoint(position) {
    const geometry = new THREE.SphereGeometry(0.5, 32, 32);
    const material = new THREE.MeshBasicMaterial({
      color: 0xffffff,
      side: THREE.DoubleSide,
      depthTest: false,
    });
    const point = new THREE.Mesh(geometry, material);
    point.position.set(position.x, position.y, position.z);
    this.scene.add(point); // 将圆形添加到场景中
    return point;
  }

  /**
   * 绘制线
   * @param {*} points
   */
  creatLine(points, id) {
    const positions = [];
    for (let i = 0; i < points.length; i++) {
      positions.push(points[i].x, points[i].y, points[i].z);
    }
    const line = new MeshLine();
    line.setPoints(positions);
    line.originalPositions = positions;
    const material = new MeshLineMaterial({
      useMap: 0,
      resolution: new THREE.Vector2(
        this.viewer.container.offsetWidth,
        this.viewer.container.offsetHeight,
      ),
      lineWidth: 5,
      sizeAttenuation: 0,
      color: '#ffffff',
      opacity: 1,
      transparent: true,
      side: THREE.DoubleSide,
      depthTest: false,
    });
    const meshLine = new THREE.Mesh(line.geometry, material);
    meshLine.name = 'meshLine' + id;
    this.group.add(meshLine);

    // 面

    const pointsArr = [];
    const faceGeometry = new THREE.BufferGeometry().setFromPoints(pointsArr);
    const indices = [0, 1, 2, 0, 2, 3];
    faceGeometry.setIndex(indices);
    const transparentMaterial = new THREE.MeshBasicMaterial({
      color: 0xffffff,
      opacity: 0.5,
      transparent: true,
      side: THREE.DoubleSide,
      depthTest: false,
    });
    const meshFace = new THREE.Mesh(faceGeometry, transparentMaterial);
    meshFace.name = 'meshFace' + id;
    meshFace.visible = false;
    this.group.add(meshFace);

    if (points.length > 2) {
      const geometry = new THREE.BufferGeometry().setFromPoints(points);
      const indices = [];
      for (let i = 0; i < points.length - 2; i++) {
        indices.push(0, i + 1, i + 2); // 从第一个点开始，形成三角形索引
      }
      geometry.setIndex(indices);
      meshFace.geometry.dispose();
      meshFace.geometry = geometry;
    }
  }

  removeAll() {
    this.group.clear();
  }
}

export { Draw };
