export async function drawRectWaveform(file) {
  // 创建 canvas 元素并获取其上下文
  const canvas = document.createElement('canvas');
  const ctx = canvas.getContext('2d');

  // 创建 AudioContext 和 AnalyserNode
  const audioContext = new (window.AudioContext || window.webkitAudioContext)();
  const analyser = audioContext.createAnalyser();

  // 设置 FFT 大小
  analyser.fftSize = 256;

  // 这个函数是用来绘制矩形波形
  const drawRectWaveform = (dataArray, duration) => {
    canvas.width = duration * 60; // 60像素1s
    canvas.height = 50;
    const wave = 1; // 波长 DB
    const canvasWidth = canvas.width;
    const canvasHeight = canvas.height;

    // 清除画布内容
    ctx.clearRect(0, 0, canvasWidth, canvasHeight);

    const barWidth = 0.5; // 每个矩形的宽度
    const gap = 0; // 每个矩形之间的间隔
    const step = Math.ceil(dataArray.length / (canvasWidth / (barWidth + gap))); // 采样步长

    let x = 0;
    for (let i = 0; i < dataArray.length; i += step) {
      const v = dataArray[i] * wave;
      const y = canvasHeight / 2 + (v * canvasHeight) / 2;
      const height = Math.abs(v * canvasHeight);
      ctx.fillStyle = 'rgb(255, 255, 255)';
      ctx.fillRect(x, canvasHeight / 2 - height / 2, barWidth, height);
      x += barWidth + gap;
    }
  };

  // 读取文件并解码音频数据
  if (file) {
    const reader = new FileReader();
    // 返回一个 Promise 来处理异步的文件读取和音频解码
    const buffer = await new Promise((resolve, reject) => {
      reader.onload = (event) => resolve(event.target.result);
      reader.onerror = reject;
      reader.readAsArrayBuffer(file);
    });

    // 解码音频数据并绘制波形
    const audioBuffer = await audioContext.decodeAudioData(buffer);
    const dataArray = audioBuffer.getChannelData(0); // 获取左声道数据
    // 时长
    const duration = audioBuffer.duration;
    drawRectWaveform(dataArray, duration);
  }

  return canvas;
}
