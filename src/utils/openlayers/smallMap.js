import 'ol/ol.css';
import XYZ from 'ol/source/XYZ';
import TileLayer from 'ol/layer/Tile.js';
import Map from 'ol/Map';
import Projection from 'ol/proj/Projection';
import Static from 'ol/source/ImageStatic';
import ImageLayer from 'ol/layer/Image';
import View from 'ol/View';
import { Vector as VectorLayer } from 'ol/layer';
import VectorSource from 'ol/source/Vector';
import { getCenter } from 'ol/extent';
import { Draw, Modify, Snap, Select } from 'ol/interaction.js';
import Feature from 'ol/Feature';
import { Style, Fill, Stroke, Icon, Circle as CircleStyle, Text } from 'ol/style';
import Point from 'ol/geom/Point';
import Overlay from 'ol/Overlay.js';
import { isEmpty } from 'lodash';

const key = 'fec225bc2e92332193986233b92606b1';
let map;
let element;
let imageWidth, imageHeight;
let modifyEvent, select;

const initMap = (targetName) => {
  element = document.getElementById(targetName);
  const width = element?.offsetWidth || 100,
    height = element?.offsetHeight || 100;
  const extent = [0, 0, width, height]; // 图片范围
  const projection = new Projection({
    code: 'xkcd-image',
    units: 'pixels',
    extent: extent,
  });
  if (!map) {
    map = new Map({
      layers: [],
      target: targetName,
      view: new View({
        projection: projection,
        center: getCenter(extent),
        zoom: 1,
        minZoom: 1,
        maxZoom: 20,
      }),
      controls: [],
    });
    map.getViewport().oncontextmenu = function (event) {
      event.preventDefault();
    }; // 右键事件

    setMapViewByType(0);
    setMapLayerByType(0);
  }
};

/**
 * 设置地图类型
 * @param type
 */
const setMapViewByType = (type = 0) => {
  if (type === 0) {
    const projection = 'EPSG:4326';
    const center = [116.4073963, 39.9041999];
    const newView = new View({
      projection: projection,
      center: center,
      zoom: 8,
      minZoom: 1,
      maxZoom: 20,
    });
    map.setView(newView);
  } else if (type === 1) {
    const width = element?.offsetWidth || 100,
      height = element?.offsetHeight || 100;
    const extent = [0, 0, width, height]; // 图片范围
    const projection = new Projection({
      code: 'xkcd-image',
      units: 'pixels',
      extent: extent,
    });
    const center = getCenter(extent);
    const newView = new View({
      projection: projection,
      center: center,
      zoom: 0,
      minZoom: -5,
      maxZoom: 20,
    });
    map.setView(newView);
  }
};

/**
 * 根据地图类型设置地图图层
 * @param type
 * @param imgUrl
 */
const setMapLayerByType = async (type = 0, imgUrl = null) => {
  const layerArr = findMapLayersByType('all');
  clearMapLayer(layerArr);
  // 标记图层
  const vectorLayer = new VectorLayer({
    type: 'markerLayer',
    source: new VectorSource({
      features: [],
    }),
    zIndex: 1,
  });
  map.addLayer(vectorLayer);
  if (type === 0) {
    // 经纬度
    const projection = 'EPSG:4326';
    const layer1 = new TileLayer({
      type: 'baseLayer',
      source: new XYZ({
        projection: 'EPSG:4326',
        url: `https://t{0-7}.tianditu.gov.cn/DataServer?T=vec_c&tk=${key}&x={x}&y={y}&l={z}`,
      }),
    });
    const layer2 = new TileLayer({
      type: 'baseLayer',
      source: new XYZ({
        url: `https://t{0-7}.tianditu.gov.cn/DataServer?T=cva_c&tk=${key}&x={x}&y={y}&l={z}`,
        projection,
      }),
    });
    map.addLayer(layer1);
    map.addLayer(layer2);
  } else if (type === 1 && imgUrl) {
    await addImageLayer(imgUrl);
  }
};

/**
 * 添加图片图层
 * @param {*} imgUrl
 */
const addImageLayer = (imgUrl) => {
  const image = new Image();
  image.src = imgUrl;
  return new Promise((resolve, reject) => {
    image.onload = () => {
      const width = image.width;
      const height = image.height;
      imageWidth = width;
      imageHeight = height;
      let extent = [0, 0, width, height];
      map.getView().fit(extent, map.getSize());

      let projection = new Projection({
        code: 'xkcd-image',
        units: 'pixels',
        extent: extent,
      });
      let baseLayer = new ImageLayer({
        source: new Static({
          url: imgUrl,
          projection: projection,
          imageExtent: extent,
        }),
        zIndex: 0,
        type: 'baseLayer',
      });
      map.addLayer(baseLayer);
      resolve('加载成功');
    };
    image.onerror = () => {
      reject(`图片加载失败: ${url}`);
    };
  });
};

/**
 * 根据type查询图层集合
 * @param value
 * @returns
 */
const findMapLayersByType = (value) => {
  var layers = map.getLayers(),
    layer,
    layerArr = [];
  for (var i = 0; i < layers.getLength(); i++) {
    layer = layers.item(i);
    if ((layer.get('type') && layer.get('type') == value) || value == 'all') {
      layerArr.push(layer);
    }
  }
  return layerArr;
};

const clearMapLayer = (layerArr) => {
  if (layerArr) {
    for (var i = 0; i < layerArr.length; i++) {
      map.removeLayer(layerArr[i]);
    }
  }
};

/**
 * 添加标记
 */
const addMarker = (params) => {
  const { name, coordinate, id, size, titleShow } = params;
  let coord = [];
  if (coordinate && coordinate.length > 0) {
    coord = coordinate;
  } else {
    coord = map.getView().getCenter();
  }
  const marker = new Feature({
    id: id,
    name: name,
    geometry: new Point(coord),
  });
  marker.setId(id); // 设置id
  const labelStyle = new Style({
    image: new CircleStyle({
      radius: 4,
      stroke: new Stroke({
        width: 1,
        color: 'rgba(255, 255, 255, 1.0)',
      }),
      fill: new Fill({
        color: 'rgba(40, 110, 250, 1.0)',
      }),
    }),
    text: new Text({
      font: '12px 宋体',
      text: titleShow ? name : '',
      textAlign: 'center',
      offsetY: -20,
      fill: new Fill({
        color: '#FFFFFF',
      }),
      padding: [4, 4, 4, 4],
      backgroundFill: new Fill({
        color: 'rgba(0, 0, 0, 0.6)',
      }),
    }),
  });

  const markerStyle = new Style({
    image: new Icon({
      src: require('@/assets/images/ol/point.png'),
      scale: 0.1, // 图标的大小
    }),
  });

  marker.setStyle(labelStyle);

  const layer = findMapLayersByType('markerLayer')[0];
  const source = layer.getSource();
  source.addFeature(marker);

  return coord;
};

const addOverlay = (element, position) => {
  const vienna = new Overlay({
    position: position,
    element: element,
  });
  map.addOverlay(vienna);
};

/**
 * 添加可拖动矢量事件 ['select','modify']
 */
const addInteractions = (eventList = ['select', 'modify']) => {
  const selectedStyle = new Style({
    image: new CircleStyle({
      radius: 6,
      stroke: new Stroke({
        width: 2,
        color: 'rgba(255, 255, 255, 1.0)',
      }),
      fill: new Fill({
        color: 'rgba(255, 0, 0, 1.0)',
      }),
    }),
    text: new Text({
      font: '14px 宋体',
      text: 'Selected',
      textAlign: 'center',
      offsetY: -27,
      fill: new Fill({
        color: '#FFFFFF',
      }),
      padding: [6, 6, 6, 6],
      backgroundFill: new Fill({
        color: 'rgba(0, 0, 0, 0.6)',
      }),
    }),
  });

  if (eventList.includes('select')) {
    select = new Select({
      style: function (feature) {
        const style = selectedStyle;
        style.getText().setText(feature.get('name'));
        return style;
      },
      toggleCondition: (event) => {
        const feature = map.forEachFeatureAtPixel(event.pixel, (feat) => feat);
        return feature === undefined; // 确保只有点击要素时才选中
      },
    });
    map.addInteraction(select);
  }

  if (eventList.includes('modify')) {
    modifyEvent = new Modify({
      style: selectedStyle,
      features: select.getFeatures(),
    });
    map.addInteraction(modifyEvent);

    const target = element;
    modifyEvent.on(['modifystart', 'modifyend'], function (evt) {
      target.style.cursor = evt.type === 'modifystart' ? 'grabbing' : 'pointer';
    });
    const overlaySource = modifyEvent.getOverlay().getSource();
    overlaySource.on(['addfeature', 'removefeature'], function (evt) {
      target.style.cursor = evt.type === 'addfeature' ? 'pointer' : '';
    });
  }
};

const selectEventListener = (callback) => {
  select.on('select', function (e) {
    const selectedFeatures = e.selected;
    if (selectedFeatures && selectedFeatures[0]) {
      callback(selectedFeatures[0].get('id'));
    } else {
      callback(null);
    }
  });
};

const modifyendEventListener = (callback) => {
  modifyEvent.on('modifyend', function (event) {
    const modifiedFeature = event.features.item(0);
    const geometry = modifiedFeature.getGeometry();
    const id = modifiedFeature.get('id');
    if (geometry) {
      const coordinates = geometry.getCoordinates();
      callback(id, coordinates);
    }
  });
};

/**
 * 通过id选中要素
 * @param {*} id
 */
const setMarkerSelectedById = (id) => {
  if (!id) {
    select.getFeatures().clear();
    return;
  }
  const layer = findMapLayersByType('markerLayer')[0];
  const source = layer.getSource();
  const marker = source.getFeatureById(id);
  select.getFeatures().clear();
  if (marker) {
    select.getFeatures().push(marker);
  } else {
    console.log('没有找到该要素');
  }
};

const clearLayer = (layer) => {
  const source = layer.getSource();
  source.clear();
};

const unloadMap = () => {
  if (map) {
    map.setTarget(null);
    map.dispose();
    map.removeInteraction(modifyEvent);
    map.removeInteraction(select);
    map = null;
  }
};
export {
  initMap,
  setMapViewByType,
  setMapLayerByType,
  addImageLayer,
  findMapLayersByType,
  clearMapLayer,
  addMarker,
  unloadMap,
  selectEventListener,
  setMarkerSelectedById,
  modifyendEventListener,
  clearLayer,
  addInteractions,
  addOverlay,
};
