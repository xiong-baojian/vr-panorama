// processor2.js
let video, canvas, ctx, canvas_tmp, ctx_tmp;
function init() {
  video = document.getElementById('video');
  canvas = document.getElementById('output-canvas');
  ctx = canvas.getContext('2d');
  // 创建的canvas宽高最好与显示图片的canvas、video宽高一致
  canvas_tmp = document.createElement('canvas');
  canvas_tmp.setAttribute('width', 270);
  canvas_tmp.setAttribute('height', 480);
  ctx_tmp = canvas_tmp.getContext('2d');
  video.addEventListener('play', computeFrame);
}

export function computeFrame() {
  if (video) {
    if (video.paused || video.ended) return;
  }

  const targetColor = { rTarget: 1, gTarget: 219, bTarget: 0 };
  const { rTarget, gTarget, bTarget } = targetColor;

  const a = 100;

  const rMin = rTarget - a,
    rMax = rTarget + a;
  const gMin = gTarget - a,
    gMax = gTarget + a;
  const bMin = bTarget - a,
    bMax = bTarget + a;

  ctx_tmp.drawImage(video, 0, 0, video.clientWidth / 1, video.clientHeight / 1);
  let frame = ctx_tmp.getImageData(0, 0, video.clientWidth, video.clientHeight);
  const pointLens = frame.data.length / 4;
  for (let i = 0; i < pointLens; i++) {
    let r = frame.data[i * 4];
    let g = frame.data[i * 4 + 1];
    let b = frame.data[i * 4 + 2];

    if (r >= rMin && r <= rMax && g >= gMin && g <= gMax && b >= bMin && b <= bMax) {
      // 设置该像素为透明
      frame.data[i * 4 + 3] = 0;
    }
  }
  // 重新绘制到canvas中显示
  ctx.putImageData(frame, 0, 0);
  // 递归调用
  setTimeout(computeFrame, 0);
}
document.addEventListener('DOMContentLoaded', () => {
  init();
});
