// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 删除鸟瞰图 DELETE /aerialview/delete/${param0} */
export async function deleteAerialview(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.deleteAerialviewParams,
  options?: { [key: string]: any },
) {
  const { ems_aerialview_id: param0, ...queryParams } = params;
  return request<API.ApiResponse>(`/aerialview/delete/${param0}`, {
    method: 'DELETE',
    params: { ...queryParams },
    ...(options || {}),
  });
}

/** 查询鸟瞰图 GET /aerialview/findByMap */
export async function findAerialviewByMap(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.findAerialviewByMapParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponseTableDataInfoAerialviewVo>('/aerialview/findByMap', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 新增鸟瞰图 POST /aerialview/insert */
export async function insertAerialview(body: API.Aerialview, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/aerialview/insert', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 设置默认鸟瞰图 POST /aerialview/setDefault/${param0} */
export async function setDefault(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.setDefaultParams,
  options?: { [key: string]: any },
) {
  const { ems_aerialview_id: param0, ...queryParams } = params;
  return request<API.ApiResponse>(`/aerialview/setDefault/${param0}`, {
    method: 'POST',
    params: { ...queryParams },
    ...(options || {}),
  });
}

/** 编辑鸟瞰图 POST /aerialview/update */
export async function updateAerialview(body: API.Aerialview, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/aerialview/update', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 上传鸟瞰图文件 POST /aerialview/upload */
export async function uploadAerialviewFile(
  body: {},
  file?: File,
  options?: { [key: string]: any },
) {
  const formData = new FormData();

  if (file) {
    formData.append('file', file);
  }

  Object.keys(body).forEach((ele) => {
    const item = (body as any)[ele];

    if (item !== undefined && item !== null) {
      if (typeof item === 'object' && !(item instanceof File)) {
        if (item instanceof Array) {
          item.forEach((f) => formData.append(ele, f || ''));
        } else {
          formData.append(ele, JSON.stringify(item));
        }
      } else {
        formData.append(ele, item);
      }
    }
  });

  return request<API.ApiResponseSysfile>('/aerialview/upload', {
    method: 'POST',
    data: formData,
    requestType: 'form',
    ...(options || {}),
  });
}
