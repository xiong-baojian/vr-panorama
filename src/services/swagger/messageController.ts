// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 审核留言 POST /message/auditMessage */
export async function auditMessage(body: API.Message, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/message/auditMessage', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 批量审核留言 POST /message/batchAuditMessage */
export async function batchAuditMessage(body: API.Message[], options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/message/batchAuditMessage', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 批量删除留言 DELETE /message/batchDelete */
export async function batchDeleteMessage(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.batchDeleteMessageParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponse>('/message/batchDelete', {
    method: 'DELETE',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 删除留言 DELETE /message/delete/${param0} */
export async function deleteMessage(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.deleteMessageParams,
  options?: { [key: string]: any },
) {
  const { ems_message_id: param0, ...queryParams } = params;
  return request<API.ApiResponse>(`/message/delete/${param0}`, {
    method: 'DELETE',
    params: { ...queryParams },
    ...(options || {}),
  });
}

/** 查询留言 GET /message/findByMap */
export async function findMessageByMap(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.findMessageByMapParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponseTableDataInfoMessageVo>('/message/findByMap', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 新增留言 POST /message/insert */
export async function insertMessage(body: API.Message, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/message/insert', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 编辑留言 POST /message/update */
export async function updateMessage(body: API.Message, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/message/update', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
