// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 删除开放主题 DELETE /opensubject/delete/${param0} */
export async function deleteOpensubject(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.deleteOpensubjectParams,
  options?: { [key: string]: any },
) {
  const { ems_opensubject_id: param0, ...queryParams } = params;
  return request<API.ApiResponse>(`/opensubject/delete/${param0}`, {
    method: 'DELETE',
    params: { ...queryParams },
    ...(options || {}),
  });
}

/** 查询开放主题 GET /opensubject/findByMap */
export async function findOpensubjectByMap(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.findOpensubjectByMapParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponse>('/opensubject/findByMap', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 新增开放主题 POST /opensubject/insert */
export async function insertOpensubject(body: API.Opensubject, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/opensubject/insert', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 编辑开放主题 POST /opensubject/update */
export async function updateOpensubject(body: API.Opensubject, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/opensubject/update', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
