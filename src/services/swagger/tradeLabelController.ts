// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 删除行业标签 DELETE /tradelabel/delete/${param0} */
export async function deleteTradeLabel(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.deleteTradeLabelParams,
  options?: { [key: string]: any },
) {
  const { ems_tradelabel_id: param0, ...queryParams } = params;
  return request<API.ApiResponse>(`/tradelabel/delete/${param0}`, {
    method: 'DELETE',
    params: { ...queryParams },
    ...(options || {}),
  });
}

/** 查询行业标签 GET /tradelabel/findByMap */
export async function findTradeLabelByMap(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.findTradeLabelByMapParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponse>('/tradelabel/findByMap', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 新增行业标签标签 POST /tradelabel/insert */
export async function insertTradeLabel(
  body: API.TradelabelAddPo,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponse>('/tradelabel/insert', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 编辑行业标签 POST /tradelabel/update */
export async function updateTradeLabel(
  body: API.TradelabelEditPo,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponse>('/tradelabel/update', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
