// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 审核回复 POST /replies/auditReplies */
export async function auditReplies(body: API.Replies, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/replies/auditReplies', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 批量审核回复 POST /replies/batchAuditReplies */
export async function batchAuditReplies(body: API.Replies[], options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/replies/batchAuditReplies', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 批量删除回复 DELETE /replies/batchDelete */
export async function batchDeleteReplies(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.batchDeleteRepliesParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponse>('/replies/batchDelete', {
    method: 'DELETE',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 删除回复 DELETE /replies/delete/${param0} */
export async function deleteReplies(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.deleteRepliesParams,
  options?: { [key: string]: any },
) {
  const { ems_replies_id: param0, ...queryParams } = params;
  return request<API.ApiResponse>(`/replies/delete/${param0}`, {
    method: 'DELETE',
    params: { ...queryParams },
    ...(options || {}),
  });
}

/** 查询回复 GET /replies/findByMap */
export async function findRepliesByMap(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.findRepliesByMapParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponseTableDataInfoRepliesVo>('/replies/findByMap', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 新增回复 POST /replies/insert */
export async function insertReplies(body: API.Replies, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/replies/insert', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 编辑回复 PUT /replies/update */
export async function insertReplies1(body: API.Replies, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/replies/update', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
