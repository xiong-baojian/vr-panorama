// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 删除开放设施 DELETE /openfacility/delete/${param0} */
export async function deleteOpenfacility(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.deleteOpenfacilityParams,
  options?: { [key: string]: any },
) {
  const { ems_openfacility_id: param0, ...queryParams } = params;
  return request<API.ApiResponse>(`/openfacility/delete/${param0}`, {
    method: 'DELETE',
    params: { ...queryParams },
    ...(options || {}),
  });
}

/** 查询开放设施 GET /openfacility/findByMap */
export async function findOpenfacilityByMap(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.findOpenfacilityByMapParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponse>('/openfacility/findByMap', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 新增开放设施 POST /openfacility/insert */
export async function insertOpenfacility(body: API.Openfacility, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/openfacility/insert', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 编辑开放设施 POST /openfacility/update */
export async function updateOpenfacility(body: API.Openfacility, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/openfacility/update', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
