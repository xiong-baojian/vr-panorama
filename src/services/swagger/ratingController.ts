// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 新增或删除评价 POST /rating/insertOrDelete */
export async function insertOrDeleteRating(body: API.Rating, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/rating/insertOrDelete', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
