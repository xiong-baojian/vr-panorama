// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 批量新增布设 POST /layout/batchInsert */
export async function batchInsertLayout(body: API.Layout[], options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/layout/batchInsert', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 批量编辑布设 POST /layout/batchUpdate */
export async function batchUpdateLayout(body: API.Layout[], options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/layout/batchUpdate', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 删除布设 DELETE /layout/delete/${param0} */
export async function deleteLayout(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.deleteLayoutParams,
  options?: { [key: string]: any },
) {
  const { ems_layout_id: param0, ...queryParams } = params;
  return request<API.ApiResponse>(`/layout/delete/${param0}`, {
    method: 'DELETE',
    params: { ...queryParams },
    ...(options || {}),
  });
}

/** 查询布设 GET /layout/findByMap */
export async function findLayoutByMap(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.findLayoutByMapParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponseTableDataInfoLayoutVo>('/layout/findByMap', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 新增布设 POST /layout/insert */
export async function insertLayout(body: API.Layout, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/layout/insert', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 编辑布设 POST /layout/update */
export async function updateLayout(body: API.Layout, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/layout/update', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
