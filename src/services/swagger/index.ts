// @ts-ignore
/* eslint-disable */
// API 更新时间：
// API 唯一标识：
import * as aerialviewController from './aerialviewController';
import * as apiController from './apiController';
import * as commonController from './commonController';
import * as compositionController from './compositionController';
import * as configurationController from './configurationController';
import * as customerController from './customerController';
import * as devicealarmController from './devicealarmController';
import * as deviceController from './deviceController';
import * as DrawController from './DrawController';
import * as guideController from './guideController';
import * as htmlController from './htmlController';
import * as instanceController from './instanceController';
import * as InstancePanoramaGroupController from './InstancePanoramaGroupController';
import * as itemController from './itemController';
import * as jobGroupController from './jobGroupController';
import * as jobInfoController from './jobInfoController';
import * as jobLogController from './jobLogController';
import * as layoutController from './layoutController';
import * as lookpointController from './lookpointController';
import * as messageController from './messageController';
import * as MonitordataController from './MonitordataController';
import * as navigationController from './navigationController';
import * as openfacilityController from './openfacilityController';
import * as openindustryController from './openindustryController';
import * as opensubjectController from './opensubjectController';
import * as panoramaController from './panoramaController';
import * as productController from './productController';
import * as propertyController from './propertyController';
import * as ratingController from './ratingController';
import * as repliesController from './repliesController';
import * as reportController from './reportController';
import * as restapigroupController from './restapigroupController';
import * as resultmapController from './resultmapController';
import * as roleController from './roleController';
import * as situationController from './situationController';
import * as syslogController from './syslogController';
import * as sysuserController from './sysuserController';
import * as tagtypeController from './tagtypeController';
import * as tradeLabelController from './tradeLabelController';
import * as tradelabelattributeController from './tradelabelattributeController';
import * as videofusionController from './videofusionController';
import * as ViewangleController from './ViewangleController';
import * as vrPreviewController from './vrPreviewController';
import * as XiLingDrController from './XiLingDrController';
export default {
  aerialviewController,
  apiController,
  commonController,
  compositionController,
  configurationController,
  customerController,
  devicealarmController,
  deviceController,
  DrawController,
  guideController,
  htmlController,
  instanceController,
  InstancePanoramaGroupController,
  itemController,
  jobGroupController,
  jobInfoController,
  jobLogController,
  layoutController,
  lookpointController,
  messageController,
  MonitordataController,
  navigationController,
  openfacilityController,
  openindustryController,
  opensubjectController,
  panoramaController,
  productController,
  propertyController,
  ratingController,
  repliesController,
  reportController,
  restapigroupController,
  resultmapController,
  roleController,
  situationController,
  syslogController,
  sysuserController,
  tagtypeController,
  tradeLabelController,
  tradelabelattributeController,
  videofusionController,
  ViewangleController,
  vrPreviewController,
  XiLingDrController,
};
