// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 删除视角 POST /viewangle/delete/${param0} */
export async function deleteViewangle(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.deleteViewangleParams,
  options?: { [key: string]: any },
) {
  const { ems_view_id: param0, ...queryParams } = params;
  return request<API.ApiResponse>(`/viewangle/delete/${param0}`, {
    method: 'POST',
    params: { ...queryParams },
    ...(options || {}),
  });
}

/** 查询视角 GET /viewangle/findByMap */
export async function findByViewangleMap(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.findByViewangleMapParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponseTableDataInfoViewangleVo>('/viewangle/findByMap', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 新增视角 POST /viewangle/insert */
export async function insertViewangle(body: API.Viewangle, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/viewangle/insert', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 切换视角 GET /viewangle/switchPerspective */
export async function switchPerspective(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.switchPerspectiveParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponse>('/viewangle/switchPerspective', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 编辑视角 POST /viewangle/update */
export async function updateViewangle(body: API.Viewangle, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/viewangle/update', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
