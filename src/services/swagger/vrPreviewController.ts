// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 删除vr预览链接 GET /vrpreview/delete */
export async function deleteVrPreview(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.deleteVrPreviewParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponse>('/vrpreview/delete', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 查询vr预览链接 GET /vrpreview/findByMap */
export async function findByVrPreviewMap(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.findByVrPreviewMapParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponseTableDataInfoVrPreview>('/vrpreview/findByMap', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 生成vr预览 POST /vrpreview/generate */
export async function generateVrPreview(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.generateVrPreviewParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponse>('/vrpreview/generate', {
    method: 'POST',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 查询vr预览链接是否过期 GET /vrpreview/isExpired */
export async function isExpired(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.isExpiredParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponse>('/vrpreview/isExpired', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 编辑vr预览 POST /vrpreview/update */
export async function vrPreview(body: API.VrPreview, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/vrpreview/update', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
