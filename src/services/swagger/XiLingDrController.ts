// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 数字人回调 POST /dr/callback */
export async function callback(body: API.fastjson2, options?: { [key: string]: any }) {
  return request<any>('/dr/callback', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 查询视频合成任务 GET /dr/findVideoTask */
export async function findVideoTask(options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/dr/findVideoTask', {
    method: 'GET',
    ...(options || {}),
  });
}

/** 数字人生成 POST /dr/generateDr */
export async function generateDr(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.generateDrParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponse>('/dr/generateDr', {
    method: 'POST',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}
