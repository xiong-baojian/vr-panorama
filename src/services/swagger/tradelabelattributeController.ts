// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 批量新增行业标签属性 POST /tradelabelattribute/insertBatch */
export async function insertBatchTradelabelattribute(
  body: API.TradelabelattributeAddPo[],
  options?: { [key: string]: any },
) {
  return request<API.ApiResponse>('/tradelabelattribute/insertBatch', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
