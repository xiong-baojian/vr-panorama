// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 删除开放行业 DELETE /openindustrylayout/delete/${param0} */
export async function deleteOpenindustry(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.deleteOpenindustryParams,
  options?: { [key: string]: any },
) {
  const { ems_openindustry_id: param0, ...queryParams } = params;
  return request<API.ApiResponse>(`/openindustrylayout/delete/${param0}`, {
    method: 'DELETE',
    params: { ...queryParams },
    ...(options || {}),
  });
}

/** 查询开放行业 GET /openindustrylayout/findByMap */
export async function findOpenindustryByMap(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.findOpenindustryByMapParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponse>('/openindustrylayout/findByMap', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 新增开放行业 POST /openindustrylayout/insert */
export async function insertOpenindustry(body: API.Openindustry, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/openindustrylayout/insert', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 编辑开放行业 POST /openindustrylayout/update */
export async function updateOpenindustry(body: API.Openindustry, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/openindustrylayout/update', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
