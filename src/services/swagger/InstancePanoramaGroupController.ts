// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 删除站房全景组 DELETE /instancepanoramaGroup/delete/${param0} */
export async function deleteInstancePanoramaGroup(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.deleteInstancePanoramaGroupParams,
  options?: { [key: string]: any },
) {
  const { ems_instance_panorama_group_id: param0, ...queryParams } = params;
  return request<API.ApiResponse>(`/instancepanoramaGroup/delete/${param0}`, {
    method: 'DELETE',
    params: { ...queryParams },
    ...(options || {}),
  });
}

/** 根据站房查询全景组 GET /instancepanoramaGroup/findGroupByInstance/${param0} */
export async function findGroupByInstance(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.findGroupByInstanceParams,
  options?: { [key: string]: any },
) {
  const { ems_instance_panorama_group_instanceid: param0, ...queryParams } = params;
  return request<API.ApiResponseListInstancePanoramaGroup>(
    `/instancepanoramaGroup/findGroupByInstance/${param0}`,
    {
      method: 'GET',
      params: { ...queryParams },
      ...(options || {}),
    },
  );
}

/** 新增站房全景组 POST /instancepanoramaGroup/insert */
export async function insertInstancePanoramaGroup(
  body: API.InstancePanoramaGroupReq,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponse>('/instancepanoramaGroup/insert', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 编辑站房全景组 POST /instancepanoramaGroup/update */
export async function updateInstancePanoramaGroup(
  body: API.InstancePanoramaGroupReq,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponse>('/instancepanoramaGroup/update', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
