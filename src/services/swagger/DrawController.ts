// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 删除绘制 DELETE /draw/delete/${param0} */
export async function deleteDraw(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.deleteDrawParams,
  options?: { [key: string]: any },
) {
  const { ems_draw_id: param0, ...queryParams } = params;
  return request<API.ApiResponse>(`/draw/delete/${param0}`, {
    method: 'DELETE',
    params: { ...queryParams },
    ...(options || {}),
  });
}

/** 查询绘制 GET /draw/findByMap */
export async function findDrawByMap(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.findDrawByMapParams,
  options?: { [key: string]: any },
) {
  return request<API.ApiResponse>('/draw/findByMap', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 新增绘制 POST /draw/insert */
export async function insertDraw(body: API.Draw, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/draw/insert', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 编辑绘制 POST /draw/update */
export async function updateDraw(body: API.Draw, options?: { [key: string]: any }) {
  return request<API.ApiResponse>('/draw/update', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
